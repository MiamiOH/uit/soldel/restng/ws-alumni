# ws-alumni

RESTful Api to manage Alumni records in Banner ERP.

## Local development setup

Follow Instruction from the RestNg Docs to get docker setup for local development. [https://wsdev.apps.miamioh.edu/api/docs/docker/restng/](https://wsdev.apps.miamioh.edu/api/docs/docker/restng/).

## Api Documentation

Checkout the Api documentation in Swagger UI.
[https://wsdev.apps.miamioh.edu/api/swagger-ui/#/alumni/](https://wsdev.apps.miamioh.edu/api/swagger-ui/#/alumni/).

## Authorizations in CAM

Authorization for ws-alumni is stored in `CAM`, `WebServices` application and `Alumni` category. Following keys `view` and `all` are used.
