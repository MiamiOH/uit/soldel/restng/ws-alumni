<?php

return [
    'resources' => [
        'alumni' => [
            MiamiOH\AlumniWebService\App\Resources\AlumniEmploymentHistoryResourceProvider::class,
            \MiamiOH\AlumniWebService\App\Resources\AlumniLinkedInResourceProvider::class,
        ],
    ]
];
