<?php

namespace MiamiOH\AlumniWebService\App\Resources;

use MiamiOH\RESTng\App;

class AlumniEmploymentHistoryResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    /**
     * @var string
     */
    private $tag = 'alumni';
    /**
     * @var string
     */
    private $serviceName = 'Alumni.EmploymentHistory';

    public function registerDefinitions(): void
    {
        $this->addTag([
            'name' => $this->tag,
            'description' => 'Resources for data about Alumni Employment History'
        ]);

        $this->addDefinition([
            'name' => 'Alumni.EmploymentHistory',
            'type' => 'object',
            'properties' => [
                'prospectId' => [
                    'type' => 'string',
                    'example' => 'A00462170'
                ],
                'sequence' => [
                    'type' => 'integer',
                    'example' => '2'
                ],
                'employer' => [
                    'type' => 'string',
                    'example' => '3M'
                ],
                'employerId' => [
                    'type' => 'string',
                    'example' => 'THREEM'
                ],
                'position' => [
                    'type' => 'string',
                    'example' => 'CEO and founder'
                ],
                'isPrimary' => [
                    'type' => 'boolean',
                    'example' => false
                ],
                'from' => [
                    'type' => 'string',
                    'example' => '2020-01-23'
                ],
                'to' => [
                    'type' => 'string',
                    'example' => '2020-08-12'
                ],
                'jobCategoryCodes' => [
                    'type' => 'array',
                    'items' => [],
                    'example' => ['FDR', 'CEO']
                ],
                'standardIndustrialCode' => [
                    'type' => 'string',
                    'example' => '38XX'
                ],
                'crossReferenceCode' => [
                    'type' => 'string',
                    'example' => 'FEE'
                ],
                'statusCode' => [
                    'type' => 'string',
                    'example' => 'R'
                ],
                'weeklyHours' => [
                    'type' => 'float',
                    'example' => 20
                ],
                'isOkForNotes' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'isDisplayedNotes' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'isMatchingGift' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'isReviewed' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'reviewedBy' => [
                    'type' => 'string',
                    'example' => 'johndam'
                ],
                'isCoop' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'noteDate' => [
                    'type' => 'string',
                    'example' => '2020-03-12'
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Jenifor found this record on LinkedIn on 03/12/20.'
                ],
                'address' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Address.v2',
                ],
                'phones' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Phone.Collection'
                ],
                'workEmails' => [
                    'type' => 'array',
                    'items' => [
                        '$ref' => '#/definitions/Person.Email.V2'
                    ]
                ],
                'user' => [
                    'type' => 'string',
                    'example' => 'rejemr'
                ],
                'updatedAt' => [
                    'type' => 'string',
                    'example' => '2020-02-20 13:20:35'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.EmploymentHistory.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Alumni.EmploymentHistory'
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.WorkEmail.Request',
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'example' => 'john.doe@3m.com'
                ],
                'isPreferred' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Jeniffer found this email on LinkedIn.'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.Phone.Request',
            'type' => 'object',
            'properties' => [
                'type' => [
                    'type' => 'string',
                    'example' => 'A1'
                ],
                'areaCode' => [
                    'type' => 'string',
                    'example' => '513'
                ],
                'number' => [
                    'type' => 'string',
                    'example' => '2371823'
                ],
                'extension' => [
                    'type' => 'string',
                    'example' => '3357'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.Address.Request',
            'type' => 'object',
            'properties' => [
                'type' => [
                    'type' => 'string',
                    'example' => 'A1'
                ],
                'from' => [
                    'type' => 'string',
                    'example' => '2020-02-21'
                ],
                'to' => [
                    'type' => 'string',
                    'example' => '2020-07-12'
                ],
                'isPreferred' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'streetLine1' => [
                    'type' => 'string',
                    'example' => '200 Main St.'
                ],
                'streetLine2' => [
                    'type' => 'string',
                    'example' => 'Suit 3'
                ],
                'streetLine3' => [
                    'type' => 'string',
                    'example' => 'null'
                ],
                'city' => [
                    'type' => 'string',
                    'example' => 'Cincinnati'
                ],
                'state' => [
                    'type' => 'string',
                    'example' => 'OH'
                ],
                'zip' => [
                    'type' => 'string',
                    'example' => '45202'
                ],
                'addresSourceCode' => [
                    'type' => 'string',
                    'example' => 'COR'
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.Request',
            'type' => 'object',
            'properties' => [
                'prospectId' => [
                    'type' => 'string',
                    'example' => 'A00414240'
                ],
                'employer' => [
                    'type' => 'string',
                    'example' => '3M'
                ],
                'employerId' => [
                    'type' => 'string',
                    'example' => 'THREEM'
                ],
                'position' => [
                    'type' => 'string',
                    'example' => 'Founder and CEO'
                ],
                'isPrimary' => [
                    'type' => 'string',
                    'example' => true
                ],
                'from' => [
                    'type' => 'string',
                    'example' => '2001-03-29'
                ],
                'to' => [
                    'type' => 'string',
                    'example' => '2010-12-23'
                ],
                'weeklyHours' => [
                    'type' => 'string',
                    'example' => 40.5
                ],
                'statusCode' => [
                    'type' => 'string',
                    'example' => 'C'
                ],
                'jobCategoryCodes' => [
                    'type' => 'string',
                    'example' => ['FDR', 'CEO']
                ],
                'standardIndustrialCode' => [
                    'type' => 'string',
                    'example' => '80XX'
                ],
                'crossReferenceCode' => [
                    'type' => 'string',
                    'example' => 'EME'
                ],
                'isOkForNotes' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'isDisplayedNotes' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'isMatchingGift' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'isReviewed' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'reviewedBy' => [
                    'type' => 'string',
                    'example' => 'johndam'
                ],
                'isCoop' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'noteDate' => [
                    'type' => 'string',
                    'example' => '2020-03-12'
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Jeniffer found this record on LinkedIn on 03/04/20.'
                ],
                'workEmail' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.WorkEmail.Request'
                ],
                'phone' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.Phone.Request'
                ],
                'address' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.Address.Request'
                ],
                'createdBy' => [
                    'type' => 'string',
                    'example' => 'Bobam'
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.Request.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.Request'
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.Result',
            'type' => 'object',
            'properties' => [
                'isSuccess' => [
                    'type' => 'boolean'
                ],
                'data' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Alumni.EmploymentHistory'
                ],
                'message' => [
                    'type' => 'string'
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.EmploymentHistory.Result.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.Result'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->serviceName,
            'class' => 'MiamiOH\AlumniWebService\App\Services\AlumniEmploymentHistoryService',
            'description' => 'Provides Alumni Employment History data.',
            'params' => [
                'app' => [
                    'name' => 'APIApp'
                ]
            ],
            'set' => [
                'dataSource' => [
                    'type' => 'service', 'name' => 'APIDataSource'
                ],
            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get Alumni Employment History',
            'name' => 'alumni.employmentHistory.get',
            'service' => $this->serviceName,
            'method' => 'get',
            'tags' => [$this->tag],
            'pattern' => '/alumni/employmentHistory/:prospectId',
            'params' => [
                'prospectId' => [
                    'description' => 'Alumni ID',
                    'type' => 'single',
                    'required' => true
                ],
            ],

            'responses' => [
                App::API_OK => array(
                    'description' => 'A collection of employment history',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Alumni.EmploymentHistory.Collection',
                    )
                ),
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'get',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create Alumni Employment History',
            'name' => 'alumni.employmentHistory.create',
            'service' => $this->serviceName,
            'method' => 'create',
            'tags' => [$this->tag],
            'pattern' => '/alumni/employmentHistory',
            'body' => [
                'required' => true,
                'description' => 'Create Alumni Employment History Request',
                'schema' => [
                    '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.Request.Collection'
                ]
            ],
            'responses' => [
                App::API_OK => array(
                    'description' => 'A collection of result',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Alumni.Create.EmploymentHistory.Result.Collection',
                    )
                ),
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'create',
                    ]
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
