<?php


namespace MiamiOH\AlumniWebService\App\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AlumniLinkedInResourceProvider extends ResourceProvider
{
    /**
     * @var string
     */
    private $tag = 'alumni';
    /**
     * @var string
     */
    private $serviceName = 'Alumni.LinkedIn';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => 'Alumni.LinkedIn',
            'type' => 'object',
            'properties' => [
                'id' => [
                    'type' => 'integer',
                    'example' => '172836',
                ],
                'prospectId' => [
                    'type' => 'string',
                    'example' => 'A38294734',
                ],
                'url' => [
                    'type' => 'string',
                    'example' => 'https://www.linkedin.com/in/john-doe-12839'
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Jeniffer found his LinkedIn URL from the vendor in July.'
                ],
                'updatedAt' => [
                    'type' => 'string',
                    'example' => '2020-06-12 14:10:00'
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.LinkedIn.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Alumni.LinkedIn'
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.LinkedIn.Request',
            'type' => 'object',
            'properties' => [
                'prospectId' => [
                    'type' => 'string',
                    'example' => 'A38294734',
                ],
                'url' => [
                    'type' => 'string',
                    'example' => 'https://www.linkedin.com/in/john-doe-12839'
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Jeniffer found his LinkedIn URL from the vendor in July.'
                ],
                'createdBy' => [
                    'type' => 'string',
                    'example' => 'johnas'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Update.LinkedIn.Request',
            'type' => 'object',
            'properties' => [
                'url' => [
                    'type' => 'string',
                    'example' => 'https://www.linkedin.com/in/john-doe-12839'
                ],
                'comment' => [
                    'type' => 'string',
                    'example' => 'Jeniffer found his LinkedIn URL from the vendor in July.'
                ],
                'updatedBy' => [
                    'type' => 'string',
                    'example' => 'johnas'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.LinkedIn.Request.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Alumni.Create.LinkedIn.Request'
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.LinkedIn.Result',
            'type' => 'object',
            'properties' => [
                'isSuccess' => [
                    'type' => 'boolean'
                ],
                'data' => [
                    'type' => 'object',
                    '$ref' => '#/definitions/Alumni.LinkedIn'
                ],
                'message' => [
                    'type' => 'string'
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'Alumni.Create.LinkedIn.Result.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Alumni.Create.LinkedIn.Result'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->serviceName,
            'class' => 'MiamiOH\AlumniWebService\App\Services\AlumniLinkedInService',
            'description' => 'Provides Alumni LinkedIn records.',
            'params' => [
                'app' => [
                    'name' => 'APIApp'
                ]
            ],
            'set' => [
                'dataSource' => [
                    'type' => 'service', 'name' => 'APIDataSource'
                ],
            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get Alumni LinkedIn Url',
            'name' => 'alumni.linkedIn.get',
            'service' => $this->serviceName,
            'method' => 'get',
            'tags' => [$this->tag],
            'pattern' => '/alumni/linkedIn/:prospectId',
            'params' => [
                'prospectId' => [
                    'description' => 'Alumni ID',
                    'type' => 'single',
                    'required' => true
                ],
            ],
            'responses' => [
                App::API_OK => array(
                    'description' => 'A collection of LinkedIn',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Alumni.LinkedIn.Collection',
                    )
                ),
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'get',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create LinkedIn',
            'name' => 'alumni.linkedIn.create',
            'service' => $this->serviceName,
            'method' => 'create',
            'tags' => [$this->tag],
            'pattern' => '/alumni/linkedIn',
            'body' => [
                'required' => true,
                'description' => 'Create Alumni LinkedIn Request',
                'schema' => [
                    '$ref' => '#/definitions/Alumni.Create.LinkedIn.Request.Collection'
                ]
            ],
            'responses' => [
                App::API_OK => array(
                    'description' => 'A collection of result',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Alumni.Create.LinkedIn.Result.Collection',
                    )
                ),
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'create',
                    ]
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'description' => 'Update LinkedIn',
            'name' => 'alumni.linkedIn.update',
            'service' => $this->serviceName,
            'method' => 'update',
            'tags' => [$this->tag],
            'pattern' => '/alumni/linkedIn/:prospectId/:id',
            'params' => [
                'prospectId' => [
                    'description' => 'Prospect ID',
                    'type' => 'single',
                    'required' => true
                ],
                'id' => [
                    'description' => 'LinkedIn ID',
                    'type' => 'single',
                    'required' => true
                ],
            ],
            'body' => [
                'required' => true,
                'description' => 'Update Alumni LinkedIn Request',
                'schema' => [
                    '$ref' => '#/definitions/Alumni.Update.LinkedIn.Request'
                ]
            ],
            'responses' => [
                App::API_OK => array(
                    'description' => 'A collection of result',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Alumni.LinkedIn',
                    )
                ),
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'all',
                    ],
                    [
                        'application' => 'WebServices',
                        'module' => 'Alumni',
                        'key' => 'update',
                    ]
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
        //
    }
}
