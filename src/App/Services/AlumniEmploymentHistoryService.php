<?php

namespace MiamiOH\AlumniWebService\App\Services;

use Illuminate\Database\Eloquent\Model;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Services\AlumniAddressService;
use MiamiOH\AlumniWebService\Domain\Services\AlumniPhoneService;
use MiamiOH\AlumniWebService\Domain\Services\AlumniWorkEmailService;
use MiamiOH\AlumniWebService\Domain\Services\CreateAlumniEmploymentHistory;
use MiamiOH\AlumniWebService\Domain\Services\GetAlumniEmploymentHistory;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Exceptions\BadRequestsException;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumni;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumniEmployment;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentEmployer;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniEmploymentRepository;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniRepository;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniAddressRepository;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniPhoneRepository;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniWorkEmailRepository;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Util\Response;

class AlumniEmploymentHistoryService extends BaseService
{
    /**
     * @var CreateAlumniEmploymentHistory
     */
    private $createAlumniEmploymentHistory;
    /**
     * @var GetAlumniEmploymentHistory
     */
    private $getAlumniEmploymentHistory;

    public function __construct(
        App $app = null,
        DataSource $ds = null,
        CreateAlumniEmploymentHistory $createAlumniEmploymentHistory = null,
        GetAlumniEmploymentHistory $getAlumniEmploymentHistory = null
    ) {
        parent::__construct($app, $ds);
        $this->createAlumniEmploymentHistory = $createAlumniEmploymentHistory;
        $this->getAlumniEmploymentHistory = $getAlumniEmploymentHistory;
    }

    protected function setup(App $app): void
    {
        $alumniRepository = new EloquentAlumniRepository(new EloquentAlumni());
        $alumniAddressRepository = new RESTngAlumniAddressRepository($app);
        $alumniEmploymentRepository = new EloquentAlumniEmploymentRepository(
            new EloquentAlumniEmployment(),
            new EloquentEmployer(),
            Model::getConnectionResolver()->connection()
        );
        $alumniPhoneRepository = new RESTngAlumniPhoneRepository($app);
        $alumniWorkEmailRepository = new RESTngAlumniWorkEmailRepository($app);
        $alumniWorkEmailService = new AlumniWorkEmailService($alumniWorkEmailRepository);
        $alumniPhoneService = new AlumniPhoneService($alumniPhoneRepository);
        $alumniAddressService = new AlumniAddressService($alumniAddressRepository);

        $this->createAlumniEmploymentHistory = new CreateAlumniEmploymentHistory(
            $alumniRepository,
            $alumniAddressService,
            $alumniEmploymentRepository,
            $alumniPhoneService,
            $alumniWorkEmailService
        );

        $this->getAlumniEmploymentHistory = new GetAlumniEmploymentHistory(
            $alumniRepository,
            $alumniAddressRepository,
            $alumniEmploymentRepository,
            $alumniPhoneRepository,
            $alumniWorkEmailRepository
        );
    }

    public function get(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $prospectId = $request->getResourceParam('prospectId');

        try {
            $employments = $this->getAlumniEmploymentHistory->byProspectId($prospectId);
            $response->setPayload($employments->toJsonArray());
            $response->setStatus(App::API_OK);
        } catch (ApplicationException $e) {
            $response->setPayload($e->toJsonArray());
            $response->setStatus(App::API_FAILED);
        }

        return $response;
    }

    public function create(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();
        $user = $this->getApiUser()->getUsername();

        try {
            $requests = CreateAlumniEmploymentRequestDTOCollection::createFromArray($body, $user);
            $results = $this->createAlumniEmploymentHistory->create($requests);

            $response->setPayload($results->toJsonArray());
            $response->setStatus(App::API_OK);
        } catch (BadRequestsException $e) {
            $response->setPayload($e->toJsonArray());
            $response->setStatus(App::API_BADREQUEST);
        }

        return $response;
    }
}
