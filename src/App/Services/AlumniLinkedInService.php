<?php

namespace MiamiOH\AlumniWebService\App\Services;

use Illuminate\Validation\ValidationException;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniLinkedInUrlRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateAlumniLinkedInUrlRequest;
use MiamiOH\AlumniWebService\Domain\Services\AlumniLinkedInService as AlumniLinkedInDomainService;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Exceptions\BadRequestsException;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumni;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniRepository;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniWorkEmailRepository;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Util\Response;

class AlumniLinkedInService extends BaseService
{
    /**
     * @var AlumniLinkedInDomainService
     */
    private $alumniLinkedInService;

    public function __construct(
        App $app = null,
        DataSource $ds = null,
        AlumniLinkedInDomainService $alumniLinkedInService = null
    ) {
        parent::__construct($app, $ds);
        $this->alumniLinkedInService = $alumniLinkedInService;
    }

    protected function setup(App $app): void
    {
        $alumniRepository = new EloquentAlumniRepository(new EloquentAlumni());
        $alumniWorkEmailRepository = new RESTngAlumniWorkEmailRepository($app);

        $this->alumniLinkedInService = new AlumniLinkedInDomainService(
            $alumniRepository,
            $alumniWorkEmailRepository
        );
    }

    public function get(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $prospectId = $request->getResourceParam('prospectId');

        try {
            $urls = $this->alumniLinkedInService->get($prospectId);

            $response->setPayload($urls->toJsonArray());
            $response->setStatus(App::API_OK);
        } catch (ApplicationException $e) {
            $response->setPayload($e->toJsonArray());
            $response->setStatus(App::API_FAILED);
        }

        return $response;
    }

    public function create(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();

        $user = $this->getApiUser()->getUsername();
        foreach ($body as &$item) {
            if (!isset($item['createdBy'])) {
                $item['createdBy'] = $user;
            }
        }

        try {
            $requests = CreateAlumniLinkedInUrlRequestDTOCollection::createFromArray($body);

            $results = $this->alumniLinkedInService->create($requests);

            $response->setPayload($results->toJsonArray());
            $response->setStatus(App::API_OK);
        } catch (BadRequestsException $e) {
            $response->setPayload($e->toJsonArray());
            $response->setStatus(App::API_BADREQUEST);
        }

        return $response;
    }

    public function update(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $body = $request->getData();

        $user = $this->getApiUser()->getUsername();
        if (!isset($body['updatedBy'])) {
            $body['updatedBy'] = $user;
        }
        $body['id'] = $request->getResourceParam('id');
        $body['prospectId'] = $request->getResourceParam('prospectId');

        try {
            $request = UpdateAlumniLinkedInUrlRequest::createFromArray($body);
            $url = $this->alumniLinkedInService->update($request);

            $response->setPayload($url->toJsonArray());
            $response->setStatus(App::API_OK);
        } catch (ValidationException $e) {
            $errors = [];
            foreach ($e->errors() as $errs) {
                foreach ($errs as $err) {
                    $errors[] = $err;
                }
            }
            $response->setPayload($errors);
            $response->setStatus(App::API_BADREQUEST);
        } catch (ApplicationException $e) {
            $response->setPayload($e->toJsonArray());
            $response->setStatus(App::API_FAILED);
        }

        return $response;
    }
}
