<?php


namespace MiamiOH\AlumniWebService\App\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;

abstract class BaseService extends Service
{
    /**
     * @var DataSource
     */
    private $ds;
    /**
     * @var App
     */
    private $restngApp;

    /**
     * BaseService constructor.
     * @param App|null $app
     * @param DataSource|null $ds
     */
    public function __construct(App $app = null, DataSource $ds = null)
    {
        if ($app) {
            $this->restngApp = $app;
        }
        if ($ds) {
            $this->ds = $ds;
        }
    }


    public function setDataSource(DataSource $datasource)
    {
        $this->setup($this->restngApp);
    }

    abstract protected function setup(App $app): void;
}
