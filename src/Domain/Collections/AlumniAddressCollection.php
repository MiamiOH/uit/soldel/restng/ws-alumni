<?php


namespace MiamiOH\AlumniWebService\Domain\Collections;

use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;

class AlumniAddressCollection extends BaseModelCollection
{
    public function lookup(CreateAlumniAddressRequest $request): ?AlumniAddress
    {
        /** @var AlumniAddress $addr */
        foreach ($this as $addr) {
            if ($addr->getPidm() === $request->getPidm()
                && $addr->getAddressType() === $request->getType()
                && $addr->getStreetLine1() === $request->getStreetLine1()
                && $addr->getStreetLine2() === $request->getStreetLine2()
                && $addr->getStreetLine3() === $request->getStreetLine3()
                && $addr->getCity() === $request->getCity()
                && $addr->getState() === $request->getState()
                && $addr->getPostalCode() === $request->getZip()
            ) {
                return $addr;
            }
        }

        return null;
    }

    public function getByTypeAndSeq(string $type, int $seq): ?AlumniAddress
    {
        /** @var AlumniAddress $addr */
        foreach ($this as $addr) {
            if ($addr->getAddressType() === $type && $addr->getSequenceNumber() === $seq) {
                return $addr;
            }
        }

        return null;
    }
}
