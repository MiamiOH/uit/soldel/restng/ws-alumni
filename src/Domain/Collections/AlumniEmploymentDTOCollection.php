<?php

namespace MiamiOH\AlumniWebService\Domain\Collections;

use MiamiOH\AlumniWebService\Domain\Models\AlumniEmploymentDTO;

class AlumniEmploymentDTOCollection extends BaseModelCollection
{
    public function getAllAddressTypes(): array
    {
        $types = [];

        /** @var AlumniEmploymentDTO $dto */
        foreach ($this as $dto) {
            $type = $dto->getAddressType();

            if ($type && !in_array($type, $types)) {
                $types[] = $type;
            }
        }

        return $types;
    }
}
