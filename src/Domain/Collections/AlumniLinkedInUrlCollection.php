<?php

namespace MiamiOH\AlumniWebService\Domain\Collections;

use MiamiOH\AlumniWebService\Domain\Models\AlumniLinkedInUrl;

class AlumniLinkedInUrlCollection extends BaseModelCollection
{
    public function getById(string $id): ?AlumniLinkedInUrl
    {
        /** @var AlumniLinkedInUrl $url */
        foreach ($this as $url) {
            if ($url->getId() === $id) {
                return $url;
            }
        }
        return null;
    }
}
