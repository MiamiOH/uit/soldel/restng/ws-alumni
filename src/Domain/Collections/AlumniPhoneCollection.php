<?php

namespace MiamiOH\AlumniWebService\Domain\Collections;

use MiamiOH\AlumniWebService\Domain\Models\AlumniPhone;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;

class AlumniPhoneCollection extends BaseModelCollection
{
    public function lookup(CreateAlumniPhoneRequest $request): ?AlumniPhone
    {
        /** @var AlumniPhone $phone */
        foreach ($this as $phone) {
            if ($phone->getPidm() === $request->getPidm()
                && $phone->getAreaPart() === $request->getAreaCode()
                && $phone->getNumberPart() === $request->getNumber()
                && $phone->getExt() === $request->getExtension()
                && $phone->getPhoneType() === $request->getType()
            ) {
                return $phone;
            }
        }
        return null;
    }
}
