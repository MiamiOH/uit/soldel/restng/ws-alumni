<?php

namespace MiamiOH\AlumniWebService\Domain\Collections;

use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;

class AlumniWorkEmailCollection extends BaseModelCollection
{
    public function lookup(CreateAlumniWorkEmailRequest $request): ?AlumniWorkEmail
    {
        /** @var AlumniWorkEmail $email */
        foreach ($this as $email) {
            if ($email->getPidm() === $request->getPidm()
                && $email->getTypeCode() === $request->getType()
                && $email->getEmail() === $request->getEmail()
            ) {
                return $email;
            }
        }
        return null;
    }

    public function byId(string $emailId): ?AlumniWorkEmail
    {
        /** @var AlumniWorkEmail $email */
        foreach ($this as $email) {
            if ($email->getId() === $emailId) {
                return $email;
            }
        }
        return null;
    }
}
