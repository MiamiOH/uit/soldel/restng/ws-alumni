<?php


namespace MiamiOH\AlumniWebService\Domain\Collections;

use Illuminate\Support\Collection;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

abstract class BaseModelCollection extends Collection implements Jsonable
{
    public function toJsonArray(): array
    {
        $data = [];

        /** @var Jsonable $model */
        foreach ($this as $model) {
            $data[] = $model->toJsonArray();
        }

        return $data;
    }
}
