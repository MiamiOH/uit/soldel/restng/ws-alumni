<?php


namespace MiamiOH\AlumniWebService\Domain\Collections;

use Illuminate\Validation\ValidationException;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequestDTO;
use MiamiOH\AlumniWebService\Exceptions\BadRequestsException;

class CreateAlumniEmploymentRequestDTOCollection extends BaseModelCollection
{
    /**
     * @param array $data
     * @param string $user
     * @return self
     * @throws BadRequestsException
     */
    public static function createFromArray(array $data, string $user): self
    {
        $requests = new self();
        $errors = [];

        foreach ($data as $index => $requestData) {
            if (!isset($requestData['createdBy']) || !$requestData['createdBy']) {
                $requestData['createdBy'] = $user;
            }
            try {
                $requests->push(CreateAlumniEmploymentRequestDTO::createFromArray($requestData));
            } catch (ValidationException $e) {
                $rawErrs = $e->errors();
                foreach ($rawErrs as $errs) {
                    foreach ($errs as $err) {
                        $errors[] = sprintf('at request %d: %s', $index, $err);
                    }
                }
            }
        }

        if (!empty($errors)) {
            throw new BadRequestsException('Failed to create "CreateAlumniEmploymentRequest" from array.', $errors);
        }

        return $requests;
    }
}
