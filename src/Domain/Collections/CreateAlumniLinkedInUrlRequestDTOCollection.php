<?php


namespace MiamiOH\AlumniWebService\Domain\Collections;

use Illuminate\Validation\ValidationException;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniLinkedInUrlRequestDTO;
use MiamiOH\AlumniWebService\Exceptions\BadRequestsException;

class CreateAlumniLinkedInUrlRequestDTOCollection extends BaseModelCollection
{
    /**
     * @param array $data
     * @return self
     * @throws BadRequestsException
     */
    public static function createFromArray(array $data): self
    {
        $requests = new self();
        $errors = [];

        foreach ($data as $index => $requestData) {
            try {
                $requests->push(CreateAlumniLinkedInUrlRequestDTO::createFromArray($requestData));
            } catch (ValidationException $e) {
                $rawErrs = $e->errors();
                foreach ($rawErrs as $errs) {
                    foreach ($errs as $err) {
                        $errors[] = sprintf('at request %d: %s', $index, $err);
                    }
                }
            }
        }

        if (!empty($errors)) {
            throw new BadRequestsException('Failed to create "CreateAlumniLinkedInUrlRequest" from array.', $errors);
        }

        return $requests;
    }
}
