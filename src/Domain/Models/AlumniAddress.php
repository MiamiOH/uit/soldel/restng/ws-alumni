<?php

namespace MiamiOH\AlumniWebService\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

/**
 * Class AlumniAddress
 * @package MiamiOH\AlumniWebService\Domain\Models
 */
class AlumniAddress implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $addressType;
    /**
     * @var string|null
     */
    private $streetLine1;
    /**
     * @var string|null
     */
    private $streetLine2;
    /**
     * @var string|null
     */
    private $streetLine3;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string|null
     */
    private $state;
    /**
     * @var string|null
     */
    private $postalCode;
    /**
     * @var string|null
     */
    private $nationBannerCode;
    /**
     * @var string|null
     */
    private $nationDescription;
    /**
     * @var int
     */
    private $sequenceNumber;
    /**
     * @var string|null
     */
    private $status;
    /**
     * @var Carbon|null
     */
    private $from;
    /**
     * @var Carbon|null
     */
    private $to;
    /**
     * @var string|null
     */
    private $addressSourceCode;

    /**
     * AlumniAddress constructor.
     * @param string $id
     * @param int $pidm
     * @param string $addressType
     * @param string|null $streetLine1
     * @param string|null $streetLine2
     * @param string|null $streetLine3
     * @param string $city
     * @param string|null $state
     * @param string|null $postalCode
     * @param string|null $nationBannerCode
     * @param string|null $nationDescription
     * @param int $sequenceNumber
     * @param string|null $status
     * @param Carbon|null $from
     * @param Carbon|null $to
     * @param string|null $addressSourceCode
     */
    public function __construct(
        string $id,
        int $pidm,
        string $addressType,
        ?string $streetLine1,
        ?string $streetLine2,
        ?string $streetLine3,
        string $city,
        ?string $state,
        ?string $postalCode,
        ?string $nationBannerCode,
        ?string $nationDescription,
        int $sequenceNumber,
        ?string $status,
        ?Carbon $from,
        ?Carbon $to,
        ?string $addressSourceCode
    ) {
        $this->id = $id;
        $this->pidm = $pidm;
        $this->addressType = $addressType;
        $this->streetLine1 = $streetLine1;
        $this->streetLine2 = $streetLine2;
        $this->streetLine3 = $streetLine3;
        $this->city = $city;
        $this->state = $state;
        $this->postalCode = $postalCode;
        $this->nationBannerCode = $nationBannerCode;
        $this->nationDescription = $nationDescription;
        $this->sequenceNumber = $sequenceNumber;
        $this->status = $status;
        $this->from = $from;
        $this->to = $to;
        $this->addressSourceCode = $addressSourceCode;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }


    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getAddressType(): string
    {
        return $this->addressType;
    }

    /**
     * @return string|null
     */
    public function getStreetLine1(): ?string
    {
        return $this->streetLine1;
    }

    /**
     * @return string|null
     */
    public function getStreetLine2(): ?string
    {
        return $this->streetLine2;
    }

    /**
     * @return string|null
     */
    public function getStreetLine3(): ?string
    {
        return $this->streetLine3;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @return string|null
     */
    public function getNationBannerCode(): ?string
    {
        return $this->nationBannerCode;
    }

    /**
     * @return string|null
     */
    public function getNationDescription(): ?string
    {
        return $this->nationDescription;
    }

    /**
     * @return int
     */
    public function getSequenceNumber(): int
    {
        return $this->sequenceNumber;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return Carbon|null
     */
    public function getFrom(): ?Carbon
    {
        return $this->from;
    }

    /**
     * @return Carbon|null
     */
    public function getTo(): ?Carbon
    {
        return $this->to;
    }

    /**
     * @return string|null
     */
    public function getAddressSourceCode(): ?string
    {
        return $this->addressSourceCode;
    }

    /**
     * @return array
     */
    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'pidm' => $this->getPidm(),
            'addressType' => $this->getAddressType(),
            'streetLine1' => $this->getStreetLine1(),
            'streetLine2' => $this->getStreetLine2(),
            'streetLine3' => $this->getStreetLine3(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'postalCode' => $this->getPostalCode(),
            'nationBannerCode' => $this->getNationBannerCode(),
            'nationDescription' => $this->getNationDescription(),
            'sequenceNumber' => $this->getSequenceNumber(),
            'status' => $this->getStatus(),
            'from' => $this->getFrom() === null ? null : $this->getFrom()->format('Y-m-d'),
            'to' => $this->getTo() === null ? null : $this->getTo()->format('Y-m-d'),
            'addressSourceCode' => $this->getAddressSourceCode()
        ];
    }
}
