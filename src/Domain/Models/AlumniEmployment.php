<?php

namespace MiamiOH\AlumniWebService\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

/**
 * Class AlumniEmployment
 * @package MiamiOH\AlumniWebService\Domain\Models
 */
class AlumniEmployment implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $prospectId;
    /**
     * @var int
     */
    private $seq;
    /**
     * @var string|null
     */
    private $employer;
    /**
     * @var string|null
     */
    private $employerId;
    /**
     * @var string|null
     */
    private $position;
    /**
     * @var bool
     */
    private $isPrimary;
    /**
     * @var Carbon|null
     */
    private $from;
    /**
     * @var Carbon|null
     */
    private $to;
    /**
     * @var array
     */
    private $jobCategoryCodes;
    /**
     * @var string|null
     */
    private $standardIndustrialCode;
    /**
     * @var string|null
     */
    private $crossReferenceCode;
    /**
     * @var string|null
     */
    private $statusCode;
    /**
     * @var float|null
     */
    private $weeklyHours;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var AlumniAddress|null
     */
    private $address;
    /**
     * @var AlumniPhoneCollection|null
     */
    private $phones;
    /**
     * @var AlumniWorkEmailCollection|null
     */
    private $workEmails;
    /**
     * @var string
     */
    private $user;
    /**
     * @var Carbon
     */
    private $updatedAt;
    /**
     * @var bool
     */
    private $isOkForNotes;
    /**
     * @var bool
     */
    private $isDisplayedNotes;
    /**
     * @var bool
     */
    private $isMatchingGift;
    /**
     * @var bool
     */
    private $isReviewed;
    /**
     * @var string|null
     */
    private $reviewedBy;
    /**
     * @var bool
     */
    private $isCoop;
    /**
     * @var Carbon|null
     */
    private $noteDate;

  /**
   * AlumniEmployment constructor.
   * @param string $id
   * @param string $prospectId
   * @param int $seq
   * @param string|null $employer
   * @param string|null $employerId
   * @param string|null $position
   * @param bool $isPrimary
   * @param Carbon|null $from
   * @param Carbon|null $to
   * @param array $jobCategoryCodes
   * @param string|null $standardIndustrialCode
   * @param string|null $crossReferenceCode
   * @param string|null $statusCode
   * @param float|null $weeklyHours
   * @param string|null $comment
   * @param AlumniAddress|null $address
   * @param AlumniPhoneCollection|null $phones
   * @param AlumniWorkEmailCollection|null $workEmails
   * @param string $user
   * @param Carbon $updatedAt
   * @param bool $isOkForNotes
   * @param bool $isDisplayedNotes
   * @param bool $isMatchingGift
   * @param bool $isReviewed
   * @param string|null $reviewedBy
   * @param bool $isCoop
   * @param Carbon|null $noteDate
   */
  public function __construct(
    string $id,
    string $prospectId,
    int $seq,
    ?string $employer,
    ?string $employerId,
    ?string $position,
    bool $isPrimary,
    ?Carbon $from,
    ?Carbon $to,
    array $jobCategoryCodes,
    ?string $standardIndustrialCode,
    ?string $crossReferenceCode,
    ?string $statusCode,
    ?float $weeklyHours,
    ?string $comment,
    ?AlumniAddress $address,
    ?AlumniPhoneCollection $phones,
    ?AlumniWorkEmailCollection $workEmails,
    string $user,
    Carbon $updatedAt,
    bool $isOkForNotes,
    bool $isDisplayedNotes,
    bool $isMatchingGift,
    bool $isReviewed,
    ?string $reviewedBy,
    bool $isCoop,
    ?Carbon $noteDate
  ) {
    $this->id = $id;
    $this->prospectId = $prospectId;
    $this->seq = $seq;
    $this->employer = $employer;
    $this->employerId = $employerId;
    $this->position = $position;
    $this->isPrimary = $isPrimary;
    $this->from = $from;
    $this->to = $to;
    $this->jobCategoryCodes = $jobCategoryCodes;
    $this->standardIndustrialCode = $standardIndustrialCode;
    $this->crossReferenceCode = $crossReferenceCode;
    $this->statusCode = $statusCode;
    $this->weeklyHours = $weeklyHours;
    $this->comment = $comment;
    $this->address = $address;
    $this->phones = $phones;
    $this->workEmails = $workEmails;
    $this->user = $user;
    $this->updatedAt = $updatedAt;
    $this->isOkForNotes = $isOkForNotes;
    $this->isDisplayedNotes = $isDisplayedNotes;
    $this->isMatchingGift = $isMatchingGift;
    $this->isReviewed = $isReviewed;
    $this->reviewedBy = $reviewedBy;
    $this->isCoop = $isCoop;
    $this->noteDate = $noteDate;
  }


  public static function create(
        AlumniEmploymentDTO $employmentDTO,
        AlumniPhoneCollection $phones = null,
        AlumniWorkEmailCollection $emails = null,
        AlumniAddress $address = null
    ) {
        return new self(
            $employmentDTO->getId(),
            $employmentDTO->getProspectId(),
            $employmentDTO->getSeq(),
            $employmentDTO->getEmployer(),
            $employmentDTO->getEmployerId(),
            $employmentDTO->getPosition(),
            $employmentDTO->isPrimary(),
            $employmentDTO->getFrom(),
            $employmentDTO->getTo(),
            $employmentDTO->getJobCategoryCodes(),
            $employmentDTO->getStandardIndustrialCode(),
            $employmentDTO->getCrossReferenceCode(),
            $employmentDTO->getStatusCode(),
            $employmentDTO->getWeeklyHours(),
            $employmentDTO->getComment(),
            $address,
            $phones,
            $emails,
            $employmentDTO->getUser(),
            $employmentDTO->getUpdatedAt(),
            $employmentDTO->isOkForNotes(),
            $employmentDTO->isDisplayedNotes(),
            $employmentDTO->isMatchingGift(),
            $employmentDTO->isReviewed(),
            $employmentDTO->getReviewedBy(),
            $employmentDTO->isCoop(),
            $employmentDTO->getNoteDate()
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSeq(): int
    {
        return $this->seq;
    }

    /**
     * @return string
     */
    public function getProspectId(): string
    {
        return $this->prospectId;
    }

    /**
     * @return string|null
     */
    public function getEmployer(): ?string
    {
        return $this->employer;
    }

    /**
     * @return string|null
     */
    public function getEmployerId(): ?string
    {
        return $this->employerId;
    }

    /**
     * @return string|null
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }

    /**
     * @return Carbon|null
     */
    public function getFrom(): ?Carbon
    {
        return $this->from;
    }

    /**
     * @return Carbon|null
     */
    public function getTo(): ?Carbon
    {
        return $this->to;
    }

    /**
     * @return array
     */
    public function getJobCategoryCodes(): array
    {
        return $this->jobCategoryCodes;
    }

    /**
     * @return string|null
     */
    public function getStandardIndustrialCode(): ?string
    {
        return $this->standardIndustrialCode;
    }

    /**
     * @return string|null
     */
    public function getCrossReferenceCode(): ?string
    {
        return $this->crossReferenceCode;
    }

    /**
     * @return string|null
     */
    public function getStatusCode(): ?string
    {
        return $this->statusCode;
    }

    /**
     * @return float|null
     */
    public function getWeeklyHours(): ?float
    {
        return $this->weeklyHours;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return AlumniAddress|null
     */
    public function getAddress(): ?AlumniAddress
    {
        return $this->address;
    }

    /**
     * @return AlumniPhoneCollection|null
     */
    public function getPhones(): ?AlumniPhoneCollection
    {
        return $this->phones;
    }

    /**
     * @return AlumniWorkEmailCollection|null
     */
    public function getWorkEmails(): ?AlumniWorkEmailCollection
    {
        return $this->workEmails;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function isOkForNotes(): bool
    {
        return $this->isOkForNotes;
    }

    /**
     * @return bool
     */
    public function isDisplayedNotes(): bool
    {
        return $this->isDisplayedNotes;
    }

    /**
     * @return bool
     */
    public function isMatchingGift(): bool
    {
        return $this->isMatchingGift;
    }

    /**
     * @return bool
     */
    public function isReviewed(): bool
    {
        return $this->isReviewed;
    }

    /**
     * @return string|null
     */
    public function getReviewedBy(): ?string
    {
        return $this->reviewedBy;
    }

    /**
     * @return bool
     */
    public function isCoop(): bool
    {
        return $this->isCoop;
    }

    /**
     * @return Carbon|null
     */
    public function getNoteDate(): ?Carbon
    {
        return $this->noteDate;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'prospectId' => $this->getProspectId(),
            'sequence' => $this->getSeq(),
            'employer' => $this->getEmployer(),
            'employerId' => $this->getEmployerId(),
            'position' => $this->getPosition(),
            'isPrimary' => $this->isPrimary(),
            'from' => $this->getFrom() ? $this->getFrom()->format('Y-m-d') : null,
            'to' => $this->getTo() ? $this->getTo()->format('Y-m-d') : null,
            'jobCategoryCodes' => $this->getJobCategoryCodes(),
            'standardIndustrialCode' => $this->getStandardIndustrialCode(),
            'crossReferenceCode' => $this->getCrossReferenceCode(),
            'statusCode' => $this->getStatusCode(),
            'weeklyHours' => $this->getWeeklyHours(),
            'comment' => $this->getComment(),
            'address' => $this->getAddress() ? $this->getAddress()->toJsonArray() : null,
            'phones' => $this->getPhones() ? $this->getPhones()->toJsonArray() : null,
            'workEmails' => $this->getWorkEmails() ? $this->getWorkEmails()->toJsonArray() : null,
            'user' => $this->getUser(),
            'updatedAt' => $this->getUpdatedAt()->format('Y-m-d H:i:s'),
            'isOkForNotes' => $this->isOkForNotes(),
            'isDisplayedNotes' => $this->isDisplayedNotes(),
            'isMatchingGift' => $this->isMatchingGift(),
            'isReviewed' => $this->isReviewed(),
            'reviewedBy' => $this->getReviewedBy(),
            'isCoop' => $this->isCoop(),
            'noteDate' => $this->getNoteDate() ? $this->getNoteDate()->format('Y-m-d') : null,
        ];
    }
}
