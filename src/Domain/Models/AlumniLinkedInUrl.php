<?php

namespace MiamiOH\AlumniWebService\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class AlumniLinkedInUrl implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $prospectId;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * AlumniLinkedInUrl constructor.
     * @param string $id
     * @param string $prospectId
     * @param string $url
     * @param string|null $comment
     * @param Carbon $updatedAt
     */
    public function __construct(string $id, string $prospectId, string $url, ?string $comment, Carbon $updatedAt)
    {
        $this->id = $id;
        $this->prospectId = $prospectId;
        $this->url = $url;
        $this->comment = $comment;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProspectId(): string
    {
        return $this->prospectId;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'prospectId' => $this->getProspectId(),
            'url' => $this->getUrl(),
            'comment' => $this->getComment(),
            'updatedAt' => $this->getUpdatedAt()->format('Y-m-d H:i:s')
        ];
    }
}
