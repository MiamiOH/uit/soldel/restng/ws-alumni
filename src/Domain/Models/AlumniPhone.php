<?php

namespace MiamiOH\AlumniWebService\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class AlumniPhone implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $phoneType;
    /**
     * @var string
     */
    private $phoneDesc;
    /**
     * @var bool
     */
    private $isPrimary;
    /**
     * @var string|null
     */
    private $areaPart;
    /**
     * @var string|null
     */
    private $numberPart;
    /**
     * @var string
     */
    private $phoneNumber;
    /**
     * @var string|null
     */
    private $ext;
    /**
     * @var string
     */
    private $phoneNumberNational;
    /**
     * @var string
     */
    private $phoneNumberInternational;
    /**
     * @var int
     */
    private $sequenceNumber;
    /**
     * @var string
     */
    private $status;
    /**
     * @var Carbon
     */
    private $activityDate;

    /**
     * AlumniPhone constructor.
     * @param string $id
     * @param int $pidm
     * @param string $phoneType
     * @param string $phoneDesc
     * @param bool $isPrimary
     * @param string|null $areaPart
     * @param string|null $numberPart
     * @param string $phoneNumber
     * @param string|null $ext
     * @param string $phoneNumberNational
     * @param string $phoneNumberInternational
     * @param int $sequenceNumber
     * @param string $status
     * @param Carbon $activityDate
     */
    public function __construct(string $id, int $pidm, string $phoneType, string $phoneDesc, bool $isPrimary, ?string $areaPart, ?string $numberPart, string $phoneNumber, ?string $ext, string $phoneNumberNational, string $phoneNumberInternational, int $sequenceNumber, string $status, Carbon $activityDate)
    {
        $this->id = $id;
        $this->pidm = $pidm;
        $this->phoneType = $phoneType;
        $this->phoneDesc = $phoneDesc;
        $this->isPrimary = $isPrimary;
        $this->areaPart = $areaPart;
        $this->numberPart = $numberPart;
        $this->phoneNumber = $phoneNumber;
        $this->ext = $ext;
        $this->phoneNumberNational = $phoneNumberNational;
        $this->phoneNumberInternational = $phoneNumberInternational;
        $this->sequenceNumber = $sequenceNumber;
        $this->status = $status;
        $this->activityDate = $activityDate;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getPhoneType(): string
    {
        return $this->phoneType;
    }

    /**
     * @return string
     */
    public function getPhoneDesc(): string
    {
        return $this->phoneDesc;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }

    /**
     * @return string|null
     */
    public function getAreaPart(): ?string
    {
        return $this->areaPart;
    }

    /**
     * @return string|null
     */
    public function getNumberPart(): ?string
    {
        return $this->numberPart;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @return string|null
     */
    public function getExt(): ?string
    {
        return $this->ext;
    }

    /**
     * @return string
     */
    public function getPhoneNumberNational(): string
    {
        return $this->phoneNumberNational;
    }

    /**
     * @return string
     */
    public function getPhoneNumberInternational(): string
    {
        return $this->phoneNumberInternational;
    }

    /**
     * @return int
     */
    public function getSequenceNumber(): int
    {
        return $this->sequenceNumber;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return Carbon
     */
    public function getActivityDate(): Carbon
    {
        return $this->activityDate;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'pidm' => $this->getPidm(),
            'phoneType' => $this->getPhoneType(),
            'phoneDesc' => $this->getPhoneDesc(),
            'isPrimary' => $this->isPrimary(),
            'areaPart' => $this->getAreaPart(),
            'numberPart' => $this->getNumberPart(),
            'phoneNumber' => $this->getPhoneNumber(),
            'ext' => $this->getExt(),
            'phoneNumberNational' => $this->getPhoneNumberNational(),
            'phoneNumberInternational' => $this->getPhoneNumberInternational(),
            'sequenceNumber' => $this->getSequenceNumber(),
            'status' => $this->getStatus(),
            'activityDate' => $this->getActivityDate()->format('Y-m-d H:i:s'),
        ];
    }
}
