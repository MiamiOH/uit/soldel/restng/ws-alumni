<?php

namespace MiamiOH\AlumniWebService\Domain\Repositories;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;

interface AlumniAddressRepository
{
    public function getByPidmAndType(int $pidm, string $type): AlumniAddressCollection;

    public function getByPidmAndTypes(int $pidm, array $types): AlumniAddressCollection;

    public function create(CreateAlumniAddressRequest $request): AlumniAddress;

    public function delete(string $id): void;
}
