<?php

namespace MiamiOH\AlumniWebService\Domain\Repositories;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniEmploymentDTOCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmploymentDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest;

interface AlumniEmploymentRepository
{
    public function create(CreateAlumniEmploymentRequest $request): AlumniEmploymentDTO;

    public function get(int $pidm): AlumniEmploymentDTOCollection;
}
