<?php

namespace MiamiOH\AlumniWebService\Domain\Repositories;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniPhone;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;

interface AlumniPhoneRepository
{
    public function getByPidmAndType(int $pidm, string $type): AlumniPhoneCollection;

    public function getByPidmAndTypes(int $pidm, array $types): AlumniPhoneCollection;

    public function create(CreateAlumniPhoneRequest $request): AlumniPhone;

    public function delete(string $id): void;
}
