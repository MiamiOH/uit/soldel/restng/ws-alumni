<?php


namespace MiamiOH\AlumniWebService\Domain\Repositories;

interface AlumniRepository
{
    public function getPidmByProspectId(string $prospectId): ?int;
}
