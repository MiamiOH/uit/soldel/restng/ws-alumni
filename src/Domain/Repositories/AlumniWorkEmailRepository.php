<?php

namespace MiamiOH\AlumniWebService\Domain\Repositories;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;

interface AlumniWorkEmailRepository
{
    public function getByPidmAndType(int $pidm, string $type): AlumniWorkEmailCollection;

    public function create(CreateAlumniWorkEmailRequest $request): AlumniWorkEmail;

    public function delete(string $id): void;

    public function update(UpdateEmailRequest $request): AlumniWorkEmail;
}
