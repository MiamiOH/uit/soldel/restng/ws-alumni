<?php

namespace MiamiOH\AlumniWebService\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

/**
 * Class CreateAlumniAddressRequest
 * @package MiamiOH\AlumniWebService\Domain\Requests
 */
class CreateAlumniAddressRequest implements Jsonable
{
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $type;
    /**
     * @var Carbon|null
     */
    private $from;
    /**
     * @var Carbon|null
     */
    private $to;
    /**
     * @var bool
     */
    private $isPreferred;
    /**
     * @var string|null
     */
    private $streetLine1;
    /**
     * @var string|null
     */
    private $streetLine2;
    /**
     * @var string|null
     */
    private $streetLine3;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string|null
     */
    private $state;
    /**
     * @var string|null
     */
    private $zip;
    /**
     * @var string|null
     */
    private $addressSourceCode;

    /**
     * CreateAlumniAddressRequest constructor.
     * @param int $pidm
     * @param string $type
     * @param Carbon|null $from
     * @param Carbon|null $to
     * @param bool $isPreferred
     * @param string|null $streetLine1
     * @param string|null $streetLine2
     * @param string|null $streetLine3
     * @param string $city
     * @param string|null $state
     * @param string|null $zip
     * @param string|null $addressSourceCode
     */
    public function __construct(
        int $pidm,
        string $type,
        ?Carbon $from,
        ?Carbon $to,
        bool $isPreferred,
        ?string $streetLine1,
        ?string $streetLine2,
        ?string $streetLine3,
        string $city,
        ?string $state,
        ?string $zip,
        ?string $addressSourceCode
    ) {
        $this->pidm = $pidm;
        $this->type = $type;
        $this->from = $from;
        $this->to = $to;
        $this->isPreferred = $isPreferred;
        $this->streetLine1 = $streetLine1;
        $this->streetLine2 = $streetLine2;
        $this->streetLine3 = $streetLine3;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
        $this->addressSourceCode = $addressSourceCode;
    }


    /**
     * @param int $pidm
     * @param CreateAlumniAddressRequestDTO $dto
     * @return self
     */
    public static function createFromDTO(int $pidm, CreateAlumniAddressRequestDTO $dto): self
    {
        return new self(
            $pidm,
            $dto->getType(),
            $dto->getFrom(),
            $dto->getTo(),
            $dto->isPreferred(),
            $dto->getStreetLine1(),
            $dto->getStreetLine2(),
            $dto->getStreetLine3(),
            $dto->getCity(),
            $dto->getState(),
            $dto->getZip(),
            $dto->getAddressSourceCode()
        );
    }

    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Carbon|null
     */
    public function getFrom(): ?Carbon
    {
        return $this->from;
    }

    /**
     * @return Carbon|null
     */
    public function getTo(): ?Carbon
    {
        return $this->to;
    }

    /**
     * @return bool
     */
    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    /**
     * @return string|null
     */
    public function getStreetLine1(): ?string
    {
        return $this->streetLine1;
    }

    /**
     * @return string|null
     */
    public function getStreetLine2(): ?string
    {
        return $this->streetLine2;
    }

    /**
     * @return string|null
     */
    public function getStreetLine3(): ?string
    {
        return $this->streetLine3;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @return string|null
     */
    public function getAddressSourceCode(): ?string
    {
        return $this->addressSourceCode;
    }

    /**
     * @return array
     */
    public function toJsonArray(): array
    {
        return [
            'pidm' => $this->getPidm(),
            'type' => $this->getType(),
            'from' => $this->getFrom() ? $this->getFrom()->format('Y-m-d') : null,
            'to' => $this->getTo() ? $this->getTo()->format('Y-m-d') : null,
            'isPreferred' => $this->isPreferred(),
            'streetLine1' => $this->getStreetLine1(),
            'streetLine2' => $this->getStreetLine2(),
            'streetLine3' => $this->getStreetLine3(),
            'city' => $this->getCity(),
            'state' => $this->getState(),
            'zip' => $this->getZip(),
            'addressSourceCode' => $this->getAddressSourceCode()
        ];
    }
}
