<?php

namespace MiamiOH\AlumniWebService\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class CreateAlumniEmploymentRequestDTO implements Jsonable
{
    /**
     * @var string
     */
    private $prospectId;
    /**
     * @var string|null
     */
    private $employer;
    /**
     * @var string|null
     */
    private $employerId;
    /**
     * @var string|null
     */
    private $position;
    /**
     * @var bool
     */
    private $isPrimary;
    /**
     * @var Carbon|null
     */
    private $from;
    /**
     * @var Carbon|null
     */
    private $to;
    /**
     * @var float|null
     */
    private $weeklyHours;
    /**
     * @var string|null
     */
    private $statusCode;
    /**
     * @var array
     */
    private $jobCategoryCodes;
    /**
     * @var string|null
     */
    private $standardIndustrialCode;
    /**
     * @var string|null
     */
    private $crossReferenceCode;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var bool
     */
    private $isOkForNotes;
    /**
     * @var bool
     */
    private $isDisplayedNotes;
    /**
     * @var bool
     */
    private $isMatchingGift;
    /**
     * @var bool
     */
    private $isReviewed;
    /**
     * @var string|null
     */
    private $reviewedBy;
    /**
     * @var bool
     */
    private $isCoop;
    /**
     * @var Carbon|null
     */
    private $noteDate;
    /**
     * @var CreateAlumniWorkEmailRequestDTO|null
     */
    private $createWorkEmailRequest;
    /**
     * @var CreateAlumniPhoneRequestDTO|null
     */
    private $createPhoneRequest;
    /**
     * @var CreateAlumniAddressRequestDTO|null
     */
    private $createAddressRequest;
    /**
     * @var string
     */
    private $createdBy;

    /**
     * CreateAlumniEmploymentRequestDTO constructor.
     * @param string $prospectId
     * @param string|null $employer
     * @param string|null $employerId
     * @param string|null $position
     * @param bool $isPrimary
     * @param Carbon|null $from
     * @param Carbon|null $to
     * @param float|null $weeklyHours
     * @param string|null $statusCode
     * @param array $jobCategoryCodes
     * @param string|null $standardIndustrialCode
     * @param string|null $crossReferenceCode
     * @param string|null $comment
     * @param bool $isOkForNotes
     * @param bool $isDisplayedNotes
     * @param bool $isMatchingGift
     * @param bool $isReviewed
     * @param string|null $reviewedBy
     * @param bool $isCoop
     * @param Carbon|null $noteDate
     * @param CreateAlumniWorkEmailRequestDTO|null $createWorkEmailRequest
     * @param CreateAlumniPhoneRequestDTO|null $createPhoneRequest
     * @param CreateAlumniAddressRequestDTO|null $createAddressRequest
     * @param string $createdBy
     */
    public function __construct(string $prospectId, ?string $employer, ?string $employerId, ?string $position, bool $isPrimary, ?Carbon $from, ?Carbon $to, ?float $weeklyHours, ?string $statusCode, array $jobCategoryCodes, ?string $standardIndustrialCode, ?string $crossReferenceCode, ?string $comment, bool $isOkForNotes, bool $isDisplayedNotes, bool $isMatchingGift, bool $isReviewed, ?string $reviewedBy, bool $isCoop, ?Carbon $noteDate, ?CreateAlumniWorkEmailRequestDTO $createWorkEmailRequest, ?CreateAlumniPhoneRequestDTO $createPhoneRequest, ?CreateAlumniAddressRequestDTO $createAddressRequest, string $createdBy)
    {
        $this->prospectId = $prospectId;
        $this->employer = $employer;
        $this->employerId = $employerId;
        $this->position = $position;
        $this->isPrimary = $isPrimary;
        $this->from = $from;
        $this->to = $to;
        $this->weeklyHours = $weeklyHours;
        $this->statusCode = $statusCode;
        $this->jobCategoryCodes = $jobCategoryCodes;
        $this->standardIndustrialCode = $standardIndustrialCode;
        $this->crossReferenceCode = $crossReferenceCode;
        $this->comment = $comment;
        $this->isOkForNotes = $isOkForNotes;
        $this->isDisplayedNotes = $isDisplayedNotes;
        $this->isMatchingGift = $isMatchingGift;
        $this->isReviewed = $isReviewed;
        $this->reviewedBy = $reviewedBy;
        $this->isCoop = $isCoop;
        $this->noteDate = $noteDate;
        $this->createWorkEmailRequest = $createWorkEmailRequest;
        $this->createPhoneRequest = $createPhoneRequest;
        $this->createAddressRequest = $createAddressRequest;
        $this->createdBy = $createdBy;
    }

    public static function createFromArray(array $data): self
    {
        RESTngValidatorFactory::make($data, [
            'prospectId' => 'bail|required|string|regex:/^A\\d+$/',
            'employer' => 'bail|nullable|string',
            'employerId' => 'bail|nullable|string',
            'position' => 'bail|nullable|string',
            'isPrimary' => 'bail|nullable|boolean',
            'from' => 'bail|nullable|date_format:Y-m-d',
            'to' => 'bail|nullable|date_format:Y-m-d',
            'weeklyHours' => 'bail|nullable|numeric',
            'statusCode' => 'bail|nullable|string',
            'jobCategoryCodes' => 'bail|nullable|array',
            'jobCategoryCodes.*' => 'bail|required|string',
            'standardIndustrialCode' => 'bail|nullable|string',
            'crossReferenceCode' => 'bail|nullable|string',
            'comment' => 'bail|nullable|string',
            'isOkForNotes' => 'bail|nullable|boolean',
            'isDisplayedNotes' => 'bail|nullable|boolean',
            'isMatchingGift' => 'bail|nullable|boolean',
            'isReviewed' => 'bail|nullable|boolean',
            'reviewedBy' => 'bail|nullable|string',
            'isCoop' => 'bail|nullable|boolean',
            'noteDate' => 'bail|nullable|date_format:Y-m-d',
            'workEmail.email' => 'bail|nullable|email',
            'workEmail.isPreferred' => 'bail|nullable|boolean',
            'workEmail.comment' => 'bail|nullable|string',
            'phone.type' => 'bail|nullable|string',
            'phone.areaCode' => 'bail|nullable|string',
            'phone.number' => 'bail|nullable|string',
            'phone.extension' => 'bail|nullable|string',
            'address.type' => 'bail|nullable|string',
            'address.from' => 'bail|nullable|date_format:Y-m-d',
            'address.to' => 'bail|nullable|date_format:Y-m-d',
            'address.isPreferred' => 'bail|nullable|boolean',
            'address.streetLine1' => 'bail|nullable|string',
            'address.streetLine2' => 'bail|nullable|string',
            'address.streetLine3' => 'bail|nullable|string',
            'address.city' => 'bail|nullable|string',
            'address.state' => 'bail|nullable|string',
            'address.zip' => 'bail|nullable|string',
            'createdBy' => 'bail|required|string'
        ])->validate();

        return new self(
            $data['prospectId'],
            $data['employer'] ?? null,
            $data['employerId'] ?? null,
            $data['position'] ?? null,
            $data['isPrimary'],
            $data['from'] ? Carbon::createFromFormat('Y-m-d', $data['from']) : null,
            $data['to'] ? Carbon::createFromFormat('Y-m-d', $data['to']) : null,
            $data['weeklyHours'] ?? null,
            $data['statusCode'] ?? null,
            is_array($data['jobCategoryCodes']) ? array_unique($data['jobCategoryCodes']) : [],
            $data['standardIndustrialCode'] ?? null,
            $data['crossReferenceCode'] ?? null,
            $data['comment'] ?? null,
            $data['isOkForNotes'] ?? false,
            $data['isDisplayedNotes'] ?? false,
            $data['isMatchingGift'] ?? false,
            $data['isReviewed'] ?? false,
            $data['reviewedBy'] ?? null,
            $data['isCoop'] ?? false,
            $data['noteDate'] ? Carbon::createFromFormat('Y-m-d', $data['noteDate']) : null,
            $data['workEmail'] ? new CreateAlumniWorkEmailRequestDTO(
                'WRKE',
                $data['workEmail']['email'],
                $data['workEmail']['isPreferred'],
                $data['workEmail']['comment'] ?? null,
                true,
                true,
                $data['createdBy']
            ) : null,
            $data['phone'] ? new CreateAlumniPhoneRequestDTO(
                $data['phone']['type'],
                $data['phone']['areaCode'] ?? null,
                $data['phone']['number'] ?? null,
                $data['phone']['extension'] ?? null
            ) : null,
            $data['address'] ? new CreateAlumniAddressRequestDTO(
                $data['address']['type'],
                $data['address']['from'] ? Carbon::createFromFormat('Y-m-d', $data['address']['from']) : null,
                $data['address']['to'] ? Carbon::createFromFormat('Y-m-d', $data['address']['to']) : null,
                $data['address']['isPreferred'],
                $data['address']['streetLine1'] ?? null,
                $data['address']['streetLine2'] ?? null,
                $data['address']['streetLine3'] ?? null,
                $data['address']['city'],
                $data['address']['state'] ?? null,
                $data['address']['zip'] ?? null,
                $data['address']['addressSourceCode'] ?? null
            ) : null,
            $data['createdBy']
        );
    }

    /**
     * @return string
     */
    public function getProspectId(): string
    {
        return $this->prospectId;
    }

    /**
     * @return string|null
     */
    public function getEmployer(): ?string
    {
        return $this->employer;
    }

    /**
     * @return string|null
     */
    public function getEmployerId(): ?string
    {
        return $this->employerId;
    }

    /**
     * @return string|null
     */
    public function getPosition(): ?string
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->isPrimary;
    }

    /**
     * @return Carbon|null
     */
    public function getFrom(): ?Carbon
    {
        return $this->from;
    }

    /**
     * @return Carbon|null
     */
    public function getTo(): ?Carbon
    {
        return $this->to;
    }

    /**
     * @return float|null
     */
    public function getWeeklyHours(): ?float
    {
        return $this->weeklyHours;
    }

    /**
     * @return string|null
     */
    public function getStatusCode(): ?string
    {
        return $this->statusCode;
    }

    /**
     * @return array
     */
    public function getJobCategoryCodes(): array
    {
        return $this->jobCategoryCodes;
    }

    /**
     * @return string|null
     */
    public function getStandardIndustrialCode(): ?string
    {
        return $this->standardIndustrialCode;
    }

    /**
     * @return string|null
     */
    public function getCrossReferenceCode(): ?string
    {
        return $this->crossReferenceCode;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return bool
     */
    public function isOkForNotes(): bool
    {
        return $this->isOkForNotes;
    }

    /**
     * @return bool
     */
    public function isDisplayedNotes(): bool
    {
        return $this->isDisplayedNotes;
    }

    /**
     * @return bool
     */
    public function isMatchingGift(): bool
    {
        return $this->isMatchingGift;
    }

    /**
     * @return bool
     */
    public function isReviewed(): bool
    {
        return $this->isReviewed;
    }

    /**
     * @return string|null
     */
    public function getReviewedBy(): ?string
    {
        return $this->reviewedBy;
    }

    /**
     * @return bool
     */
    public function isCoop(): bool
    {
        return $this->isCoop;
    }

    /**
     * @return Carbon|null
     */
    public function getNoteDate(): ?Carbon
    {
        return $this->noteDate;
    }

    /**
     * @return CreateAlumniWorkEmailRequestDTO|null
     */
    public function getCreateWorkEmailRequest(): ?CreateAlumniWorkEmailRequestDTO
    {
        return $this->createWorkEmailRequest;
    }

    /**
     * @return CreateAlumniPhoneRequestDTO|null
     */
    public function getCreatePhoneRequest(): ?CreateAlumniPhoneRequestDTO
    {
        return $this->createPhoneRequest;
    }

    /**
     * @return CreateAlumniAddressRequestDTO|null
     */
    public function getCreateAddressRequest(): ?CreateAlumniAddressRequestDTO
    {
        return $this->createAddressRequest;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function toJsonArray(): array
    {
        return [
            'prospectId' => $this->getProspectId(),
            'employer' => $this->getEmployer(),
            'employerId' => $this->getEmployerId(),
            'position' => $this->getPosition(),
            'isPrimary' => $this->isPrimary(),
            'from' => $this->getFrom() ? $this->getFrom()->format('Y-m-d') : null,
            'to' => $this->getTo() ? $this->getTo()->format('Y-m-d') : null,
            'weeklyHours' => $this->getWeeklyHours(),
            'statusCode' => $this->getStatusCode(),
            'jobCategoryCodes' => $this->getJobCategoryCodes(),
            'standardIndustrialCode' => $this->getStandardIndustrialCode(),
            'crossReferenceCode' => $this->getCrossReferenceCode(),
            'comment' => $this->getComment(),
            'isOkForNotes' => $this->isOkForNotes(),
            'isDisplayedNotes' => $this->isDisplayedNotes(),
            'isMatchingGift' => $this->isMatchingGift(),
            'isReviewed' => $this->isReviewed(),
            'reviewedBy' => $this->getReviewedBy(),
            'isCoop' => $this->isCoop(),
            'noteDate' => $this->getNoteDate() ? $this->getNoteDate()->format('Y-m-d') : null,
            'createWorkEmailRequest' => $this->getCreateWorkEmailRequest() ? $this->getCreateWorkEmailRequest()->toJsonArray() : null,
            'createPhoneRequest' => $this->getCreatePhoneRequest() ? $this->getCreatePhoneRequest()->toJsonArray() : null,
            'createAddressRequest' => $this->getCreateAddressRequest() ? $this->getCreateAddressRequest()->toJsonArray() : null,
            'createdBy' => $this->getCreatedBy()
        ];
    }
}
