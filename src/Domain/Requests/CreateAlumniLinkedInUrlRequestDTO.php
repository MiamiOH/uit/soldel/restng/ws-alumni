<?php

namespace MiamiOH\AlumniWebService\Domain\Requests;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class CreateAlumniLinkedInUrlRequestDTO implements Jsonable
{
    /**
     * @var string
     */
    private $prospectId;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var string
     */
    private $createdBy;

    /**
     * CreateAlumniLinkedInUrlRequestDTO constructor.
     * @param string $prospectId
     * @param string $url
     * @param string|null $comment
     * @param string $createdBy
     */
    public function __construct(string $prospectId, string $url, ?string $comment, string $createdBy)
    {
        $this->prospectId = $prospectId;
        $this->url = $url;
        $this->comment = $comment;
        $this->createdBy = $createdBy;
    }

    public static function createFromArray(array $data): self
    {
        RESTngValidatorFactory::make($data, [
            'prospectId' => 'bail|required|string',
            'url' => 'bail|required|url',
            'comment' => 'bail|nullable|string',
            'createdBy' => 'bail|required|string'
        ])->validate();

        return new self(
            $data['prospectId'],
            $data['url'],
            $data['comment'],
            $data['createdBy']
        );
    }

    /**
     * @return string
     */
    public function getProspectId(): string
    {
        return $this->prospectId;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function toJsonArray(): array
    {
        return [
            'prospectId' => $this->getProspectId(),
            'url' => $this->getUrl(),
            'comment' => $this->getComment(),
            'createdBy' => $this->getCreatedBy()
        ];
    }
}
