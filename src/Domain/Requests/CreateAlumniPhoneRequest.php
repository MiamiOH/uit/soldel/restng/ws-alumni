<?php

namespace MiamiOH\AlumniWebService\Domain\Requests;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class CreateAlumniPhoneRequest implements Jsonable
{
    /**
     * @var int
     */
    private $pidm;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string|null
     */
    private $areaCode;
    /**
     * @var string|null
     */
    private $number;
    /**
     * @var string|null
     */
    private $extension;

    /**
     * CreateAlumniPhoneRequest constructor.
     * @param int $pidm
     * @param string $type
     * @param string|null $areaCode
     * @param string|null $number
     * @param string|null $extension
     */
    public function __construct(int $pidm, string $type, ?string $areaCode, ?string $number, ?string $extension)
    {
        $this->pidm = $pidm;
        $this->type = $type;
        $this->areaCode = $areaCode;
        $this->number = $number;
        $this->extension = $extension;
    }

    public static function createFromDTO(int $pidm, CreateAlumniPhoneRequestDTO $dto): self
    {
        return new self(
            $pidm,
            $dto->getType(),
            $dto->getAreaCode(),
            $dto->getNumber(),
            $dto->getExtension()
        );
    }

    /**
     * @return int
     */
    public function getPidm(): int
    {
        return $this->pidm;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getAreaCode(): ?string
    {
        return $this->areaCode;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function toJsonArray(): array
    {
        return [
            'pidm' => $this->getPidm(),
            'type' => $this->getType(),
            'areaCode' => $this->getAreaCode(),
            'number' => $this->getNumber(),
            'extension' => $this->getExtension()
        ];
    }
}
