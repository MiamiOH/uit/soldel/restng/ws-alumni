<?php

namespace MiamiOH\AlumniWebService\Domain\Requests;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class CreateAlumniPhoneRequestDTO implements Jsonable
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var string|null
     */
    private $areaCode;
    /**
     * @var string|null
     */
    private $number;
    /**
     * @var string|null
     */
    private $extension;

    /**
     * CreateAlumniPhoneRequest constructor.
     * @param string $type
     * @param string|null $areaCode
     * @param string|null $number
     * @param string|null $extension
     */
    public function __construct(string $type, ?string $areaCode, ?string $number, ?string $extension)
    {
        $this->type = $type;
        $this->areaCode = $areaCode;
        $this->number = $number;
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getAreaCode(): ?string
    {
        return $this->areaCode;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function toJsonArray(): array
    {
        return [
            'type' => $this->getType(),
            'areaCode' => $this->getAreaCode(),
            'number' => $this->getNumber(),
            'extension' => $this->getExtension()
        ];
    }
}
