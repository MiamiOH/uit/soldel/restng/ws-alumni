<?php


namespace MiamiOH\AlumniWebService\Domain\Requests;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class CreateAlumniWorkEmailRequestDTO implements Jsonable
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $email;
    /**
     * @var bool
     */
    private $isPreferred;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var bool
     */
    private $isActive;
    /**
     * @var bool
     */
    private $isDispWeb;
    /**
     * @var string
     */
    private $createdBy;

    /**
     * CreateAlumniWorkEmailRequestDTO constructor.
     * @param string $type
     * @param string $email
     * @param bool $isPreferred
     * @param string|null $comment
     * @param bool $isActive
     * @param bool $isDispWeb
     * @param string $createdBy
     */
    public function __construct(string $type, string $email, bool $isPreferred, ?string $comment, bool $isActive, bool $isDispWeb, string $createdBy)
    {
        $this->type = $type;
        $this->email = $email;
        $this->isPreferred = $isPreferred;
        $this->comment = $comment;
        $this->isActive = $isActive;
        $this->isDispWeb = $isDispWeb;
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return bool
     */
    public function isDispWeb(): bool
    {
        return $this->isDispWeb;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function toJsonArray(): array
    {
        return [
            'type' => $this->getType(),
            'email' => $this->getEmail(),
            'isPreferred' => $this->isPreferred(),
            'comment' => $this->getComment(),
            'isActive' => $this->isActive(),
            'isDispWeb' => $this->isDispWeb(),
            'createdBy' => $this->getCreatedBy()
        ];
    }
}
