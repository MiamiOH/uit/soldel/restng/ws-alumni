<?php


namespace MiamiOH\AlumniWebService\Domain\Requests;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class UpdateAlumniLinkedInUrlRequest implements Jsonable
{
    /**
     * @var string
     */
    private $prospectId;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string|null
     */
    private $url;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var string
     */
    private $updatedBy;

    /**
     * UpdateAlumniLinkedInUrlRequest constructor.
     * @param string $prospectId
     * @param string $id
     * @param string|null $url
     * @param string|null $comment
     * @param string $updatedBy
     */
    public function __construct(string $prospectId, string $id, ?string $url, ?string $comment, string $updatedBy)
    {
        $this->prospectId = $prospectId;
        $this->id = $id;
        $this->url = $url;
        $this->comment = $comment;
        $this->updatedBy = $updatedBy;
    }

    public static function createFromArray(array $data): self
    {
        RESTngValidatorFactory::make($data, [
            'prospectId' => 'bail|required|string',
            'id' => 'bail|required|string',
            'url' => 'bail|nullable|url',
            'comment' => 'bail|nullable|string',
            'updatedBy' => 'bail|required|string',
        ])->validate();

        return new self(
            $data['prospectId'],
            $data['id'],
            $data['url'] ?? null,
            $data['comment'] ?? null,
            $data['updatedBy'],
        );
    }

    /**
     * @return string
     */
    public function getProspectId(): string
    {
        return $this->prospectId;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getUpdatedBy(): string
    {
        return $this->updatedBy;
    }

    public function toJsonArray(): array
    {
        return [
            'prospectId' => $this->getProspectId(),
            'id' => $this->getId(),
            'url' => $this->getUrl(),
            'comment' => $this->getComment(),
            'updatedBy' => $this->getUpdatedBy()
        ];
    }
}
