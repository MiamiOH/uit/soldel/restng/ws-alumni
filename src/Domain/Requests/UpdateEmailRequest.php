<?php


namespace MiamiOH\AlumniWebService\Domain\Requests;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class UpdateEmailRequest implements Jsonable
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var bool
     */
    private $isPreferred;
    /**
     * @var string|null
     */
    private $comment;
    /**
     * @var string
     */
    private $updatedBy;

    /**
     * UpdateEmailRequest constructor.
     * @param string $id
     * @param bool $isPreferred
     * @param string|null $comment
     * @param string $updatedBy
     */
    public function __construct(string $id, bool $isPreferred, ?string $comment, string $updatedBy)
    {
        $this->id = $id;
        $this->isPreferred = $isPreferred;
        $this->comment = $comment;
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isPreferred(): bool
    {
        return $this->isPreferred;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getUpdatedBy(): string
    {
        return $this->updatedBy;
    }

    public function toJsonArray(): array
    {
        return [
            'id' => $this->getId(),
            'isPreferred' => $this->isPreferred(),
            'comment' => $this->getComment(),
            'updatedBy' => $this->getUpdatedBy()
        ];
    }
}
