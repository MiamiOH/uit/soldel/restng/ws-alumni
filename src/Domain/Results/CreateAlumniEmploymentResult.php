<?php


namespace MiamiOH\AlumniWebService\Domain\Results;

use MiamiOH\AlumniWebService\Domain\Models\AlumniEmployment;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class CreateAlumniEmploymentResult implements Jsonable
{
    /**
     * @var bool
     */
    private $isSuccess;
    /**
     * @var AlumniEmployment|null
     */
    private $data;
    /**
     * @var string|null
     */
    private $message;
    /**
     * @var array
     */
    private $errors;

    /**
     * CreateAlumniEmploymentResult constructor.
     * @param bool $isSuccess
     * @param AlumniEmployment|null $data
     * @param string|null $message
     * @param array $errors
     */
    public function __construct(bool $isSuccess, AlumniEmployment $data = null, string $message = null, array $errors = [])
    {
        $this->isSuccess = $isSuccess;
        $this->data = $data;
        $this->message = $message;
        $this->errors = $errors;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * @return AlumniEmployment|null
     */
    public function getData(): ?AlumniEmployment
    {
        return $this->data;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function toJsonArray(): array
    {
        return [
            'isSuccess' => $this->isSuccess(),
            'data' => $this->getData() ? $this->getData()->toJsonArray() : null,
            'message' => $this->getMessage(),
            'errors' => $this->getErrors()
        ];
    }
}
