<?php


namespace MiamiOH\AlumniWebService\Domain\Results;

use MiamiOH\AlumniWebService\Domain\Models\AlumniLinkedInUrl;
use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class CreateAlumniLinkedInUrlResult implements Jsonable
{
    /**
     * @var bool
     */
    private $isSuccess;
    /**
     * @var AlumniLinkedInUrl|null
     */
    private $data;
    /**
     * @var string|null
     */
    private $message;

    /**
     * CreateAlumniEmploymentResult constructor.
     * @param bool $isSuccess
     * @param AlumniLinkedInUrl $data
     * @param string|null $message
     */
    public function __construct(bool $isSuccess, ?AlumniLinkedInUrl $data, ?string $message)
    {
        $this->isSuccess = $isSuccess;
        $this->data = $data;
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * @return AlumniLinkedInUrl|null
     */
    public function getData(): ?AlumniLinkedInUrl
    {
        return $this->data;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function toJsonArray(): array
    {
        return [
            'isSuccess' => $this->isSuccess(),
            'data' => $this->getData() ? $this->getData()->toJsonArray() : null,
            'message' => $this->getMessage()
        ];
    }
}
