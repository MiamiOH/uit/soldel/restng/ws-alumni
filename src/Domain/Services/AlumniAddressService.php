<?php


namespace MiamiOH\AlumniWebService\Domain\Services;

use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniAddressRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class AlumniAddressService
{
    /**
     * @var AlumniAddressRepository
     */
    private $alumniAddressRepository;

    /**
     * AddressService constructor.
     * @param AlumniAddressRepository $alumniAddressRepository
     */
    public function __construct(AlumniAddressRepository $alumniAddressRepository)
    {
        $this->alumniAddressRepository = $alumniAddressRepository;
    }

    public function findOrCreate(CreateAlumniAddressRequest $request, ?bool &$isCreated): AlumniAddress
    {
        try {
            $type = $request->getType();

            $addresses = $this->alumniAddressRepository->getByPidmAndType($request->getPidm(), $type);

            if (($address = $addresses->lookup($request)) !== null) {
                $isCreated = false;
                return $address;
            }

            $address = $this->alumniAddressRepository->create($request);
            $isCreated = true;
            return $address;
        } catch (\Exception $e) {
            throw new ApplicationException('Failed to create an address', 0, $e);
        }
    }

    public function delete(string $id): void
    {
        try {
            $this->alumniAddressRepository->delete($id);
        } catch (\Exception $e) {
            throw new ApplicationException('Failed to delete the address.', 0, $e);
        }
    }
}
