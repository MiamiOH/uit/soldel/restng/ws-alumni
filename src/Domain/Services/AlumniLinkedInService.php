<?php


namespace MiamiOH\AlumniWebService\Domain\Services;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniLinkedInUrlCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniLinkedInUrlRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniLinkedInUrlResultCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniLinkedInUrl;
use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniLinkedInUrlRequestDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateAlumniLinkedInUrlRequest;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;
use MiamiOH\AlumniWebService\Domain\Results\CreateAlumniLinkedInUrlResult;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class AlumniLinkedInService
{
    public const LINKEDIN_EMAIL_TYPE = 'LINK';

    /**
     * @var AlumniRepository
     */
    private $alumniRepository;
    /**
     * @var AlumniWorkEmailRepository
     */
    private $alumniWorkEmailRepository;

    /**
     * AlumniLinkedInService constructor.
     * @param AlumniRepository $alumniRepository
     * @param AlumniWorkEmailRepository $alumniWorkEmailRepository
     */
    public function __construct(AlumniRepository $alumniRepository, AlumniWorkEmailRepository $alumniWorkEmailRepository)
    {
        $this->alumniRepository = $alumniRepository;
        $this->alumniWorkEmailRepository = $alumniWorkEmailRepository;
    }


    public function get(string $prospectId): AlumniLinkedInUrlCollection
    {
        try {
            $alumniLinkedInUrlCollection = new AlumniLinkedInUrlCollection();
            $pidm = $this->alumniRepository->getPidmByProspectId($prospectId);

            if ($pidm === null) {
                return new AlumniLinkedInUrlCollection();
            }
            $alumniWorkEmailOfLinkTypeCollection = $this->alumniWorkEmailRepository->getByPidmAndType(
                $pidm,
                self::LINKEDIN_EMAIL_TYPE
            );

            /** @var AlumniWorkEmail $alumniWorkEmailOfLinkType */
            foreach ($alumniWorkEmailOfLinkTypeCollection as $alumniWorkEmailOfLinkType) {
                $alumniLinkedInUrlCollection->push(new AlumniLinkedInUrl(
                    $alumniWorkEmailOfLinkType->getId(),
                    $prospectId,
                    $alumniWorkEmailOfLinkType->getEmail(),
                    $alumniWorkEmailOfLinkType->getComment(),
                    $alumniWorkEmailOfLinkType->getUpdatedAt()
                ));
            }
            return $alumniLinkedInUrlCollection;
        } catch (\Exception $e) {
            throw new ApplicationException('Failed to get the Alumni Linkedin Url record.', 0, $e);
        }
    }

    public function update(UpdateAlumniLinkedInUrlRequest $request): AlumniLinkedInUrl
    {
        $urls = $this->get($request->getProspectId());

        if (($url = $urls->getById($request->getId())) === null) {
            throw new ApplicationException(sprintf(
                'Alumni (ID: %s) does not exist, OR he/she does not has a LinkedIn URL with ID "%s"',
                $request->getProspectId(),
                $request->getId()
            ));
        }

        try {
            $email = $this->alumniWorkEmailRepository->update(new UpdateEmailRequest(
                $request->getId(),
                false,
                $request->getComment(),
                $request->getUpdatedBy()
            ));

            return new AlumniLinkedInUrl(
                $email->getId(),
                $url->getProspectId(),
                $email->getEmail(),
                $email->getComment(),
                $email->getUpdatedAt()
            );
        } catch (\Exception $e) {
            throw new ApplicationException('Failed to update LinkedIn Url.', 0, $e);
        }
    }

    public function create(CreateAlumniLinkedInUrlRequestDTOCollection $requests): CreateAlumniLinkedInUrlResultCollection
    {
        $results = new CreateAlumniLinkedInUrlResultCollection();

        /** @var CreateAlumniLinkedInUrlRequestDTO $request */
        foreach ($requests as $request) {
            $results->push($this->createOne($request));
        }

        return $results;
    }

    private function createOne(CreateAlumniLinkedInUrlRequestDTO $request): CreateAlumniLinkedInUrlResult
    {
        try {
            $pidm = $this->alumniRepository->getPidmByProspectId($request->getProspectId());

            $email = $this->alumniWorkEmailRepository->create(new CreateAlumniWorkEmailRequest(
                $pidm,
                self::LINKEDIN_EMAIL_TYPE,
                $request->getUrl(),
                false,
                $request->getComment(),
                true,
                true,
                $request->getCreatedBy()
            ));

            return new CreateAlumniLinkedInUrlResult(true, new AlumniLinkedInUrl(
                $email->getId(),
                $request->getProspectId(),
                $email->getEmail(),
                $email->getComment(),
                $email->getUpdatedAt()
            ), null);
        } catch (\Exception $e) {
            return new CreateAlumniLinkedInUrlResult(
                false,
                null,
                sprintf('Failed to associate the LinkedIn url to the alumni, message: %s', $e->getMessage())
            );
        }
    }
}
