<?php


namespace MiamiOH\AlumniWebService\Domain\Services;

use MiamiOH\AlumniWebService\Domain\Models\AlumniPhone;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniPhoneRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class AlumniPhoneService
{
    /**
     * @var AlumniPhoneRepository
     */
    private $alumniPhoneRepository;

    /**
     * AlumniPhoneService constructor.
     * @param AlumniPhoneRepository $alumniPhoneRepository
     */
    public function __construct(AlumniPhoneRepository $alumniPhoneRepository)
    {
        $this->alumniPhoneRepository = $alumniPhoneRepository;
    }

    public function findOrCreate(CreateAlumniPhoneRequest $request, ?bool &$isCreated): AlumniPhone
    {
        try {
            $type = $request->getType();
            $pidm = $request->getPidm();

            $phones = $this->alumniPhoneRepository->getByPidmAndType($pidm, $type);

            if (($phone = $phones->lookup($request)) !== null) {
                $isCreated = false;
                return $phone;
            }

            $phone = $this->alumniPhoneRepository->create($request);
            $isCreated = true;
            return $phone;
        } catch (\Exception $e) {
            throw new ApplicationException('Failed to create the phone record', 0, $e);
        }
    }

    public function delete(string $id): void
    {
        try {
            $this->alumniPhoneRepository->delete($id);
        } catch (\Exception $e) {
            throw new ApplicationException(sprintf('Failed to delete the phone (ID: %s).', $id), 0, $e);
        }
    }
}
