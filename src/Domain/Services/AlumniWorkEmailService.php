<?php


namespace MiamiOH\AlumniWebService\Domain\Services;

use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class AlumniWorkEmailService
{
    /**
     * @var AlumniWorkEmailRepository
     */
    private $alumniWorkEmailRepository;

    /**
     * AlumniWorkEmailService constructor.
     * @param AlumniWorkEmailRepository $alumniWorkEmailRepository
     */
    public function __construct(AlumniWorkEmailRepository $alumniWorkEmailRepository)
    {
        $this->alumniWorkEmailRepository = $alumniWorkEmailRepository;
    }

    public function findOrCreate(CreateAlumniWorkEmailRequest $request, ?bool &$isCreated): AlumniWorkEmail
    {
        try {
            $type = $request->getType();
            $pidm = $request->getPidm();

            $emails = $this->alumniWorkEmailRepository->getByPidmAndType($pidm, $type);

            if (($email = $emails->lookup($request)) !== null) {
                $isCreated = false;

                if ($request->getComment() && $email->getComment() !== $request->getComment()
                    || $request->isPreferred() !== $email->isPreferred()) {
                    return $this->alumniWorkEmailRepository->update(new UpdateEmailRequest(
                        $email->getId(),
                        $request->isPreferred(),
                        $request->getComment(),
                        $request->getCreatedBy()
                    ));
                }

                return $email;
            }

            $email = $this->alumniWorkEmailRepository->create($request);
            $isCreated = true;
            return $email;
        } catch (\Exception $e) {
            throw new ApplicationException('Failed to create the work email record.', 0, $e);
        }
    }

    public function delete(string $id): void
    {
        try {
            $this->alumniWorkEmailRepository->delete($id);
        } catch (\Exception $e) {
            throw new ApplicationException(sprintf('Failed to delete the work email (ID: %s).', $id), 0, $e);
        }
    }
}
