<?php

namespace MiamiOH\AlumniWebService\Domain\Services;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentResultCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmployment;
use MiamiOH\AlumniWebService\Domain\Models\AlumniPhone;
use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniEmploymentRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequestDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Results\CreateAlumniEmploymentResult;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class CreateAlumniEmploymentHistory
{
  /**
   * @var AlumniRepository
   */
  private $alumniRepository;
  /**
   * @var AlumniAddressService
   */
  private $alumniAddressService;
  /**
   * @var AlumniEmploymentRepository
   */
  private $alumniEmploymentRepository;
  /**
   * @var AlumniPhoneService
   */
  private $alumniPhoneService;
  /**
   * @var AlumniWorkEmailService
   */
  private $alumniWorkEmailService;

  /**
   * CreateAlumniEmploymentHistory constructor.
   * @param AlumniRepository $alumniRepository
   * @param AlumniAddressService $alumniAddressService
   * @param AlumniEmploymentRepository $alumniEmploymentRepository
   * @param AlumniPhoneService $alumniPhoneService
   * @param AlumniWorkEmailService $alumniWorkEmailService
   */
  public function __construct(
    AlumniRepository $alumniRepository,
    AlumniAddressService $alumniAddressService,
    AlumniEmploymentRepository $alumniEmploymentRepository,
    AlumniPhoneService $alumniPhoneService,
    AlumniWorkEmailService $alumniWorkEmailService
  ) {
    $this->alumniRepository = $alumniRepository;
    $this->alumniAddressService = $alumniAddressService;
    $this->alumniEmploymentRepository = $alumniEmploymentRepository;
    $this->alumniPhoneService = $alumniPhoneService;
    $this->alumniWorkEmailService = $alumniWorkEmailService;
  }

  public function create(CreateAlumniEmploymentRequestDTOCollection $requests): CreateAlumniEmploymentResultCollection
  {
    $results = new CreateAlumniEmploymentResultCollection();

    /** @var CreateAlumniEmploymentRequestDTO $request */
    foreach ($requests as $request) {
      $results->push($this->createOne($request));
    }

    return $results;
  }

  private function createOne(CreateAlumniEmploymentRequestDTO $request): CreateAlumniEmploymentResult
  {
    $isAddressCreated = false;
    $address = null;
    $isPhoneCreated = false;
    $phone = null;
    $isEmailCreated = false;
    $email = null;
    $errorMessages = null;

    try {
      // get pidm from prospect ID
      $prospectId = $request->getProspectId();
      $pidm = $this->alumniRepository->getPidmByProspectId($prospectId);
      if ($pidm === null) {
        throw new ApplicationException(sprintf('Prospect ID (%s) does not exist.', $prospectId));
      }


      // create employment record
      try {
        $employmentDTO = $this->alumniEmploymentRepository->create(CreateAlumniEmploymentRequest::createFromDTO(
          $pidm,
          $request,
          $address ? $address->getAddressType() : null,
          $address ? $address->getSequenceNumber() : null
        ));
      } catch (\Exception $e) {
        throw new ApplicationException('Failed to create the alumni employment history record.', 0, $e);
      }
      // create address
      try {
        if (($dto = $request->getCreateAddressRequest()) !== null) {
          $r = CreateAlumniAddressRequest::createFromDTO($pidm, $dto);
          $address = $this->alumniAddressService->findOrCreate($r, $isAddressCreated);
        }
      } catch (\Exception $e) {
        $address = null;
        $errorMessages .= $e->getMessage() . '.';
      }

      // create phone
      try {
        if (($dto = $request->getCreatePhoneRequest()) !== null) {
          $r = CreateAlumniPhoneRequest::createFromDTO($pidm, $dto);
          $phone = $this->alumniPhoneService->findOrCreate($r, $isPhoneCreated);
        }
      } catch (\Exception $e) {
        $phone = null;
        $errorMessages .= $e->getMessage() . '.';
      }

      // create work email
      try {
        if (($dto = $request->getCreateWorkEmailRequest()) !== null) {
          $r = CreateAlumniWorkEmailRequest::createFromDTO($pidm, $dto);
          $email = $this->alumniWorkEmailService->findOrCreate($r, $isEmailCreated);
        }
      } catch (\Exception $e) {
        $email = null;
        $errorMessages .= $e->getMessage() . '.';
      }

      $employment = AlumniEmployment::create(
        $employmentDTO,
        $phone ? new AlumniPhoneCollection([$phone]) : null,
        $email ? new AlumniWorkEmailCollection([$email]) : null,
        $address
      );
      $isSuccess = $errorMessages === null;
      return new CreateAlumniEmploymentResult($isSuccess, $employment, $errorMessages);
    } catch (ApplicationException $e) {
      return $this->fail($e);
    }
  }

  private function fail(
    ApplicationException $e
  ): CreateAlumniEmploymentResult {
    return new CreateAlumniEmploymentResult(false, null, $e->getMessage(), $e->getErrors());
  }
}
