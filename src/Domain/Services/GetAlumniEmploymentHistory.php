<?php

namespace MiamiOH\AlumniWebService\Domain\Services;

use MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniEmploymentCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmployment;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmploymentDTO;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniAddressRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniEmploymentRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniPhoneRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class GetAlumniEmploymentHistory
{
    /**
     * @var AlumniRepository
     */
    private $alumniRepository;
    /**
     * @var AlumniAddressRepository
     */
    private $alumniAddressRepository;
    /**
     * @var AlumniEmploymentRepository
     */
    private $alumniEmploymentRepository;
    /**
     * @var AlumniPhoneRepository
     */
    private $alumniPhoneRepository;
    /**
     * @var AlumniWorkEmailRepository
     */
    private $alumniWorkEmailRepository;

    /**
     * CreateAlumniEmploymentHistory constructor.
     * @param AlumniRepository $alumniRepository
     * @param AlumniAddressRepository $alumniAddressRepository
     * @param AlumniEmploymentRepository $alumniEmploymentRepository
     * @param AlumniPhoneRepository $alumniPhoneRepository
     * @param AlumniWorkEmailRepository $alumniWorkEmailRepository
     */
    public function __construct(AlumniRepository $alumniRepository, AlumniAddressRepository $alumniAddressRepository, AlumniEmploymentRepository $alumniEmploymentRepository, AlumniPhoneRepository $alumniPhoneRepository, AlumniWorkEmailRepository $alumniWorkEmailRepository)
    {
        $this->alumniRepository = $alumniRepository;
        $this->alumniAddressRepository = $alumniAddressRepository;
        $this->alumniEmploymentRepository = $alumniEmploymentRepository;
        $this->alumniPhoneRepository = $alumniPhoneRepository;
        $this->alumniWorkEmailRepository = $alumniWorkEmailRepository;
    }

    public function byProspectId(string $prospectId): AlumniEmploymentCollection
    {
        try {
            $employments = new AlumniEmploymentCollection();
            $phones = new AlumniPhoneCollection();
            $emails = new AlumniWorkEmailCollection();
            $addresses = new AlumniAddressCollection();

            $pidm = $this->alumniRepository->getPidmByProspectId($prospectId);

            if ($pidm === null) {
                return $employments;
            }

            // get all employment history records associated to the alumni
            $employmentDTOs = $this->alumniEmploymentRepository->get($pidm);

            if (count($employmentDTOs)) {
                // get all alumni addresses
                $addressTypes = $employmentDTOs->getAllAddressTypes();
                if (count($addressTypes)) {
                    $addresses = $this->alumniAddressRepository->getByPidmAndTypes($pidm, $addressTypes);
                }

                // get all phones
                $phones = $this->alumniPhoneRepository->getByPidmAndTypes($pidm, ['A1', 'A2']);

                // get all work emails
                $emails = $this->alumniWorkEmailRepository->getByPidmAndType($pidm, 'WRKE');
            }

            /** @var AlumniEmploymentDTO $employmentDTO */
            foreach ($employmentDTOs as $employmentDTO) {
                $address = null;
                if ($employmentDTO->getAddressType() && $employmentDTO->getAddressSeq()) {
                    $address = $addresses->getByTypeAndSeq(
                        $employmentDTO->getAddressType(),
                        $employmentDTO->getAddressSeq()
                    );
                }

                $employments->push(AlumniEmployment::create(
                    $employmentDTO,
                    $phones,
                    $emails,
                    $address
                ));
            }

            return $employments;
        } catch (\Exception $e) {
            throw new ApplicationException(sprintf('Failed to retrieve employment history records for Alumni (ID: %s)', $prospectId), 0, $e);
        }
    }
}
