<?php

namespace MiamiOH\AlumniWebService\Domain\Utils;

interface Jsonable
{
    public function toJsonArray(): array;
}
