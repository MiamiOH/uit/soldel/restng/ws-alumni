<?php

namespace MiamiOH\AlumniWebService\Exceptions;

use MiamiOH\AlumniWebService\Domain\Utils\Jsonable;

class ApplicationException extends \Exception implements Jsonable
{
    public function getErrors(): array
    {
        $errors = [$this->getMessage()];

        $e = $this;
        while (true) {
            $prev = $e->getPrevious();
            if ($prev === null) {
                break;
            }
            $errors[] = $prev->getMessage();
            $e = $prev;
        }

        return $errors;
    }

    public function toJsonArray(): array
    {
        return [
            'message' => $this->getMessage(),
            'errors' => $this->getErrors(),
        ];
    }
}
