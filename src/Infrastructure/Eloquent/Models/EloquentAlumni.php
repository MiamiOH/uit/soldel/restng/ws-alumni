<?php

namespace MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentAlumni extends Model
{
    protected $connection='MUWS_SEC_PROD';
    protected $table = 'spriden';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('default', function (Builder $query) {
            $query->select([
                'spriden_id',
                'spriden_pidm'
            ]);
        });
    }

    public function scopeByProspectId(Builder $query, string $prospectId)
    {
        $query->where('spriden_id', '=', $prospectId);
    }

    public function getPidm(): int
    {
        return $this->spriden_pidm;
    }

    public function getId(): string
    {
        return $this->spriden_id;
    }
}
