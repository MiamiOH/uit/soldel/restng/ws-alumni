<?php

namespace MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class EloquentAlumniEmployment extends Model
{
    protected $connection='MUWS_SEC_PROD';
    protected $primaryKey = 'rowid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'aprehis';
    protected $guarded = [];
    protected $dates = [
        'aprehis_from_date',
        'aprehis_to_date',
        'aprehis_activity_date',
        'aprehis_disp_notes_date'
    ];
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('default', function (Builder $query) {
            $query->selectRaw("
                aprehis_pidm,
                aprehis_seq_no,
                aprehis_activity_date,
                aprehis_disp_notes_date,
                aprehis_empr_pidm,
                aprehis_from_date,
                aprehis_to_date,
                aprehis_atyp_code,
                aprehis_atyp_seqno,
                aprehis_comment,
                aprehis_coop_ind,
                aprehis_disp_notes_ind,
                aprehis_empl_position,
                aprehis_empr_name,
                aprehis_emps_code,
                aprehis_jobc_code,
                aprehis_jobc_code2,
                aprehis_jobc_code3,
                aprehis_jobc_code4,
                aprehis_mg_ind,
                aprehis_ok_for_notes_ind,
                aprehis_primary_ind,
                aprehis_reviewed_ind,
                aprehis_reviewed_user,
                aprehis_sicc_code,
                aprehis_user,
                aprehis_wkly_hours,
                aprehis_xref_code,
                ROWIDTOCHAR(ROWID) as row_id
            ");
        });
    }

    public function scopeByPidm(Builder $query, string $pidm)
    {
        $query->where('aprehis_pidm', $pidm);
    }

    public function scopeByPidmAndSeq(Builder $query, string $pidm, int $seq)
    {
        $query->where('aprehis_pidm', '=', $pidm)
            ->where('aprehis_seq_no', '=', $seq);
    }

    public function scopeBySeq(Builder $query, int $seq)
    {
        $query->where('aprehis_seq_no', $seq);
    }

    public function scopeIsPrimary(Builder $query)
    {
        $query->where('aprehis_primary_ind', '=', 'Y');
    }

    public function scopeOrderBySeq(Builder $query)
    {
        $query->orderBy('aprehis_seq_no');
    }

    public function employer(): HasOne
    {
        return $this->hasOne(EloquentEmployer::class, 'spriden_pidm', 'aprehis_empr_pidm');
    }

    public function alumni(): HasOne
    {
        return $this->hasOne(EloquentAlumni::class, 'spriden_pidm', 'aprehis_pidm')
            ->where('spriden_ntyp_code', '=', 'ALUM')
            ->where('spriden_id', 'like', 'A%');
    }

    public function scopeWithRelations(Builder $query)
    {
        $query->with(['employer', 'alumni']);
    }

    public function getId(): string
    {
        return $this->row_id;
    }

    public function getPidm(): int
    {
        return $this->aprehis_pidm;
    }

    public function getEmployer(): ?string
    {
        return $this->aprehis_empr_name;
    }

    public function getProspectId(): string
    {
        /** @var EloquentAlumni $alumni */
        $alumni = $this->alumni;

        return $alumni->getId();
    }

    public function getJobCategoryCode(): ?string
    {
        return $this->aprehis_jobc_code;
    }

    public function getPosition(): ?string
    {
        return $this->aprehis_empl_position;
    }

    public function getFrom(): ?Carbon
    {
        return $this->aprehis_from_date;
    }

    public function getTo(): ?Carbon
    {
        return $this->aprehis_to_date;
    }

    public function getEmployerPidm(): ?int
    {
        return $this->aprehis_empr_pidm;
    }

    public function getEmployerId(): ?string
    {
        /** @var EloquentEmployer $employer */
        $employer = $this->employer;

        if ($employer) {
            return $employer->getId();
        }

        return null;
    }

    public function getEmployerName(): ?string
    {
        /** @var EloquentEmployer $employer */
        $employer = $this->employer;

        if ($employer) {
            return $employer->getName();
        }

        return null;
    }

    public function getActivityDate(): Carbon
    {
        return $this->aprehis_activity_date;
    }

    public function getStandardIndustrialCode(): ?string
    {
        return $this->aprehis_sicc_code;
    }

    public function getCrossReferenceCode(): ?string
    {
        return $this->aprehis_xref_code;
    }

    public function getEmploymentStatusCode(): ?string
    {
        return $this->aprehis_emps_code;
    }

    public function getWeeklyHours(): ?float
    {
        return $this->aprehis_wkly_hours;
    }

    public function isPrimary(): bool
    {
        return $this->aprehis_primary_ind === 'Y';
    }

    public function getAddressType(): ?string
    {
        return $this->aprehis_atyp_code;
    }

    public function getAddressSeq(): ?int
    {
        return $this->aprehis_atyp_seqno;
    }

    public function getJobCategoryCode2(): ?string
    {
        return $this->aprehis_jobc_code2;
    }

    public function getJobCategoryCode3(): ?string
    {
        return $this->aprehis_jobc_code3;
    }

    public function getJobCategoryCode4(): ?string
    {
        return $this->aprehis_jobc_code4;
    }

    public function getComments(): ?string
    {
        return $this->aprehis_comment;
    }

    public function getSeq(): int
    {
        return $this->aprehis_seq_no;
    }

    public function getUser(): string
    {
        return $this->aprehis_user;
    }

    public function isOkForNotes(): bool
    {
        return $this->aprehis_ok_for_notes_ind === 'Y';
    }

    public function isDisplayedNotes(): bool
    {
        return $this->aprehis_disp_notes_ind === 'Y';
    }

    public function isMatchingGift(): bool
    {
        return $this->aprehis_mg_ind === 'Y';
    }

    public function isReviewed(): bool
    {
        return $this->aprehis_reviewed_ind === 'Y';
    }

    public function getReviewedBy(): ?string
    {
        return $this->aprehis_reviewed_user;
    }

    public function isCoop(): bool
    {
        return $this->aprehis_coop_ind === 'Y';
    }

    public function getNoteDate(): ?Carbon
    {
        return $this->aprehis_disp_notes_date;
    }
}
