<?php


namespace MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentEmployer extends Model
{
    protected $connection='MUWS_SEC_PROD';
    protected $table = 'spriden';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('default', function (Builder $query) {
            $query->select([
                'spriden_id',
                'spriden_pidm',
                'spriden_last_name',
                'spriden_first_name',
                'spriden_mi'
            ])->whereNull('spriden_change_ind');
        });
    }

    public function scopeById(Builder $query, string $id)
    {
        $query->where('spriden_id', '=', $id);
    }

    public function getPidm(): int
    {
        return $this->spriden_pidm;
    }

    public function getId(): string
    {
        return $this->spriden_id;
    }

    public function getName(): string
    {
        return trim(sprintf(
            '%s %s %s',
            $this->spriden_first_name,
            $this->spriden_mi,
            $this->spriden_last_name
        ));
    }
}
