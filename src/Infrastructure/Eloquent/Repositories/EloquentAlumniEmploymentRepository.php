<?php


namespace MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories;

use Carbon\Carbon;
use Illuminate\Database\ConnectionInterface;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniEmploymentDTOCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmploymentDTO;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniEmploymentRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumniEmployment;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentEmployer;

class EloquentAlumniEmploymentRepository implements AlumniEmploymentRepository
{
    /**
     * @var EloquentAlumniEmployment
     */
    private $eloquentAlumniEmploymentModel;
    /**
     * @var EloquentEmployer
     */
    private $eloquentEmployerModel;
    /**
     * @var ConnectionInterface
     */
    private $db;

    /**
     * EloquentAlumniEmploymentRepository constructor.
     * @param EloquentAlumniEmployment $eloquentAlumniEmploymentModel
     * @param EloquentEmployer $eloquentEmployerModel
     * @param ConnectionInterface $db
     */
    public function __construct(EloquentAlumniEmployment $eloquentAlumniEmploymentModel, EloquentEmployer $eloquentEmployerModel, ConnectionInterface $db)
    {
        $this->eloquentAlumniEmploymentModel = $eloquentAlumniEmploymentModel;
        $this->eloquentEmployerModel = $eloquentEmployerModel;
        $this->db = $db;
    }

    public function create(CreateAlumniEmploymentRequest $request): AlumniEmploymentDTO
    {
        return $this->db->transaction(function () use ($request) {
            $employerPidm = null;
            $pidm = $request->getPidm();

            if ($request->isPrimary()) {
                $this->eloquentAlumniEmploymentModel
                    ->byPidm($pidm)
                    ->isPrimary()
                    ->update([
                        'aprehis_primary_ind' => 'N'
                    ]);
            }

            if (($employerId = $request->getEmployerId()) !== null) {
                /** @var EloquentEmployer|null $eloquentEmployer */
                $eloquentEmployer = $this->eloquentEmployerModel->byId($employerId)->first();
                if ($eloquentEmployer === null) {
                    throw new ApplicationException(sprintf('Employer ID (%s) does not exist.', $employerId));
                }
                $employerPidm = $eloquentEmployer->getPidm();
            }

            $maxSeq = $this->eloquentAlumniEmploymentModel->byPidm($pidm)->max('aprehis_seq_no') ?? 0;
            $seq = $maxSeq + 1;

            $this->eloquentAlumniEmploymentModel->create([
                'aprehis_pidm' => $pidm,
                'aprehis_seq_no' => $seq,
                'aprehis_empr_name' => $employerPidm ? null : $request->getEmployer(),
                'aprehis_empr_pidm' => $employerPidm,
                'aprehis_empl_position' => $request->getPosition(),
                'aprehis_primary_ind' => $request->isPrimary() ? 'Y' : null,
                'aprehis_from_date' => $request->getFrom(),
                'aprehis_to_date' => $request->getTo(),
                'aprehis_wkly_hours' => $request->getWeeklyHours(),
                'aprehis_emps_code' => $request->getStatusCode(),
                'aprehis_jobc_code' => $request->getJobCategoryCodes()[0] ?? null,
                'aprehis_jobc_code2' => $request->getJobCategoryCodes()[1] ?? null,
                'aprehis_jobc_code3' => $request->getJobCategoryCodes()[2] ?? null,
                'aprehis_jobc_code4' => $request->getJobCategoryCodes()[3] ?? null,
                'aprehis_sicc_code' => $request->getStandardIndustrialCode(),
                'aprehis_xref_code' => $request->getCrossReferenceCode(),
                'aprehis_comment' => $request->getComment(),
                'aprehis_atyp_code' => $request->getAddressType(),
                'aprehis_atyp_seqno' => $request->getAddressSeq(),
                'aprehis_activity_date' => Carbon::now(),
                'aprehis_user' => strtoupper($request->getCreatedBy()),
                'aprehis_ok_for_notes_ind' => $request->isOkForNotes() ? 'Y' : null,
                'aprehis_disp_notes_ind' => $request->isDisplayedNotes() ? 'Y' : null,
                'aprehis_mg_ind' => $request->isMatchingGift() ? 'Y' : null,
                'aprehis_reviewed_ind' => $request->isReviewed() ? 'Y' : null,
                'aprehis_reviewed_user' => $request->getReviewedBy(),
                'aprehis_coop_ind' => $request->isCoop() ? 'Y' : 'N',
                'aprehis_disp_notes_date' => $request->getNoteDate(),
            ]);

            $eloquentAlumniEmployment = $this->eloquentAlumniEmploymentModel
                ->byPidmAndSeq($pidm, $seq)
                ->first();

            return $this->constructAlumniEmploymentDTO($eloquentAlumniEmployment);
        });
    }

    public function get(int $pidm): AlumniEmploymentDTOCollection
    {
        $eloquentAlumniEmployments = $this->eloquentAlumniEmploymentModel
            ->byPidm($pidm)
            ->withRelations()
            ->orderBySeq()
            ->get();

        $dtos = new AlumniEmploymentDTOCollection();

        /** @var EloquentAlumniEmployment $eloquentAlumniEmployment */
        foreach ($eloquentAlumniEmployments as $eloquentAlumniEmployment) {
            $dtos->push($this->constructAlumniEmploymentDTO($eloquentAlumniEmployment));
        }

        return $dtos;
    }

    private function constructAlumniEmploymentDTO(EloquentAlumniEmployment $eloquentAlumniEmployment): AlumniEmploymentDTO
    {
        $jobCateCodes = array_filter(array_unique([
            $eloquentAlumniEmployment->getJobCategoryCode(),
            $eloquentAlumniEmployment->getJobCategoryCode2(),
            $eloquentAlumniEmployment->getJobCategoryCode3(),
            $eloquentAlumniEmployment->getJobCategoryCode4()
        ]), function (?string $item) {
            return $item !== null;
        });

        return new AlumniEmploymentDTO(
            $eloquentAlumniEmployment->getId(),
            $eloquentAlumniEmployment->getProspectId(),
            $eloquentAlumniEmployment->getSeq(),
            $eloquentAlumniEmployment->getEmployerName() ?? $eloquentAlumniEmployment->getEmployer(),
            $eloquentAlumniEmployment->getEmployerId(),
            $eloquentAlumniEmployment->getPosition(),
            $eloquentAlumniEmployment->isPrimary(),
            $eloquentAlumniEmployment->getFrom(),
            $eloquentAlumniEmployment->getTo(),
            $jobCateCodes,
            $eloquentAlumniEmployment->getStandardIndustrialCode(),
            $eloquentAlumniEmployment->getCrossReferenceCode(),
            $eloquentAlumniEmployment->getEmploymentStatusCode(),
            $eloquentAlumniEmployment->getWeeklyHours(),
            $eloquentAlumniEmployment->getComments(),
            $eloquentAlumniEmployment->isOkForNotes(),
            $eloquentAlumniEmployment->isDisplayedNotes(),
            $eloquentAlumniEmployment->isMatchingGift(),
            $eloquentAlumniEmployment->isReviewed(),
            $eloquentAlumniEmployment->getReviewedBy(),
            $eloquentAlumniEmployment->isCoop(),
            $eloquentAlumniEmployment->getNoteDate(),
            $eloquentAlumniEmployment->getUser(),
            $eloquentAlumniEmployment->getAddressType(),
            $eloquentAlumniEmployment->getAddressSeq(),
            $eloquentAlumniEmployment->getActivityDate()
        );
    }
}
