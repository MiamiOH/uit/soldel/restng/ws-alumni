<?php

namespace MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories;

use MiamiOH\AlumniWebService\Domain\Repositories\AlumniRepository;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumni;

class EloquentAlumniRepository implements AlumniRepository
{
    /**
     * @var EloquentAlumni
     */
    private $eloquentAlumniModel;

    /**
     * EloquentAlumniRepository constructor.
     * @param EloquentAlumni $eloquentAlumniModel
     */
    public function __construct(EloquentAlumni $eloquentAlumniModel)
    {
        $this->eloquentAlumniModel = $eloquentAlumniModel;
    }

    public function getPidmByProspectId(string $prospectId): ?int
    {
        /** @var EloquentAlumni|null $eloquentAlumni */
        $eloquentAlumni = $this->eloquentAlumniModel
            ->byProspectId($prospectId)
            ->first();

        if ($eloquentAlumni === null) {
            return null;
        }

        return $eloquentAlumni->getPidm();
    }
}
