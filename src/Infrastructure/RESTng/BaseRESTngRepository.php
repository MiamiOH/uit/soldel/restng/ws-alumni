<?php


namespace MiamiOH\AlumniWebService\Infrastructure\RESTng;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;

abstract class BaseRESTngRepository
{
    /**
     * @var App
     */
    private $app;

    /**
     * BaseRESTngRepository constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    protected function callResource(
        string $resourceName,
        array $params = [],
        array $options = [],
        array $data = [],
        bool $deferred = false
    ): Response {
        $config = [];

        if (!empty($params)) {
            $config['params'] = $params;
        }

        if (!empty($options)) {
            $config['options'] = $options;
        }

        if (!empty($data)) {
            $config['data'] = $data;
        }

        return $this->app->callResource(
            $resourceName,
            $config,
            [
                'deferred' => $deferred
            ]
        );
    }
}
