<?php


namespace MiamiOH\AlumniWebService\Infrastructure\RESTng;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniAddressRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class RESTngAlumniAddressRepository extends BaseRESTngRepository implements AlumniAddressRepository
{
    public function getByPidmAndType(int $pidm, string $type): AlumniAddressCollection
    {
        return $this->getByPidmAndTypes($pidm, [$type]);
    }

    public function getByPidmAndTypes(int $pidm, array $types): AlumniAddressCollection
    {
        $response = $this->callResource('person.address.v2.get', [], [
            'pidm' => $pidm,
            'addressType' => implode(',', $types),
            'status' => 'active'
        ]);

        $rawAddresses = $response->getPayload();
        return $this->constructAlumniAddresses($rawAddresses);
    }

    public function create(CreateAlumniAddressRequest $request): AlumniAddress
    {
        $response = $this->callResource(
            'person.address.v2.muid.create',
            [
                // use alternative muid
                'muid' => 'pidm=' . $request->getPidm()
            ],
            [],
            [
                [
                    'pidm' => (string)$request->getPidm(),
                    'addressType' => $request->getType(),
                    'streetLine1' => $request->getStreetLine1(),
                    'streetLine2' => $request->getStreetLine2(),
                    'streetLine3' => $request->getStreetLine3(),
                    'city' => $request->getCity(),
                    'state' => $request->getState(),
                    'postalCode' => $request->getZip(),
                    'fromDate' => $request->getFrom() === null ? null : $request->getFrom()->format('Y-m-d'),
                    'toDate' => $request->getTo() === null ? null : $request->getTo()->format('Y-m-d'),
                    'status' => null,
                    'addressSourceCode' => $request->getAddressSourceCode()
                ]
            ]
        );

        $payload = $response->getPayload()[0] ?? null;

        if (!$payload || $payload['status'] >= 400 || !isset($payload['address'])) {
            throw new ApplicationException($payload['message']);
        }

        return $this->constructAlumniAddress($payload['address']);
    }

    public function delete(string $id): void
    {
        $response = $this->callResource(
            'person.contact.v1.id.delete',
            [
                'id' => $id
            ]
        );

        if ($response->getStatus() >= 400) {
            throw new ApplicationException($response->getPayload()['message'] ?? null);
        }
    }

    private function constructAlumniAddresses(array $rawAddresses): AlumniAddressCollection
    {
        $alumniAddressCollection = new AlumniAddressCollection();
        foreach ($rawAddresses as $rawAddress) {
            $alumniAddressCollection->push($this->constructAlumniAddress($rawAddress));
        }

        return $alumniAddressCollection;
    }

    private function constructAlumniAddress($rawAddress): AlumniAddress
    {
        return new AlumniAddress(
            $rawAddress['id'],
            $rawAddress['pidm'],
            $rawAddress['addressType'],
            $rawAddress['streetLine1'],
            $rawAddress['streetLine2'],
            $rawAddress['streetLine3'],
            $rawAddress['city'],
            $rawAddress['state'],
            $rawAddress['postalCode'],
            $rawAddress['nationBannerCode'],
            $rawAddress['nationDescription'],
            $rawAddress['sequenceNumber'],
            $rawAddress['status'] ?? null,
            $rawAddress['fromDate'] === null ? null : Carbon::createFromFormat(
                'Y-m-d',
                $rawAddress['fromDate']
            )->startOfDay(),
            $rawAddress['toDate'] === null ? null : Carbon::createFromFormat('Y-m-d', $rawAddress['toDate'])->startOfDay(),
            $rawAddress['addressSourceCode']
        );
    }
}
