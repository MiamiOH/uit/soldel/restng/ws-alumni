<?php


namespace MiamiOH\AlumniWebService\Infrastructure\RESTng;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniPhone;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniPhoneRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class RESTngAlumniPhoneRepository extends BaseRESTngRepository implements AlumniPhoneRepository
{
    public function getByPidmAndType(int $pidm, string $type): AlumniPhoneCollection
    {
        return $this->getByPidmAndTypes($pidm, [$type]);
    }

    public function getByPidmAndTypes(int $pidm, array $types): AlumniPhoneCollection
    {
        $response = $this->callResource('person.phone.v1', [], [
            'pidm' => $pidm,
            'phoneType' => implode(',', $types),
            'status' => 'active'
        ]);

        $rawPhones = $response->getPayload();

        return $this->constructAlumniPhones($rawPhones);
    }

    public function create(CreateAlumniPhoneRequest $request): AlumniPhone
    {
        $ext = null;
        if (($ext = $request->getExtension()) !== null) {
            $ext = ';ext=' . $ext;
        }

        $response = $this->callResource(
            'person.phone.post',
            [
                // use alternative muid
                'muid' => 'pidm=' . $request->getPidm()
            ],
            [],
            [
                [
                    "phoneType" => $request->getType(),
                    "phoneNumber" => '+1' . $request->getAreaCode() . $request->getNumber() . $ext
                ]
            ]
        );

        $payload = $response->getPayload()[0] ?? null;

        if (!$payload || $payload['status'] >= 400 || !isset($payload['phone'][0])) {
            $message = "Failed to insert a new phone record.";
            if ($payload && isset($payload['message'])) {
                $message .= ' ' . $payload['message'];
            }
            throw new ApplicationException($message);
        }

        return $this->constructAlumniPhone($payload['phone'][0]);
    }

    public function delete(string $id): void
    {
        $response = $this->callResource(
            'person.phone.delete.id',
            [
                'id' => $id
            ]
        );

        if ($response->getStatus() >= 400) {
            $payload = $response->getPayload();
            throw new ApplicationException($payload['message']);
        }
    }

    private function constructAlumniPhones(array $data): AlumniPhoneCollection
    {
        $phones = new AlumniPhoneCollection();
        foreach ($data as $d) {
            $phones->push($this->constructAlumniPhone($d));
        }
        return $phones;
    }

    private function constructAlumniPhone(array $data): AlumniPhone
    {
        $activityDate = Carbon::createFromFormat('d-M-y', $data['activityDate']);
        $activityDate = $activityDate->startOfDay();

        return new AlumniPhone(
            $data['id'],
            $data['pidm'],
            $data['phoneType'],
            $data['phoneDesc'],
            $data['isPrimary'],
            $data['areaPart'],
            $data['numberPart'],
            $data['phoneNumber'],
            $data['ext'],
            $data['phoneNumberNational'],
            $data['phoneNumberInternational'],
            $data['sequenceNumber'],
            $data['status'],
            $activityDate
        );
    }
}
