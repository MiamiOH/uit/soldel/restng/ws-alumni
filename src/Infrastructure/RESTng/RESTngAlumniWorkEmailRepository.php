<?php

namespace MiamiOH\AlumniWebService\Infrastructure\RESTng;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;

class RESTngAlumniWorkEmailRepository extends BaseRESTngRepository implements AlumniWorkEmailRepository
{
    /**
     * @param int $pidm
     * @param string $type
     * @return AlumniWorkEmailCollection
     */
    public function getByPidmAndType(int $pidm, string $type): AlumniWorkEmailCollection
    {
        $response = $this->callResource('person.email.v2.search', [], [
            'pidms' => $pidm,
            'typeCodes' => $type,
            'isActive' => 'true'
        ]);

        return $this->constructEmails($response->getPayload());
    }

    /**
     * @param CreateAlumniWorkEmailRequest $request
     * @return AlumniWorkEmail
     * @throws ApplicationException
     */
    public function create(CreateAlumniWorkEmailRequest $request): AlumniWorkEmail
    {
        $response = $this->callResource('person.email.v2.create', [], [], [
            'pidm' => $request->getPidm(),
            'typeCode' => $request->getType(),
            'email' => $request->getEmail(),
            'isPreferred' => $request->isPreferred(),
            'isForced' => true,
            'isActive' => $request->isActive(),
            'isDisplayedOnWeb' => $request->isDispWeb(),
            'comment' => $request->getComment(),
            'createdBy' => $request->getCreatedBy()
        ]);

        $payload = $response->getPayload();
        if ($response->getStatus() >= 400) {
            $message = $payload['message'] ?? null;
            if ($message && isset($payload['errors']) && count($payload['errors'])) {
                $message .= ' (' . implode('; ', $payload['errors']) . ')';
            }
            throw new ApplicationException($message);
        }

        return $this->constructEmail($response->getPayload());
    }

    /**
     * @param string $id
     */
    public function delete(string $id): void
    {
        $response = $this->callResource(
            'person.email.v2.delete',
            [
                'id' => $id
            ]
        );

        if ($response->getStatus() >= 400) {
            throw new ApplicationException($response->getPayload()['message'] ?? null);
        }
    }

    private function constructEmail(array $data): AlumniWorkEmail
    {
        $updatedAt = Carbon::createFromFormat('Y-m-d H:i:s', $data['updatedAt']);

        return new AlumniWorkEmail(
            $data['id'],
            $data['pidm'],
            $data['typeCode'],
            $data['type'],
            $data['email'],
            $data['isPreferred'],
            $data['isActive'],
            $data['isDisplayedOnWeb'],
            $data['comment'],
            $data['updatedBy'],
            $updatedAt
        );
    }

    private function constructEmails(array $data): AlumniWorkEmailCollection
    {
        $emails = new AlumniWorkEmailCollection();
        foreach ($data as $d) {
            $emails->push($this->constructEmail($d));
        }
        return $emails;
    }

    public function update(UpdateEmailRequest $request): AlumniWorkEmail
    {
        $body = [
            'isPreferred' => $request->isPreferred(),
            'updatedBy' => $request->getId()
        ];

        if ($request->getComment()) {
            $body['comment'] = $request->getComment();
        }

        $response = $this->callResource('person.email.v2.update', [
            'id' => $request->getId()
        ], [], $body);

        if ($response->getStatus() >= 400) {
            throw new ApplicationException($payload[0]['message'] ?? null);
        }

        return $this->constructEmail($response->getPayload());
    }
}
