<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-03-26
 * Time: 10:32
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use function DI\add;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use Carbon\Carbon;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection
 */
class AlumniAddressCollectionTest extends TestCase
{
    public function testCanLookup()
    {
        /** @var AlumniAddressCollection $address */
        $address = $this->mockAlumniAddresses(3, [
            [
                'pidm' => 11111,
                'addressType' => 'A1',
                'streetLine1' => 'street 1',
                'streetLine2' => '',
                'streetLine3' => '',
                'city' => 'Oxford',
                'state' => 'Ohio',
                'postalCode' => '12345',
                'nationBannerCode' => '',
                'nationDescription' => '',
                'sequenceNumber' => 1,
                'status' => '',
                'from' => Carbon::createFromFormat('Y-m-d', '2020-01-01'),
                'to' => Carbon::createFromFormat('Y-m-d', '2020-02-21'),
                'addressSourceCode' => '',
                
            ],
            [
                'pidm' => 22222,
                'addressType' => 'A2',
            ],
            [
                'pidm' => 33333,
                'addressType' => 'A3',
            ]
        ]);

        $request = ($address->lookUp(new CreateAlumniAddressRequest(
            '11111',
            'A1',
            Carbon::create(2021, 1, 1),
            Carbon::create(2021, 2, 21),
            false,
            'street 1',
            '',
            '',
            'Oxford',
            'Ohio',
            '12345',
            ''
        )));
        
        $this->assertSame($address[0], $request);
        
        $this->assertNull(($address->lookup(new CreateAlumniAddressRequest(
            '55555',
            'A1',
            Carbon::create(2021, 1, 1),
            Carbon::create(2021, 2, 21),
            false,
            'street 1',
            '',
            '',
            'Oxford',
            'Ohio',
            '12345',
            ''
        ))));
        
        $this->assertSame($address[0],$address->getByTypeAndSeq('A1','1'));
        $this->assertNull($address->getByTypeAndSeq('AA','1'));
    }
}
