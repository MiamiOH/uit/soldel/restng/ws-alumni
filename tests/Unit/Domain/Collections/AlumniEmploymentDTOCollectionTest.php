<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\AlumniEmploymentDTOCollection
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\BaseModelCollection
 */
class AlumniEmploymentDTOCollectionTest extends TestCase
{
    public function testCollectAllAddressTypesInTheCollectionWithoutDuplications()
    {
        $dtos = $this->mockAlumniEmploymentDTOs(5, [
            [
                'addressType' => 'A1'
            ],
            [
                'addressType' => null
            ],
            [
                'addressType' => null
            ],
            [
                'addressType' => 'A2'
            ],
            [
                'addressType' => 'A1'
            ]
        ]);

        $this->assertSame(['A1', 'A2'], $dtos->getAllAddressTypes());
    }

    public function testConvertCollectionToArray()
    {
        $dtos = $this->mockAlumniEmploymentDTOs(2);
        $arr1 = $dtos[0]->toJsonArray();
        $arr2 = $dtos[1]->toJsonArray();
        $this->assertSame([
            $arr1,
            $arr2
        ], $dtos->toJsonArray());
    }
}