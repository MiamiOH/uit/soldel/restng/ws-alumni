<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-03-26
 * Time: 14:01
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use MiamiOH\AlumniWebService\Domain\Collections\AlumniLinkedInUrlCollection;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use Carbon\Carbon;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\AlumniLinkedInUrlCollection
 */
class AlumniLinkedInUrlCollectionTest extends TestCase
{
    public function testCanGetById()
    {
        /** @var AlumniLinkedInUrlCollection $linkinUrl */
        $linkinUrl = $this->mockAlumniLinkedInUrls(2, [
           [
               'id' => '12345',
               'prospectId' => 'A22222',
               'url' => 'test@test.com',
               'comment' => 'comment',
               'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5)
           ] 
        ]);
        $request = $linkinUrl->getById('12345');
        $this->assertSame($linkinUrl[0], $linkinUrl->getById('12345'));
        $this->assertNull(  $linkinUrl->getById('45678'));
    }
}
