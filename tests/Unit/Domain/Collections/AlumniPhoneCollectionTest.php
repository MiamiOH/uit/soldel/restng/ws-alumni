<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection
 */
class AlumniPhoneCollectionTest extends TestCase
{
    public function testLookupPhoneFromRequest()
    {
        $phones = $this->mockAlumniPhones(3, [
            [
                'pidm' => 111111,
                'phoneType' => 'A1',
                'areaPart' => '513',
                'numberPart' => '1111111',
                'ext' => '1234',
            ],
            [
                'pidm' => 111111,
                'phoneType' => 'A1',
                'areaPart' => '513',
                'numberPart' => '2222222',
                'ext' => '1234',
            ],
            [
                'pidm' => 2222222,
                'phoneType' => 'A1',
                'areaPart' => '513',
                'numberPart' => '1111111',
                'ext' => '1234',
            ]
        ]);

        $this->assertSame($phones[0], $phones->lookup(new CreateAlumniPhoneRequest(
            111111,
            'A1',
            '513',
            '1111111',
            '1234'
        )));

        $this->assertNull($phones->lookup(new CreateAlumniPhoneRequest(
            111111,
            'A2',
            '513',
            '1111111',
            '1234'
        )));
    }
}