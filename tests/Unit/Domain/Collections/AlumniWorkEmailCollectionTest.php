<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection
 */
class AlumniWorkEmailCollectionTest extends TestCase
{
    public function testLookupEmail()
    {
        $emails = $this->mockAlumniWorkEmails(3, [
            [
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'aaa@miamioh.edu'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'AAAA',
                'email' => 'aaa@miamioh.edu'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'ccc@miamioh.edu'
            ]
        ]);

        $this->assertSame($emails[1], $emails->lookup($this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'AAAA',
            'email' => 'aaa@miamioh.edu'
        ])));

        $this->assertNull($emails->lookup($this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'WRKE',
            'email' => 'bbb@miamioh.edu'
        ])));
    }

    public function testGetWorkEmailFromId()
    {
        $emails = $this->mockAlumniWorkEmails(3, [
            [
                'id' => 'KDLSJFDKL'
            ],
            [
                'id' => 'KSLDKJFKL'
            ],
        ]);

        $this->assertSame($emails[1], $emails->byId('KSLDKJFKL'));
        $this->assertNull($emails->byId('CKLSKDJKLF'));
    }
}