<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-03-26
 * Time: 10:28
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequestDTO;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use Illuminate\Validation\ValidationException;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentRequestDTOCollection
 */
class CreateAlumniEmploymentRequestDTOCollectionTest extends TestCase
{
    public function testCollectCreateFromArray()
    {
        $dtos =  ([
            [
                'prospectId' => 'A12345678',
                'employer' => '3M',
                'employerId' => 'THREEM',
                'position' => 'Manager',
                'isPrimary' => true,
                'from' => '2020-01-01',
                'to' => '2020-02-01',
                'weeklyHours' => 40.5,
                'statusCode' => 'C',
                'jobCategoryCodes' =>['CEO', 'COO'],
                'standardIndustrialCode' => 'XX21',
                'crossReferenceCode' => 'DKS',
                'comment' => 'sadljk',
                'isOkForNotes' => true,
                'isDisplayedNotes' => true,
                'isMatchingGift' => true,
                'isReviewed' => true,
                'reviewedBy' => 'kslkd',
                'isCoop' => true,
                'noteDate' => '2020-02-04',
                'workEmail' => '',
                'phone' => '',
                'address' => '',
                'createdBy' => '',
            ],
            [
                'prospectId' => 'A87654321',
                'employer' => '3M',
                'employerId' => 'THREEM',
                'position' => 'Manager',
                'isPrimary' => true,
                'from' => '2020-01-01',
                'to' => '2020-02-01',
                'weeklyHours' => 40.5,
                'statusCode' => 'C',
                'jobCategoryCodes' =>['CEO', 'COO'],
                'standardIndustrialCode' => 'XX21',
                'crossReferenceCode' => 'DKS',
                'comment' => 'sadljk',
                'isOkForNotes' => true,
                'isDisplayedNotes' => true,
                'isMatchingGift' => true,
                'isReviewed' => true,
                'reviewedBy' => 'kslkd',
                'isCoop' => true,
                'noteDate' => '2020-02-04',
                'workEmail' => '',
                'phone' => '',
                'address' => '',
                'createdBy' => 'Tester',
            ]
        ]);

        $employmentRequestCollection = new CreateAlumniEmploymentRequestDTOCollection();
        $this->assertCount(2, $employmentRequestCollection->createFromArray($dtos, 'Tester'));
    }

    public function testFailedCollectCreateFromArray()
    {   
        $this->expectException(\Exception::class);
        $dtos =  ([
            [
                'prospectId' => 'A12345678',
                'employer' => '3M',
                'employerId' => 'THREEM',
                'position' => 'Manager',
                'isPrimary' => true,
                'from' => '2020-01-01',
                'to' => '2020-02-01',
                'weeklyHours' => 40.5,
                'statusCode' => 'C',
                'jobCategoryCodes' =>['CEO', 'COO'],
                'standardIndustrialCode' => 'XX21',
                'crossReferenceCode' => 'DKS',
                'comment' => 'sadljk',
                'isOkForNotes' => true,
                'isDisplayedNotes' => true,
                'isMatchingGift' => true,
                'isReviewed' => true,
                'reviewedBy' => 'kslkd',
                'isCoop' => true,
                'noteDate' => '#$#%^$^$',
                'workEmail' => '',
                'phone' => '',
                'address' => '',
                'createdBy' => '',
            ]
        ]);
        $employmentRequestCollection = new CreateAlumniEmploymentRequestDTOCollection();
        $employmentRequestCollection->createFromArray($dtos, 'Tester');
    }
}
