<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-03-29
 * Time: 09:36
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Collections;


use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniLinkedInUrlRequestDTOCollection;
use MiamiOH\AlumniWebService\Exceptions\BadRequestsException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers  \MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniLinkedInUrlRequestDTOCollection
 */
class CreateAlumniLinkedInUrlRequestCollectionTest extends TestCase
{
    public function testCanGetCollectionCreateFromArray()
    {
        $dtos = ([
            [
                'prospectId' => 'A123456789',
                'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
                'comment' => 'Test',
                'createdBy' => 'tester'
            ],
            [
                'prospectId' => 'A987654321',
                'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
                'comment' => 'Test',
                'createdBy' => 'tester'
            ]
        ]);
       
        $linkedInUrlCollection = new CreateAlumniLinkedInUrlRequestDTOCollection();
        $this->assertCount(2, $linkedInUrlCollection->createFromArray($dtos));
    }

    public function testFailGetCollectionCreateFromArray()
    {
        $this->expectException(\Exception::class);
        $dtos = [
            [
                'prospectId' => 'A123456789',
                'url' => '!@#$%^&&',
                'comment' => 'Test',
                'createdBy' => 'tester'
            ]
        ];

        $linkedInUrlCollection = new CreateAlumniLinkedInUrlRequestDTOCollection();
        $linkedInUrlCollection->createFromArray($dtos);
    }
}
