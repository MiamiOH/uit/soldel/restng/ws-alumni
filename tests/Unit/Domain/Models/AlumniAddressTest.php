<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Models;


use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * Class AlumniAddressTest
 * @package MiamiOH\AlumniWebService\Tests\Unit\Domain\Models
 * @covers \MiamiOH\AlumniWebService\Domain\Models\AlumniAddress
 */
class AlumniAddressTest extends TestCase
{
    public function testCanParseToJsonArray(): void
    {
        $alumniAddress = new AlumniAddress(
            'ABC',
            '1440394',
            'A2',
            'Qwerty Ln',
            '',
            '',
            'Qwerty city',
            'QT',
            '4242',
            '',
            '',
            1,
            'I',
            Carbon::createFromFormat('Y-m-d', '2020-02-21'),
            Carbon::createFromFormat('Y-m-d', '2021-02-21'),
            ''
        );
        $addresArray = [
            'id' => 'ABC',
            'pidm' => '1440394',
            'addressType' => 'A2',
            'streetLine1' => 'Qwerty Ln',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Qwerty city',
            'state' => 'QT',
            'postalCode' => '4242',
            'nationBannerCode' => '',
            'nationDescription' => '',
            'sequenceNumber' => 1,
            'status' => 'I',
            'from' => '2020-02-21',
            'to' => '2021-02-21',
            'addressSourceCode' => ''
        ];
        $this->assertEquals($addresArray, $alumniAddress->toJsonArray());
    }
}
