<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Models\AlumniEmploymentDTO
 */
class AlumniEmploymentDTOTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $dto = $this->mockAlumniEmploymentDTO([
            'id' => 'AKDLFJSKEL',
            'prospectId' => 'A00192839',
            'seq' => 2,
            'employer' => '3M',
            'employerId' => 'THREM',
            'position' => 'Business Manager',
            'isPrimary' => true,
            'from' => Carbon::create(2020, 1, 2),
            'to' => Carbon::create(2020, 6, 3),
            'jobCategoryCodes' => ['HB2'],
            'standardIndustrialCode' => 'XX02',
            'crossReferenceCode' => 'SDF',
            'statusCode' => 'C',
            'weeklyHours' => 40.5,
            'comment' => 'Jen found this recond on Linkedin',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'janbca',
            'isCoop' => false,
            'noteDate' => Carbon::create(2020, 2, 5),
            'user' => 'lajgk',
            'addressType' => 'A1',
            'addressSeq' => 3,
            'updatedAt' => Carbon::create(2020, 8, 15, 3, 2, 5),
        ]);

        $this->assertSame([
            'id' => 'AKDLFJSKEL',
            'seq' => 2,
            'prospectId' => 'A00192839',
            'employer' => '3M',
            'employerId' => 'THREM',
            'position' => 'Business Manager',
            'isPrimary' => true,
            'from' => '2020-01-02',
            'to' => '2020-06-03',
            'jobCategoryCodes' => ['HB2'],
            'standardIndustrialCode' => 'XX02',
            'crossReferenceCode' => 'SDF',
            'statusCode' => 'C',
            'weeklyHours' => 40.5,
            'comment' => 'Jen found this recond on Linkedin',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'janbca',
            'isCoop' => false,
            'noteDate' => '2020-02-05',
            'user' => 'lajgk',
            'addressType' => 'A1',
            'addressSeq' => 3,
            'updatedAt' => '2020-08-15 03:02:05',
        ], $dto->toJsonArray());
    }
}