<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmployment;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Models\AlumniEmployment
 */
class AlumniEmploymentTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $employment = $this->mockAlumniEmployment([
            'address' => null,
            'workEmails' => new AlumniWorkEmailCollection(),
            'phones' => new AlumniPhoneCollection(),
            'id' => 'AKDLFJSKEL',
            'prospectId' => 'A00192839',
            'seq' => 2,
            'employer' => '3M',
            'employerId' => 'THREM',
            'position' => 'Business Manager',
            'isPrimary' => true,
            'from' => Carbon::create(2020, 1, 2),
            'to' => Carbon::create(2020, 6, 3),
            'jobCategoryCodes' => ['HB2'],
            'standardIndustrialCode' => 'XX02',
            'crossReferenceCode' => 'SDF',
            'statusCode' => 'C',
            'weeklyHours' => 40.5,
            'comment' => 'Jen found this recond on Linkedin',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'janbca',
            'isCoop' => false,
            'noteDate' => Carbon::create(2020, 2, 5),
            'user' => 'lajgk',
            'updatedAt' => Carbon::create(2020, 8, 15, 3, 2, 5),
        ]);

        $this->assertSame([
            'id' => 'AKDLFJSKEL',
            'prospectId' => 'A00192839',
            'sequence' => 2,
            'employer' => '3M',
            'employerId' => 'THREM',
            'position' => 'Business Manager',
            'isPrimary' => true,
            'from' => '2020-01-02',
            'to' => '2020-06-03',
            'jobCategoryCodes' => ['HB2'],
            'standardIndustrialCode' => 'XX02',
            'crossReferenceCode' => 'SDF',
            'statusCode' => 'C',
            'weeklyHours' => 40.5,
            'comment' => 'Jen found this recond on Linkedin',
            'address' => null,
            'phones' => [],
            'workEmails' => [],
            'user' => 'lajgk',
            'updatedAt' => '2020-08-15 03:02:05',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'janbca',
            'isCoop' => false,
            'noteDate' => '2020-02-05',
        ], $employment->toJsonArray());
    }

    public function testCreateFromDTO()
    {
        $dto = $this->mockAlumniEmploymentDTO([
            'id' => 'AKDLFJSKEL',
            'prospectId' => 'A00192839',
            'seq' => 2,
            'employer' => '3M',
            'employerId' => 'THREM',
            'position' => 'Business Manager',
            'isPrimary' => true,
            'from' => Carbon::create(2020, 1, 2),
            'to' => Carbon::create(2020, 6, 3),
            'jobCategoryCodes' => ['HB2'],
            'standardIndustrialCode' => 'XX02',
            'crossReferenceCode' => 'SDF',
            'statusCode' => 'C',
            'weeklyHours' => 40.5,
            'comment' => 'Jen found this recond on Linkedin',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'janbca',
            'isCoop' => false,
            'noteDate' => Carbon::create(2020, 2, 5),
            'user' => 'lajgk',
            'addressType' => 'A1',
            'addressSeq' => 3,
            'updatedAt' => Carbon::create(2020, 8, 15, 3, 2, 5),
        ]);

        $address = null;
        $phones = $this->mockAlumniPhones(2);
        $emails = $this->mockAlumniWorkEmails(3);

        $this->assertSame([
            'id' => 'AKDLFJSKEL',
            'prospectId' => 'A00192839',
            'sequence' => 2,
            'employer' => '3M',
            'employerId' => 'THREM',
            'position' => 'Business Manager',
            'isPrimary' => true,
            'from' => '2020-01-02',
            'to' => '2020-06-03',
            'jobCategoryCodes' => ['HB2'],
            'standardIndustrialCode' => 'XX02',
            'crossReferenceCode' => 'SDF',
            'statusCode' => 'C',
            'weeklyHours' => 40.5,
            'comment' => 'Jen found this recond on Linkedin',
            'address' => null,
            'phones' => $phones->toJsonArray(),
            'workEmails' => $emails->toJsonArray(),
            'user' => 'lajgk',
            'updatedAt' => '2020-08-15 03:02:05',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'janbca',
            'isCoop' => false,
            'noteDate' => '2020-02-05',
        ], AlumniEmployment::create($dto, $phones, $emails)->toJsonArray());
    }
}