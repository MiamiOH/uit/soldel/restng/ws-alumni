<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Models;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Models\AlumniLinkedInUrl
 */
class AlumniLinkedInUrlTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $url = $this->mockAlumniLinkedInUrl([
            'id' => '1243',
            'prospectId' => 'A00849375',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'hello world',
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5)
        ]);

        $this->assertSame([
            'id' => '1243',
            'prospectId' => 'A00849375',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'hello world',
            'updatedAt' => '2020-01-02 03:04:05'
        ], $url->toJsonArray());
    }
}
