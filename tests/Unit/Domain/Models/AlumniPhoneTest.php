<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Models;


use Carbon\Carbon;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Models\AlumniPhone
 */
class AlumniPhoneTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $phone = $this->mockAlumniPhone([
            'id' => 'KSLDKFJDKS',
            'pidm' => 111111,
            'phoneType' => 'A1',
            'phoneDesc' => 'Alumni Primary Phone',
            'isPrimary' => true,
            'areaPart' => '513',
            'numberPart' => '1111111',
            'phoneNumber' => '+15131111111',
            'ext' => '8800',
            'phoneNumberNational' => '513-111-1111',
            'phoneNumberInternational' => '+1 513 111 1111',
            'sequenceNumber' => 3,
            'status' => 'active',
            'activityDate' => Carbon::create(2020, 1, 2, 3, 4, 5),
        ]);

        $this->assertSame([
            'id' => 'KSLDKFJDKS',
            'pidm' => 111111,
            'phoneType' => 'A1',
            'phoneDesc' => 'Alumni Primary Phone',
            'isPrimary' => true,
            'areaPart' => '513',
            'numberPart' => '1111111',
            'phoneNumber' => '+15131111111',
            'ext' => '8800',
            'phoneNumberNational' => '513-111-1111',
            'phoneNumberInternational' => '+1 513 111 1111',
            'sequenceNumber' => 3,
            'status' => 'active',
            'activityDate' => '2020-01-02 03:04:05',
        ], $phone->toJsonArray());
    }
}