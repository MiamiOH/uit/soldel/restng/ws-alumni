<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Models;


use Carbon\Carbon;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail
 */
class AlumniWorkEmailTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $email = $this->mockAlumniWorkEmail([
            'id' => 'KDSLKJF',
            'pidm' => 111111,
            'typeCode' => 'WRKE',
            'type' => 'Alumni Work Email',
            'email' => 'aaa@miamioh.edu',
            'isActive' => true,
            'isPreferred' => true,
            'comment' => 'kalj alksdja;ls djf',
            'isDisplayedOnWeb' => true,
            'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5),
            'updatedBy' => 'LSKDLF',
        ]);

        $this->assertSame([
            'id' => 'KDSLKJF',
            'pidm' => 111111,
            'typeCode' => 'WRKE',
            'type' => 'Alumni Work Email',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'isActive' => true,
            'isDisplayedOnWeb' => true,
            'comment' => 'kalj alksdja;ls djf',
            'updatedBy' => 'LSKDLF',
            'updatedAt' => '2020-01-02 03:04:05',
        ], $email->toJsonArray());
    }
}