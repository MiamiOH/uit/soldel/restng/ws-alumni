<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-14
 * Time: 15:11
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequestDTO;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;


/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequestDTO
 */
class CreateAlumniAddressRequestDTOTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $address = $this->mockCreateAlumniAddressRequestDTO([
            'type' => 'A2',
            'from' =>Carbon::create(2021, 1, 1),
            'to' => Carbon::create(2021, 2, 21),
            'isPreferred' => false,
            'streetLine1' => 'Street line 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'addressSourceCode' => ''
        ]);
     
        $this->assertSame([
            'type' => 'A2',
            'from' => '2021-01-01',
            'to' => '2021-02-21',
            'isPreferred' => false,
            'streetLine1' => 'Street line 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'addressSourceCode' => ''
        ], $address->toJsonArray());
    }
}
