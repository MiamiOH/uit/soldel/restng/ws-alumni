<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-12
 * Time: 08:50
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest
 */
class CreateAlumniAddressRequestTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $address = $this->mockCreateAlumniAddressRequest([
            'pidm' => 111111,
            'type' => 'A2',
            'from' => Carbon::create(2021, 1, 1),
            'to' => Carbon::create(2021, 2, 21),
            'isPreferred' => false,
            'streetLine1' => 'Street line 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'addressSourceCode' => ''
        ]);

        $this->assertSame([
            'pidm' => 111111,
            'type' => 'A2',
            'from' => '2021-01-01',
            'to' => '2021-02-21',
            'isPreferred' => false,
            'streetLine1' => 'Street line 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'addressSourceCode' => ''
        ], $address->toJsonArray());
    }

    public function testCreateFromDTO()
    {
        $dto = $this->mockCreateAlumniAddressRequestDTO([
            'pidm' => 111111,
            'type' => 'A2',
            'from' => Carbon::create(2021, 1, 1),
            'to' => Carbon::create(2021, 2, 21),
            'isPreferred' => false,
            'streetLine1' => 'Street line 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'addressSourceCode' => ''
        ]);
        
        $this->assertSame([
            'pidm' => 111111,
            'type' => 'A2',
            'from' => '2021-01-01',
            'to' => '2021-02-21',
            'isPreferred' => false,
            'streetLine1' => 'Street line 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'addressSourceCode' => '' 
        ], CreateAlumniAddressRequest::createFromDTO(111111, $dto)->toJsonArray());
    }
}
