<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-14
 * Time: 16:09
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequestDTO;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers  \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequestDTO
 */
class CreateAlumniEmploymentRequestDTOTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $alumnuEmployment = $this-> mockCreateAlumniEmploymentDTORequest([
            'prospectId' => 'A1234567',
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => Carbon::create(2020, 1, 1),
            'to' => Carbon::create(2020, 2, 2),
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => Carbon::create(2020, 2, 4),
            'createWorkEmailRequest' => null,
            'createPhoneRequest' => null,
            'createAddressRequest' => null,
            'createdBy' => Carbon::create(2020, 8, 5, 4, 6, 7),
        ]);
        
        $this->assertSame([
            'prospectId' => 'A1234567',
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => '2020-01-01',
            'to' => '2020-02-02',
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => '2020-02-04',
            'createWorkEmailRequest' => null,
            'createPhoneRequest' => null,
            'createAddressRequest' => null,
            'createdBy' => '2020-08-05 04:06:07',
        ], $alumnuEmployment->toJsonArray());
    }

    public function testCreateFromArray()
    {
        $alumnuEmploymentDTO = ([
            'prospectId' => 'A12345678',
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => '2020-01-01',
            'to' => '2020-02-01',
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' =>['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => '2020-02-04',
            'workEmail' => '',
            'phone' => '',
            'address' => '',
            'createdBy' => 'Tester',
        ]);

        $this->assertSame([
            'prospectId' => 'A12345678',
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => '2020-01-01',
            'to' => '2020-02-01',
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => '2020-02-04',
            'createWorkEmailRequest' => null,
            'createPhoneRequest' => null,
            'createAddressRequest' => null,
            'createdBy' => 'Tester',
        ], CreateAlumniEmploymentRequestDTO::createFromArray($alumnuEmploymentDTO)->toJsonArray());
    }
}
