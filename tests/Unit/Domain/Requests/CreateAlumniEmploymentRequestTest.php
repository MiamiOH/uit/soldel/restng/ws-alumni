<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;


use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest
 */
class CreateAlumniEmploymentRequestTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $req = $this->mockCreateAlumniEmploymentRequest([
            'pidm' => 111111,
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => Carbon::create(2020, 1, 1),
            'to' => Carbon::create(2020, 2, 2),
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'addressType' => 'MA',
            'addressSeq' => 3,
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => Carbon::create(2020, 2, 4),
            'createdBy' => Carbon::create(2020, 8, 5, 4, 6, 7),
        ]);

        $this->assertSame([
            'pidm' => 111111,
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => '2020-01-01',
            'to' => '2020-02-02',
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'addressType' => 'MA',
            'addressSeq' => 3,
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => '2020-02-04',
            'createdBy' => '2020-08-05 04:06:07',
        ], $req->toJsonArray());
    }

    public function testCreateFromDTO()
    {
        $dto = $this-> mockCreateAlumniEmploymentDTORequest([
            'pidm' => 111111,
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => Carbon::create(2020, 1, 1),
            'to' => Carbon::create(2020, 2, 2),
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'addressType' => 'MA',
            'addressSeq' => 3,
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => Carbon::create(2020, 2, 4),
            'createdBy' => Carbon::create(2020, 8, 5, 4, 6, 7),
        ]);
        
        $this->assertSame([
            'pidm' => 111111,
            'employer' => '3M',
            'employerId' => 'THREEM',
            'position' => 'Manager',
            'isPrimary' => true,
            'from' => '2020-01-01',
            'to' => '2020-02-02',
            'weeklyHours' => 40.5,
            'statusCode' => 'C',
            'jobCategoryCodes' => ['CEO', 'COO'],
            'standardIndustrialCode' => 'XX21',
            'crossReferenceCode' => 'DKS',
            'comment' => 'sadljk',
            'addressType' => 'MA',
            'addressSeq' => 3,
            'isOkForNotes' => true,
            'isDisplayedNotes' => true,
            'isMatchingGift' => true,
            'isReviewed' => true,
            'reviewedBy' => 'kslkd',
            'isCoop' => true,
            'noteDate' => '2020-02-04',
            'createdBy' => '2020-08-05 04:06:07',
        ], CreateAlumniEmploymentRequest::createFromDTO(111111, $dto, 'MA', 3)->toJsonArray());
    }
}
