<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-14
 * Time: 15:32
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniLinkedInUrlRequestDTO;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniLinkedInUrlRequestDTO
 */
class CreateAlumniLinkedInUrlRequestDTOTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $linkedInUrl =[
            'prospectId' => 'A00849375',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'Test',
            'createdBy' => 'tester'
        ];
        
        $this->assertSame([
            'prospectId' => 'A00849375',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'Test',
            'createdBy' => 'tester'
        ], CreateAlumniLinkedInUrlRequestDTO::createFromArray($linkedInUrl)->toJsonArray());
    }
}
