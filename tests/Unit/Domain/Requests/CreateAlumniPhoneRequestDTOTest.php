<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;


use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequestDTO
 */
class CreateAlumniPhoneRequestDTOTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $phone = $this->mockCreateAlumniPhoneRequestDTO([
            'type' => 'A1',
            'areaCode' => '513',
            'number' => '1111111',
            'extension' => '8800'
        ]);

        $this->assertSame([
            'type' => 'A1',
            'areaCode' => '513',
            'number' => '1111111',
            'extension' => '8800'
        ], $phone->toJsonArray());
    }
}