<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;


use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest
 */
class CreateAlumniPhoneRequestTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $phone = $this->mockCreateAlumniPhoneRequest([
            'pidm' => 111111,
            'type' => 'A1',
            'areaCode' => '513',
            'number' => '1111111',
            'extension' => '8800'
        ]);

        $this->assertSame([
            'pidm' => 111111,
            'type' => 'A1',
            'areaCode' => '513',
            'number' => '1111111',
            'extension' => '8800'
        ], $phone->toJsonArray());
    }

    public function testCreateFromDTO()
    {
        $dto = $this->mockCreateAlumniPhoneRequestDTO([
            'type' => 'WRKE',
            'type' => 'A1',
            'areaCode' => '513',
            'number' => '1111111',
            'extension' => '8800'
        ]);

        $this->assertSame([
            'pidm' => 111111,
            'type' => 'A1',
            'areaCode' => '513',
            'number' => '1111111',
            'extension' => '8800'
        ], CreateAlumniPhoneRequest::createFromDTO(111111, $dto)->toJsonArray());
    }
}