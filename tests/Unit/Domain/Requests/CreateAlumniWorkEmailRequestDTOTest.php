<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;


use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequestDTO
 */
class CreateAlumniWorkEmailRequestDTOTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $email = $this->mockCreateAlumniWorkEmailRequestDTO([
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdfasdf',
            'isActive' => true,
            'isDispWeb' => true,
            'createdBy' => 'asdfasd'
        ]);

        $this->assertSame([
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdfasdf',
            'isActive' => true,
            'isDispWeb' => true,
            'createdBy' => 'asdfasd'
        ], $email->toJsonArray());
    }
}