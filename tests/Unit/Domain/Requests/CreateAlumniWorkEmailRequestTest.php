<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;


use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest
 */
class CreateAlumniWorkEmailRequestTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $email = $this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdfasdf',
            'isActive' => true,
            'isDispWeb' => true,
            'createdBy' => 'asdf'
        ]);

        $this->assertSame([
            'pidm' => 111111,
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdfasdf',
            'isActive' => true,
            'isDispWeb' => true,
            'createdBy' => 'asdf'
        ], $email->toJsonArray());
    }

    public function testCreateFromDTO()
    {
        $dto = $this->mockCreateAlumniWorkEmailRequestDTO([
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdfasdf',
            'isActive' => true,
            'isDispWeb' => true,
            'createdBy' => 'asdf'
        ]);

        $this->assertSame([
            'pidm' => 111111,
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdfasdf',
            'isActive' => true,
            'isDispWeb' => true,
            'createdBy' => 'asdf'
        ], CreateAlumniWorkEmailRequest::createFromDTO(111111, $dto)->toJsonArray());
    }
}