<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-14
 * Time: 20:43
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateAlumniLinkedInUrlRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\UpdateAlumniLinkedInUrlRequest
 */
class UpdateAlumniLinkedInUrlRequestTest extends TestCase
{
    public function testCanParseToJsonArray(): void
    {
        $linkedInUrl = new UpdateAlumniLinkedInUrlRequest(
            'A00849375',
            '1243',
            'https://www.linkedin.com/in/john-doe-1238392/',
            'hello world',
            'Tester'
        );
        
        $this->assertSame([
            'prospectId' => 'A00849375',
            'id' => '1243',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'hello world',
            'updatedBy' => 'Tester'
        ], $linkedInUrl->toJsonArray());
    }

    public function testCreateFromArray()
    {
        $linkedInUrl = [
            'prospectId' => 'A00849375',
            'id' => '1243',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'hello world',
            'updatedBy' => 'Tester'
        ];
        
        $this->assertSame([
            'prospectId' => 'A00849375',
            'id' => '1243',
            'url' => 'https://www.linkedin.com/in/john-doe-1238392/',
            'comment' => 'hello world',
            'updatedBy' => 'Tester'
        ], UpdateAlumniLinkedInUrlRequest::createFromArray($linkedInUrl)->toJsonArray());
    }
}
