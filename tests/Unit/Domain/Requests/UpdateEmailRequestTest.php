<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-14
 * Time: 20:30
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Requests;


use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest
 */
class UpdateEmailRequestTest extends TestCase
{
    public function testCanParseToJsonArray(): void
    {
        $emailRequest = new UpdateEmailRequest(
            '1234',
            false,
            'Test Comment',
            'tester'
        );
        
        $this->assertSame([
            'id' => '1234',
            'isPreferred' => false,
            'comment' => 'Test Comment',
            'updatedBy' => 'tester'
            
        ], $emailRequest->toJsonArray());
    }
}
