<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Results;


use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Results\CreateAlumniEmploymentResult
 */
class CreateAlumniEmploymentResultTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $data = $this->mockAlumniEmployment();

        $result = $this->mockCreateAlumniEmploymentResult([
            'isSuccess' => true,
            'data' => $data,
            'message' => 'asdklfasd',
            'errors' => [
                'a',
                'b'
            ]
        ]);

        $this->assertSame([
            'isSuccess' => true,
            'data' => $data->toJsonArray(),
            'message' => 'asdklfasd',
            'errors' => [
                'a',
                'b'
            ]
        ], $result->toJsonArray());
    }
}
