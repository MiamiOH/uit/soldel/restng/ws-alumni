<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-03-29
 * Time: 09:59
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Results;


use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use Carbon\Carbon;

/**
 * @covers  \MiamiOH\AlumniWebService\Domain\Results\CreateAlumniLinkedInUrlResult
 */
class CreateAlumniLinkedInUrlResultTest extends TestCase
{
    public function testConvertModelToArray()
    {
        $data = $this->mockAlumniLinkedInUrl();

        $result = $this->mockCreateAlumniLinkedUrlResult([
            'isSuccess' => true,
            'data' => $data,
            'message' => 'asdklfasd'
        ]);

        $this->assertSame([
            'isSuccess' => true,
            'data' => $data->toJsonArray(),
            'message' => 'asdklfasd'
        ], $result->toJsonArray());
    }
}
