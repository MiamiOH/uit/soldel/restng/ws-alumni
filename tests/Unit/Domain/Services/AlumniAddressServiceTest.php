<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-19
 * Time: 09:12
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Services;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniAddressRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Domain\Services\AlumniAddressService;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers  \MiamiOH\AlumniWebService\Domain\Services\AlumniAddressService
 */
class AlumniAddressServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $alumniAddressRepository;
    /**
     * @var AlumniAddressService
     */
    private $alumniAddessService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->alumniAddressRepository = $this->createMock(AlumniAddressRepository::class);
        $this->alumniAddessService = new AlumniAddressService($this->alumniAddressRepository);
    }

    public function testDeleteAddressRecord()
    {
        $this->alumniAddressRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('ASDFGQWET'));
        
        $this->alumniAddessService->delete('ASDFGQWET');
        $this->assertTrue(true);
    }
    
    public function testFindExistingAddress()
    {
        $isCreated = null;
        $request = new CreateAlumniAddressRequest(
            1234567,
            'MA',
            Carbon::create(2021, 1, 1),
            Carbon::create(2021, 2, 21),
            false,
            'Street line 1',
            '',
            '',
            'Oxford',
            'Ohio',
            '12345',
            ''
        );
        $addresses = $this->mockAlumniAddresses(3, [
            [
                'pidm' => 1234567,
                'addressType' => 'MA',
                'from' => Carbon::create(2021, 1, 1),
                'to' => Carbon::create(2021, 2, 21),
                'isPreferred' => false,
                'streetLine1' => 'Street line 1',
                'streetLine2' => '',
                'streetLine3' => '',                
                'city' => 'Oxford',
                'state' => 'Ohio',
                'postalCode' => '12345'
            ],
            [
                'pidm' => 1234567,
                'addressType' => 'MA',
                'streetLine1' => 'Street 2',
                'city' => 'Cincinnati',
            ],
            [
                'pidm' => 1234567,
                'addressType' => 'MA',
            ]
        ]);
        
        $this->alumniAddressRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo('1234567'),
                    $this->equalTo('MA'))
            ->willReturn($addresses);
        
        $address = $this->alumniAddessService->findOrCreate($request, $isCreated);
        $this->assertFalse($isCreated);
        $this->assertSame($addresses[0], $address);
    }
    
    public function testCreateANewAddressWhenItDoesNotExist()
    {
        $isCreated = null;
        $request = new CreateAlumniAddressRequest(
            1234567,
            'MA',
            Carbon::create(2021, 1, 1),
            Carbon::create(2021, 2, 21),
            false,
            'Street line 1',
            '',
            '',
            'Oxford',
            'Ohio',
            '12345',
            ''
        );
        $addresses = $this->mockAlumniAddresses();

        $this->alumniAddressRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(1234567), $this->equalTo('MA'))
            ->willReturn($addresses);

        $expectedAddress = $this->mockAlumniAddress();
        $this->alumniAddressRepository->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($expectedAddress);

        $address = $this->alumniAddessService->findOrCreate($request, $isCreated);
        $this->assertTrue($isCreated);
        $this->assertSame($expectedAddress, $address);
    }

    public function testFailedToCreateANewAddressRecord()
    {
        $request = new CreateAlumniAddressRequest(
            1234567,
            'MA',
            Carbon::create(2021, 1, 1),
            Carbon::create(2021, 2, 21),
            false,
            'Street line 1',
            '',
            '',
            'Oxford',
            'Ohio',
            '12345',
            ''
        );
        $this->alumniAddressRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->willThrowException(new \Exception());

        $this->expectException(ApplicationException::class);
        $isCreated = true;
        $this->alumniAddessService->findOrCreate($request, $isCreated);
    }

    public function testFailedToDeleteAddress()
    {
        $this->alumniAddressRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLKDJFDSF'))
            ->willThrowException(new \Exception());

        $this->expectException(ApplicationException::class);
        $this->alumniAddessService->delete('KSLKDJFDSF');
    }
}
