<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-19
 * Time: 09:09
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Services;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Domain\Services\AlumniLinkedInService;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Services\AlumniLinkedInService
 */
class AlumniLinkedInServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $alumniRepository;
    /**
     * @var MockObject
     */
    private $alumniWorkEmailRepository;
    /**
     * @var AlumniLinkedInService
     */
    private $alumniLinkedInService;

    protected function setUp(): void 
    {
        parent::setUp();
        
        $this->alumniRepository = $this->createMock(AlumniRepository::class);
        $this->alumniWorkEmailRepository = $this->createMock(AlumniWorkEmailRepository::class);
        $this->alumniLinkedInService = new AlumniLinkedInService(
          $this->alumniRepository,
          $this->alumniWorkEmailRepository  
        );
    }
    
    public function testCanGetLinkedIn()
    {
        $workEmails = $this->mockAlumniWorkEmails(2, [
            [
                'id' => 123456,
                'prospectId' => 'A00111111',
                'email' => 'aaa@miamioh.edu',
                'comment' => 'LinkedIn url 1',
                'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5)
            ],
            [
                'id' => 123456,
                'prospectId' => 'A00111111',
                'email' => 'bbb@miamioh.edu',
                'comment' => 'LinkedIn url 2',
                'updatedAt' => Carbon::create(2021, 1, 2, 3, 4, 5)
            ]
        ]);
        
        $this->alumniRepository
            ->method('getPidmByProspectId')
            ->with($this->equalTo('A00111111'))
            ->willReturn(123456);
        
        
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(123456),
                   $this->equalTo('LINK'))
            ->willReturn($workEmails);
        
        $getLinkedUrls = $this->alumniLinkedInService->get('A00111111');
        $this->assertCount(2, $getLinkedUrls);
        $this->assertSame([
            "id" => "123456",
            "prospectId" => "A00111111",
            "url" => "aaa@miamioh.edu",
            "comment" => "LinkedIn url 1",
            "updatedAt" => "2020-01-02 03:04:05" 
        ], $getLinkedUrls[0]->toJsonArray());
    }

    public function testCanGetLinkedInWithNoPidm()
    {
        $this->alumniRepository
            ->method('getPidmByProspectId')
            ->with($this->equalTo('A00111111'))
            ->willReturn(null);
        
        $getLinkedUrls = $this->alumniLinkedInService->get('A00111111');
        $this->assertEmpty($getLinkedUrls);
    }

    public function testFailGetLinkedIn()
    {
        $workEmails = $this->mockAlumniWorkEmails(2, [
            [
                'id' => 123456,
                'prospectId' => 'A00111111',
                'email' => 'aaa@miamioh.edu',
                'comment' => 'LinkedIn url 1',
                'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5)
            ],
            [
                'id' => 123456,
                'prospectId' => 'A00111111',
                'email' => 'bbb@miamioh.edu',
                'comment' => 'LinkedIn url 2',
                'updatedAt' => Carbon::create(2021, 1, 2, 3, 4, 5)
            ]
        ]);

        $this->alumniRepository
            ->method('getPidmByProspectId')
            ->with($this->equalTo('A00111111'))
            ->willReturn(123456);
        
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(123456),
                $this->equalTo('LINK'))
            ->willThrowException(new \Exception());
        $this->expectException(ApplicationException::class);

        $this->alumniLinkedInService->get('A00111111');
    }
    
    public function testCanCreate()
        
    {
        $workEmails = $this->mockAlumniWorkEmails(1, [
            [
                'id' => 123456,
                'prospectId' => 'A00111111',
                'email' => 'aaa@miamioh.edu',
                'comment' => 'LinkedIn url 1',
                'updatedAt' => Carbon::create(2020, 1, 2, 3, 4, 5)
            ]
        ]);
        
        $this->alumniRepository
            ->method('getPidmByProspectId')
            ->with($this->equalTo('A00111111'))
            ->willReturn(123456);
        
        $requests = $this->mockCreateAlumniLinkedInUrlRequestDTOs(1,[
            'prospectId' => 'A00111111'
        ]);
        $linkedInCollection = $this->alumniLinkedInService->create($requests);
        $this->assertCount(1, $linkedInCollection);
    }
}
