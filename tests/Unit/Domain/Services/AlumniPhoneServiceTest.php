<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Services;


use MiamiOH\AlumniWebService\Domain\Repositories\AlumniPhoneRepository;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Domain\Services\AlumniPhoneService;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Services\AlumniPhoneService
 */
class AlumniPhoneServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $alumniPhoneRepository;
    /**
     * @var AlumniPhoneService
     */
    private $alumniPhoneService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->alumniPhoneRepository = $this->createMock(AlumniPhoneRepository::class);
        $this->alumniPhoneService = new AlumniPhoneService($this->alumniPhoneRepository);
    }

    public function testDeleteAPhoneRecord()
    {
        $this->alumniPhoneRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLKDJFDSF'));

        $this->alumniPhoneService->delete('KSLKDJFDSF');
        $this->assertTrue(true);
    }

    public function testFailedToDeleteAPhoneRecord()
    {
        $this->alumniPhoneRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLKDJFDSF'))
            ->willThrowException(new \Exception());

        $this->expectException(ApplicationException::class);
        $this->alumniPhoneService->delete('KSLKDJFDSF');
    }

    public function testFindExistingPhoneRecord()
    {
        $isCreated = null;
        $request = new CreateAlumniPhoneRequest(
            111111,
            'A1',
            '513',
            '1111111',
            '1234'
        );
        $phones = $this->mockAlumniPhones(3, [
            [
                'pidm' => 111111,
                'phoneType' => 'A1',
                'areaPart' => '513',
                'numberPart' => '1111111',
                'ext' => '1234',
            ],
            [
                'pidm' => 111111,
                'phoneType' => 'A1',
                'areaPart' => '513',
                'numberPart' => '2222222',
                'ext' => '1234',
            ],
            [
                'pidm' => 2222222,
                'phoneType' => 'A1',
                'areaPart' => '513',
                'numberPart' => '1111111',
                'ext' => '1234',
            ]
        ]);

        $this->alumniPhoneRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('A1'))
            ->willReturn($phones);

        $phone = $this->alumniPhoneService->findOrCreate($request, $isCreated);
        $this->assertFalse($isCreated);
        $this->assertSame($phones[0], $phone);
    }

    public function testCreateANewRecordWhenItDoesNotExist()
    {
        $isCreated = null;
        $request = new CreateAlumniPhoneRequest(
            111111,
            'A1',
            '513',
            '1111111',
            '1234'
        );
        $phones = $this->mockAlumniPhones();

        $this->alumniPhoneRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('A1'))
            ->willReturn($phones);

        $expectedPhone = $this->mockAlumniPhone();
        $this->alumniPhoneRepository->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($expectedPhone);

        $phone = $this->alumniPhoneService->findOrCreate($request, $isCreated);
        $this->assertTrue($isCreated);
        $this->assertSame($expectedPhone, $phone);
    }

    public function testFailedToCreateANewPhoneRecord()
    {
        $this->alumniPhoneRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->willThrowException(new \Exception());

        $this->expectException(ApplicationException::class);
        $isCreated = true;
        $this->alumniPhoneService->findOrCreate($this->mockCreateAlumniPhoneRequest(), $isCreated);
    }
}
