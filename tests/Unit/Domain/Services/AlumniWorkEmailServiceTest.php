<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Services;


use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;
use MiamiOH\AlumniWebService\Domain\Services\AlumniWorkEmailService;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Services\AlumniWorkEmailService
 */
class AlumniWorkEmailServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $alumniWorkEmailRepository;
    /**
     * @var AlumniWorkEmailService
     */
    private $alumniWorkEmailService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->alumniWorkEmailRepository = $this->createMock(AlumniWorkEmailRepository::class);
        $this->alumniWorkEmailService = new AlumniWorkEmailService($this->alumniWorkEmailRepository);
    }

    public function testDeleteAPhoneRecord()
    {
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLKDJFDSF'));

        $this->alumniWorkEmailService->delete('KSLKDJFDSF');
        $this->assertTrue(true);
    }

    public function testFailedToDeleteAPhoneRecord()
    {
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('delete')
            ->with($this->equalTo('KSLKDJFDSF'))
            ->willThrowException(new \Exception());

        $this->expectException(ApplicationException::class);
        $this->alumniWorkEmailService->delete('KSLKDJFDSF');
    }

    public function testFailedToCreateANewEmailRecord()
    {
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->willThrowException(new \Exception());

        $this->expectException(ApplicationException::class);
        $isCreated = true;
        $this->alumniWorkEmailService->findOrCreate($this->mockCreateAlumniWorkEmailRequest(), $isCreated);
    }

    public function testDoNotCreateANewRecordButUseTheExistingEmailRecord()
    {
        $isCreated = null;
        $request = $this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'AAAA',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdf'
        ]);
        $emails = $this->mockAlumniWorkEmails(3, [
            [
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'aaa@miamioh.edu',
                'isPreferred' => true,
                'comment' => 'asdf'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'AAAA',
                'email' => 'aaa@miamioh.edu',
                'isPreferred' => true,
                'comment' => 'asdf'
            ],
            [
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'ccc@miamioh.edu',
                'isPreferred' => true,
                'comment' => 'asdf'
            ]
        ]);

        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('AAAA'))
            ->willReturn($emails);

        $email = $this->alumniWorkEmailService->findOrCreate($request, $isCreated);

        $this->assertFalse($isCreated);
        $this->assertSame($emails[1], $email);
    }

    public function testDoNotCreateANewRecordButUpdateTheExistingEmailIfTheOnlyDifferenceIsComment()
    {
        $isCreated = null;
        $request = $this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'AAAA',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdf',
            'createdBy' => 'asdf'
        ]);
        $emails = $this->mockAlumniWorkEmails(3, [
            [
                'id' => 'JLSKDJSLSPDFSD',
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'aaa@miamioh.edu',
                'isPreferred' => true,
                'comment' => 'asdf'
            ],
            [
                'id' => 'LKJLFIWOIERJLKDS',
                'pidm' => 111111,
                'typeCode' => 'AAAA',
                'email' => 'aaa@miamioh.edu',
                'comment' => 'asdfasdfa',
                'isPreferred' => true,
            ],
            [
                'id' => 'CSNCKJLSDKLFDS',
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'ccc@miamioh.edu',
                'comment' => 'asdf',
                'isPreferred' => true,
            ]
        ]);

        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('AAAA'))
            ->willReturn($emails);

        $updateEmail = $this->mockAlumniWorkEmail();
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('update')
            ->with($this->equalTo(new UpdateEmailRequest(
                'LKJLFIWOIERJLKDS',
                true,
                'asdf',
                'asdf'
            )))
            ->willReturn($updateEmail);

        $email = $this->alumniWorkEmailService->findOrCreate($request, $isCreated);

        $this->assertFalse($isCreated);
        $this->assertSame($updateEmail, $email);
    }

    public function testDoNotCreateANewRecordButUpdateTheExistingEmailIfTheOnlyDifferenceIsPreferredIndicator()
    {
        $isCreated = null;
        $request = $this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'AAAA',
            'email' => 'aaa@miamioh.edu',
            'isPreferred' => true,
            'comment' => 'asdf',
            'createdBy' => 'asdf'
        ]);
        $emails = $this->mockAlumniWorkEmails(3, [
            [
                'id' => 'JLSKDJSLSPDFSD',
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'aaa@miamioh.edu',
                'isPreferred' => false,
                'comment' => 'asdf'
            ],
            [
                'id' => 'LKJLFIWOIERJLKDS',
                'pidm' => 111111,
                'typeCode' => 'AAAA',
                'email' => 'aaa@miamioh.edu',
                'comment' => 'asdf',
                'isPreferred' => false,
            ],
            [
                'id' => 'CSNCKJLSDKLFDS',
                'pidm' => 111111,
                'typeCode' => 'WRKE',
                'email' => 'ccc@miamioh.edu',
                'comment' => 'asdf',
                'isPreferred' => false,
            ]
        ]);

        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('AAAA'))
            ->willReturn($emails);

        $updateEmail = $this->mockAlumniWorkEmail();
        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('update')
            ->with($this->equalTo(new UpdateEmailRequest(
                'LKJLFIWOIERJLKDS',
                true,
                'asdf',
                'asdf'
            )))
            ->willReturn($updateEmail);

        $email = $this->alumniWorkEmailService->findOrCreate($request, $isCreated);

        $this->assertFalse($isCreated);
        $this->assertSame($updateEmail, $email);
    }

    public function testCreateANewEmailIfItDoesNotExist()
    {
        $isCreated = null;
        $request = $this->mockCreateAlumniWorkEmailRequest([
            'pidm' => 111111,
            'type' => 'WRKE',
            'email' => 'aaa@miamioh.edu'
        ]);
        $emails = $this->mockAlumniWorkEmails();
        $email2 = $this->mockAlumniWorkEmail([
            'id' => 'KSLKDFJ',
            'pidm' => 111111,
            'emailCode' => 'WRKE',
            'emailAddress' => 'aaa@miamioh.edu'
        ]);

        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('WRKE'))
            ->willReturn($emails);

        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('create')
            ->with($this->equalTo($request))
            ->willReturn($email2);

        $email = $this->alumniWorkEmailService->findOrCreate($request, $isCreated);

        $this->assertTrue($isCreated);
        $this->assertSame($email2, $email);
    }
}
