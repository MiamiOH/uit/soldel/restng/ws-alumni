<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Domain\Services;


use MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniAddressRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniEmploymentRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniPhoneRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniRepository;
use MiamiOH\AlumniWebService\Domain\Repositories\AlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Domain\Services\GetAlumniEmploymentHistory;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Domain\Services\GetAlumniEmploymentHistory
 */
class GetAlumniEmploymentHistoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $alumniRepository;
    /**
     * @var MockObject
     */
    private $alumniAddressRepository;
    /**
     * @var MockObject
     */
    private $alumniEmploymentRepository;
    /**
     * @var MockObject
     */
    private $alumniPhoneRepository;
    /**
     * @var MockObject
     */
    private $alumniWorkEmailRepository;
    /**
     * @var GetAlumniEmploymentHistory
     */
    private $getAlumniEmploymentHistory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->alumniRepository = $this->createMock(AlumniRepository::class);
        $this->alumniAddressRepository = $this->createMock(AlumniAddressRepository::class);
        $this->alumniEmploymentRepository = $this->createMock(AlumniEmploymentRepository::class);
        $this->alumniPhoneRepository = $this->createMock(AlumniPhoneRepository::class);
        $this->alumniWorkEmailRepository = $this->createMock(AlumniWorkEmailRepository::class);
        $this->getAlumniEmploymentHistory = new GetAlumniEmploymentHistory(
            $this->alumniRepository,
            $this->alumniAddressRepository,
            $this->alumniEmploymentRepository,
            $this->alumniPhoneRepository,
            $this->alumniWorkEmailRepository
        );
    }

    public function testTheAlumniDoesNotHaveAnyEmploymentRecord()
    {
        $this->alumniRepository
            ->expects($this->once())
            ->method('getPidmByProspectId')
            ->with($this->equalTo('A0293049'))
            ->willReturn(null);

        $employments = $this->getAlumniEmploymentHistory->byProspectId('A0293049');
        $this->assertCount(0, $employments);
    }

    public function testGetAllRecordByProspectId()
    {
        $this->alumniRepository
            ->expects($this->once())
            ->method('getPidmByProspectId')
            ->with($this->equalTo('A0293049'))
            ->willReturn(111111);

        $this->alumniEmploymentRepository
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo(111111))
            ->willReturn($this->mockAlumniEmploymentDTOs(3, [
                [
                    'addressType' => 'A1',
                    'addressSeq' => 1
                ],
                [
                    'addressType' => 'A2',
                    'addressSeq' => 1
                ],
                [
                    'addressType' => 'A1',
                    'addressSeq' => 2
                ]
            ]));

        $this->alumniAddressRepository
            ->expects($this->once())
            ->method('getByPidmAndTypes')
            ->with($this->equalTo(111111), $this->equalTo(['A1', 'A2']))
            ->willReturn(new AlumniAddressCollection());

        $this->alumniPhoneRepository
            ->expects($this->once())
            ->method('getByPidmAndTypes')
            ->with($this->equalTo(111111), $this->equalTo(['A1', 'A2']))
            ->willReturn($this->mockAlumniPhones(3));

        $this->alumniWorkEmailRepository
            ->expects($this->once())
            ->method('getByPidmAndType')
            ->with($this->equalTo(111111), $this->equalTo('WRKE'))
            ->willReturn($this->mockAlumniWorkEmails(3));

        $employments = $this->getAlumniEmploymentHistory->byProspectId('A0293049');
        $this->assertCount(3, $employments);
    }
}