<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Exceptions;


use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Exceptions\ApplicationException
 */
class ApplicationExceptionTest extends TestCase
{
    public function testGetHierarchyErrorMessages()
    {
        $e = new ApplicationException('a', 0,
            new ApplicationException('b', 0,
                new ApplicationException('c')
            )
        );

        $this->assertSame([
            'a',
            'b',
            'c'
        ], $e->getErrors());
    }

    public function testCanGetJsonArray(): void
    {
      $e = new ApplicationException('a', 0,
        new ApplicationException('b', 0,
          new ApplicationException('c')
        )
      );

      $this->assertSame(
      ['message' => "a",
        'errors' => [
        'a',
        'b',
        'c'
      ]], $e->toJsonArray());
    }
}
