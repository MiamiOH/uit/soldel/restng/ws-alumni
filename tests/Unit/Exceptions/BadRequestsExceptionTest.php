<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Exceptions;


use MiamiOH\AlumniWebService\Exceptions\BadRequestsException;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;

/**
 * @covers \MiamiOH\AlumniWebService\Exceptions\BadRequestsException
 */
class BadRequestsExceptionTest extends TestCase
{
    public function testCreateWithErrors()
    {
        $e = new BadRequestsException('asdf', ['a', 'b']);
        $this->assertSame(['a', 'b'], $e->getErrors());
    }
}