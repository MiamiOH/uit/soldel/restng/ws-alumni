<?php


namespace MiamiOH\AlumniWebService\Tests\Unit\Infrastructure\Eloquent\Repositories;


use Carbon\Carbon;
use Illuminate\Database\ConnectionInterface;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumniEmployment;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentEmployer;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniEmploymentRepository;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniEmploymentRepository
 */
class EloquentAlumniEmploymentRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $eloquentAlumniEmploymentModel;
    /**
     * @var MockObject
     */
    private $eloquentEmployerModel;
    /**
     * @var EloquentAlumniEmploymentRepository
     */
    private $eloquentAlumniEmploymentRepository;
    /**
     * @var MockObject
     */
    private $db;

    protected function setUp(): void
    {
        parent::setUp();
        $this->db = $this->createMock(ConnectionInterface::class);
        $this->eloquentAlumniEmploymentModel = $this->getMockBuilder(EloquentAlumniEmployment::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'byPidm',
                'max',
                'bySeq',
                'first',
                'withRelations',
                'orderBySeq',
                'create',
                'get',
                'byPidmAndSeq'
            ])
            ->getMock();

        $this->eloquentEmployerModel = $this->getMockBuilder(EloquentEmployer::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'byId',
                'first'
            ])
            ->getMock();

        $this->eloquentAlumniEmploymentRepository = new EloquentAlumniEmploymentRepository(
            $this->eloquentAlumniEmploymentModel,
            $this->eloquentEmployerModel,
            $this->db
        );
    }

    public function testEmploymentHistoryByPidm()
    {
        $this->eloquentAlumniEmploymentModel
            ->expects($this->once())
            ->method('byPidm')
            ->with($this->equalTo(111111))
            ->willReturnSelf();
        $this->eloquentAlumniEmploymentModel
            ->expects($this->once())
            ->method('withRelations')
            ->willReturnSelf();
        $this->eloquentAlumniEmploymentModel
            ->expects($this->once())
            ->method('orderBySeq')
            ->willReturnSelf();
        $this->eloquentAlumniEmploymentModel
            ->expects($this->once())
            ->method('get')
            ->willReturn($this->mockEloquentAlumniEmployments(2, [
                [
                    'getId' => 'KSLFJDKFLS',
                    'getPidm' => 111111,
                    'getEmployer' => 'Miami University',
                    'getProspectId' => 'A00111111',
                    'getJobCategoryCode' => 'CEO',
                    'getPosition' => 'CEO & Founder',
                    'getFrom' => Carbon::create(2020, 1, 1, 0, 0, 0),
                    'getTo' => null,
                    'getEmployerPidm' => null,
                    'getEmployerId' => null,
                    'getEmployerName' => null,
                    'getActivityDate' => Carbon::create(2020, 8, 24, 2, 4, 2),
                    'getStandardIndustrialCode' => '92XX',
                    'getCrossReferenceCode' => 'FMG',
                    'getEmploymentStatusCode' => 'C',
                    'getWeeklyHours' => 40.5,
                    'isPrimary' => true,
                    'getAddressType' => 'A1',
                    'getAddressSeq' => 3,
                    'getJobCategoryCode2' => 'CON',
                    'getJobCategoryCode3' => null,
                    'getJobCategoryCode4' => null,
                    'getComments' => 'Jen found this on the LinkedIn',
                    'getSeq' => 2,
                    'getUser' => 'jenlab',
                    'isOkForNotes' => true,
                    'isDisplayedNotes' => true,
                    'isMatchingGift' => false,
                    'isReviewed' => true,
                    'getReviewedBy' => 'coobla',
                    'isCoop' => false,
                    'getNoteDate' => Carbon::create(2020, 3, 5, 2, 5, 7),
                ],
                [
                    'getId' => 'KFLDSJFKSLD',
                    'getPidm' => 111111,
                    'getEmployer' => null,
                    'getProspectId' => 'A00111111',
                    'getJobCategoryCode' => null,
                    'getPosition' => 'Business manager',
                    'getFrom' => Carbon::create(2019, 1, 1, 0, 0, 0),
                    'getTo' => Carbon::create(2019, 12, 31, 23, 59, 59),
                    'getEmployerPidm' => 138293,
                    'getEmployerId' => 'THREM',
                    'getEmployerName' => '3M',
                    'getActivityDate' => Carbon::create(2020, 8, 24, 2, 4, 2),
                    'getStandardIndustrialCode' => null,
                    'getCrossReferenceCode' => null,
                    'getEmploymentStatusCode' => 'D',
                    'getWeeklyHours' => null,
                    'isPrimary' => false,
                    'getAddressType' => null,
                    'getAddressSeq' => null,
                    'getJobCategoryCode2' => null,
                    'getJobCategoryCode3' => null,
                    'getJobCategoryCode4' => null,
                    'getComments' => null,
                    'getSeq' => 1,
                    'getUser' => 'ckald',
                    'isOkForNotes' => false,
                    'isDisplayedNotes' => false,
                    'isMatchingGift' => false,
                    'isReviewed' => false,
                    'getReviewedBy' => null,
                    'isCoop' => false,
                    'getNoteDate' => null,
                ]
            ]));
        $this->assertSame([
            [
                'id' => 'KSLFJDKFLS',
                'seq' => 2,
                'prospectId' => 'A00111111',
                'employer' => 'Miami University',
                'employerId' => null,
                'position' => 'CEO & Founder',
                'isPrimary' => true,
                'from' => '2020-01-01',
                'to' => null,
                'jobCategoryCodes' => ['CEO', 'CON'],
                'standardIndustrialCode' => '92XX',
                'crossReferenceCode' => 'FMG',
                'statusCode' => 'C',
                'weeklyHours' => 40.5,
                'comment' => 'Jen found this on the LinkedIn',
                'isOkForNotes' => true,
                'isDisplayedNotes' => true,
                'isMatchingGift' => false,
                'isReviewed' => true,
                'reviewedBy' => 'coobla',
                'isCoop' => false,
                'noteDate' => '2020-03-05',
                'user' => 'jenlab',
                'addressType' => 'A1',
                'addressSeq' => 3,
                'updatedAt' => '2020-08-24 02:04:02',
            ],
            [
                'id' => 'KFLDSJFKSLD',
                'seq' => 1,
                'prospectId' => 'A00111111',
                'employer' => '3M',
                'employerId' => 'THREM',
                'position' => 'Business manager',
                'isPrimary' => false,
                'from' => '2019-01-01',
                'to' => '2019-12-31',
                'jobCategoryCodes' => [],
                'standardIndustrialCode' => null,
                'crossReferenceCode' => null,
                'statusCode' => 'D',
                'weeklyHours' => null,
                'comment' => null,
                'isOkForNotes' => false,
                'isDisplayedNotes' => false,
                'isMatchingGift' => false,
                'isReviewed' => false,
                'reviewedBy' => null,
                'isCoop' => false,
                'noteDate' => null,
                'user' => 'ckald',
                'addressType' => null,
                'addressSeq' => null,
                'updatedAt' => '2020-08-24 02:04:02',
            ]
        ], $this->eloquentAlumniEmploymentRepository->get(111111)->toJsonArray());
    }

    /**
     *
     */
//    public function testCanCreateAlumniEmployement(): void
//    {
//      $this->markTestIncomplete('Closure used making it harder to mock');
//    }

}
