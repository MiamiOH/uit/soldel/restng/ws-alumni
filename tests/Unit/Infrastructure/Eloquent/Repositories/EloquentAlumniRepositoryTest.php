<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Infrastructure\Eloquent\Repositories;

use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumni;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniRepository;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Infrastructure\Eloquent\Repositories\EloquentAlumniRepository
 */
class EloquentAlumniRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $eloquentAlumniModel;
    /**
     * @var EloquentAlumniRepository
     */
    private $eloquentAlumniRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->eloquentAlumniModel = $this->getMockBuilder(EloquentAlumni::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'byProspectId',
                'first'
            ])
            ->getMock();
        $this->eloquentAlumniRepository = new EloquentAlumniRepository($this->eloquentAlumniModel);
    }

    public function testPidmNotFoundForAGivenProspectId()
    {
        $this->eloquentAlumniModel->method('byProspectId')
            ->with($this->equalTo('A0012934'))
            ->willReturnSelf();

        $this->eloquentAlumniModel->method('first')
            ->willReturn(null);

        $this->assertNull($this->eloquentAlumniRepository->getPidmByProspectId('A0012934'));
    }

    public function testGetPidmFromProspectId()
    {
        $this->eloquentAlumniModel->method('byProspectId')
            ->with($this->equalTo('A0012934'))
            ->willReturnSelf();

        $this->eloquentAlumniModel->method('first')
            ->willReturn($this->mockEloquentAlumni([
                'getPidm' => 1392049
            ]));

        $this->assertSame(1392049, $this->eloquentAlumniRepository->getPidmByProspectId('A0012934'));
    }
}