<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 2021-01-14
 * Time: 21:01
 */

namespace MiamiOH\AlumniWebService\Tests\Unit\Infrastructure\RESTng;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniAddressRepository;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers  \MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniAddressRepository
 * @covers  \MiamiOH\AlumniWebService\Infrastructure\RESTng\BaseRESTngRepository
 */
class RESTngAlumniAddressRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $app;
    /**
     * @var RESTngAlumniAddressRepository
     */
    private $restngAlumniAddressRepository;

    protected function setUp(): void 
    {
        parent::setUp();
        
        $this->app = $this->createMock(App::class);
        $this->restngAlumniAddressRepository = new RESTngAlumniAddressRepository($this->app);
    }

    public function testGetByPidmAndTypes()
    {
        $response = $this->createMock(Response::class);
        $response
            ->method('getPayLoad')
            ->willReturn([
                [
                    'pidm' =>  111111,
                    'addressType' => 'MA',
                    'sequenceNumber' => '1',
                    'fromDate' =>  '2012-11-16',
                    'toDate' =>  '2020-11-16',
                    'streetLine1' => 'Street 1' ,
                    'streetLine2' =>  '',
                    'streetLine3' =>  '',
                    'city' =>  'Oxford',
                    'state' =>  'Ohio',
                    'postalCode' =>  '12345',
                    'nationBannerCode' =>  '',
                    'nationDescription' =>  '',
                    'status' => 'active' ,
                    'id' =>  'AAAV3IAATAAH3RWAAc',
                    'addressSourceCode' => '' 
                ]
            ]);
        
        $this->app->method('callResource')
            ->with(
                $this->equalTo('person.address.v2.get'),
                $this->equalTo([
                    'options' => [
                        'pidm' => 111111,
                        'addressType' => 'MA',
                        'status' => 'active'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);
        
        $address = $this->restngAlumniAddressRepository->getByPidmAndType(111111, 'MA');
        $this->assertCount(1, $address);
        $this->assertSame([
            [
                "id" => "AAAV3IAATAAH3RWAAc",
                "pidm" => 111111,
                "addressType" => "MA",
                "streetLine1" => "Street 1",
                "streetLine2" => "",
                "streetLine3" => "",
                "city" => "Oxford",
                "state" => "Ohio",
                "postalCode" => "12345",
                "nationBannerCode" => "",
                "nationDescription" => "",
                "sequenceNumber" => 1,
                "status" => "active",
                "from" => "2012-11-16",
                "to" => "2020-11-16",
                "addressSourceCode" => ""
            ]
        ], $address->toJsonArray());
    }
    
    public function testCanCreateAddress()
    {
        $response = $this->createMock(Response::class);
        $response
            ->method('getPayLoad')
            ->willReturn([
                [
                    'status' => App::API_CREATED, 
                    'address' => [
                        'pidm' =>  111111,
                        'addressType' => 'MA',
                        'sequenceNumber' => '1',
                        'fromDate' =>  '2012-11-16',
                        'toDate' =>  '2020-11-16',
                        'streetLine1' => 'Street 1' ,
                        'streetLine2' =>  '',
                        'streetLine3' =>  '',
                        'city' =>  'Oxford',
                        'state' =>  'Ohio',
                        'postalCode' =>  '12345',
                        'nationBannerCode' =>  '',
                        'nationDescription' =>  '',
                        'status' => null ,
                        'id' =>  'AAAV3IAATAAH3RWAAc',
                        'addressSourceCode' => ''
                    ]
                ]
                
            ]);
        $response->method('getStatus')->willReturn(200);
        
        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.address.v2.muid.create'),
                $this->equalTo([
                    'params' => [
                        'muid' => 'pidm=111111',
                    ],
                    'data' => [
                        [
                            'pidm' => '111111',
                            'addressType' => 'MA',
                            'streetLine1' => 'Street 1' ,
                            'streetLine2' =>  '',
                            'streetLine3' =>  '',
                            'city' =>  'Oxford',
                            'state' =>  'Ohio',
                            'postalCode' => '12345',
                            'fromDate' =>  '2012-11-16',
                            'toDate' =>  '2020-11-16',
                            'status' => null ,
                            'addressSourceCode' => ''
                        ]
                    ]
            ]),
             $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);
        
        $addressRequest =  $this->mockCreateAlumniAddressRequest([
            'pidm' => 111111,
            'type' => 'MA',
            'from' => Carbon::create(2012, 11, 16),
            'to' => Carbon::create(2020, 11, 16),
            'isPreferred' => false,
            'streetLine1' => 'Street 1',
            'streetLine2' => '',
            'streetLine3' => '',
            'city' => 'Oxford',
            'state' => 'Ohio',
            'zip' => '12345',
            'status' => null,
            'addressSourceCode' => ''
        ]);

        $address = $this->restngAlumniAddressRepository->create($addressRequest);

        $this->assertSame([
            "id" => "AAAV3IAATAAH3RWAAc",
            "pidm" => 111111,
            "addressType" => "MA",
            "streetLine1" => "Street 1",
            "streetLine2" => "",
            "streetLine3" => "",
            "city" => "Oxford",
            "state" => "Ohio",
            "postalCode" => "12345",
            "nationBannerCode" => "",
            "nationDescription" => "",
            "sequenceNumber" => 1,
            "status" => null,
            "from" => "2012-11-16",
            "to" => "2020-11-16",
            "addressSourceCode" => ""
        ], $address->toJsonArray());
    }
      
    public function testCanDeleteAddress()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([]);
        $response->method('getStatus')->willReturn(App::API_OK);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.contact.v1.id.delete'),
                $this->equalTo([
                    'params' => [
                        'id' => 'ABDKSLEKFJ'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->restngAlumniAddressRepository->delete('ABDKSLEKFJ');
        $this->assertTrue(true);
    }

    public function testFailedToDeleteAddressDueToIDNotFound()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn(['message' => 'message']);
        $response->method('getStatus')->willReturn(App::API_FAILED);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.contact.v1.id.delete'),
                $this->equalTo([
                    'params' => [
                        'id' => 'ABDKSLEKFJ'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->expectException(ApplicationException::class);
        $this->restngAlumniAddressRepository->delete('ABDKSLEKFJ');
    }
}
