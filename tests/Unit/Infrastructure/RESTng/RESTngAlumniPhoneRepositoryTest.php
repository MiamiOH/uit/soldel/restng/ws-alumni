<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Infrastructure\RESTng;

use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniPhoneRepository;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniPhoneRepository
 * @covers \MiamiOH\AlumniWebService\Infrastructure\RESTng\BaseRESTngRepository
 */
class RESTngAlumniPhoneRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $app;
    /**
     * @var RESTngAlumniPhoneRepository
     */
    private $restngAlumniPhoneRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app = $this->createMock(App::class);
        $this->restngAlumniPhoneRepository = new RESTngAlumniPhoneRepository($this->app);
    }

    public function testGetByPidmAndType()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            [
                "areaPart" => "513",
                "numberPart" => "1234567",
                "pidm" => "111111",
                "phoneType" => "A1",
                "phoneDesc" => "Alumni Primary Phone",
                "activityDate" => "14-JUL-14",
                "sequenceNumber" => "4",
                "isPrimary" => true,
                "status" => "active",
                "ext" => "8800",
                "id" => "AAAWV8AAUAAHPE6ABG",
                "phoneNumber" => "+15131234567",
                "phoneNumberNational" => "(513) 123-4567",
                "phoneNumberInternational" => "+1 513-123-4567"
            ]
        ]);

        $this->app->method('callResource')
            ->with(
                $this->equalTo('person.phone.v1'),
                $this->equalTo([
                    'options' => [
                        'pidm' => 111111,
                        'phoneType' => 'A1',
                        'status' => 'active'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $phones = $this->restngAlumniPhoneRepository->getByPidmAndType(111111, 'A1');
        $this->assertCount(1, $phones);
        $this->assertSame([
            [
                'id' => "AAAWV8AAUAAHPE6ABG",
                'pidm' => 111111,
                'phoneType' => 'A1',
                'phoneDesc' => 'Alumni Primary Phone',
                'isPrimary' => true,
                'areaPart' => '513',
                'numberPart' => '1234567',
                'phoneNumber' => '+15131234567',
                'ext' => '8800',
                'phoneNumberNational' => '(513) 123-4567',
                'phoneNumberInternational' => '+1 513-123-4567',
                'sequenceNumber' => 4,
                'status' => 'active',
                'activityDate' => '2014-07-14 00:00:00',
            ]
        ], $phones->toJsonArray());
    }

    public function testGetByPidmAndMultipleTypes()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            [
                "areaPart" => "513",
                "numberPart" => "1234567",
                "pidm" => "111111",
                "phoneType" => "A1",
                "phoneDesc" => "Alumni Primary Phone",
                "activityDate" => "14-JUL-14",
                "sequenceNumber" => "4",
                "isPrimary" => true,
                "status" => "active",
                "ext" => "8800",
                "id" => "AAAWV8AAUAAHPE6ABG",
                "phoneNumber" => "+15131234567",
                "phoneNumberNational" => "(513) 123-4567",
                "phoneNumberInternational" => "+1 513-123-4567"
            ],
            [
                "areaPart" => "513",
                "numberPart" => "1234567",
                "pidm" => "111111",
                "phoneType" => "A2",
                "phoneDesc" => "Alumni Primary Phone",
                "activityDate" => "14-JUL-14",
                "sequenceNumber" => "4",
                "isPrimary" => true,
                "status" => "active",
                "ext" => "8800",
                "id" => "AAAWV8AAUAAHPE6ABG",
                "phoneNumber" => "+15131234567",
                "phoneNumberNational" => "(513) 123-4567",
                "phoneNumberInternational" => "+1 513-123-4567"
            ]
        ]);

        $this->app->method('callResource')
            ->with(
                $this->equalTo('person.phone.v1'),
                $this->equalTo([
                    'options' => [
                        'pidm' => 111111,
                        'phoneType' => 'A1,A2',
                        'status' => 'active'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $phones = $this->restngAlumniPhoneRepository->getByPidmAndTypes(111111, ['A1', 'A2']);
        $this->assertCount(2, $phones);
    }

    public function testDeleteAPhoneByID()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([]);
        $response->method('getStatus')->willReturn(App::API_OK);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.phone.delete.id'),
                $this->equalTo([
                    'params' => [
                        'id' => 'ABDKSLEKFJ'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->restngAlumniPhoneRepository->delete('ABDKSLEKFJ');
        $this->assertTrue(true);
    }

    public function testFailedToDeleteAPhoneDueToIDNotFound()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn(['message' => 'asdf']);
        $response->method('getStatus')->willReturn(App::API_FAILED);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.phone.delete.id'),
                $this->equalTo([
                    'params' => [
                        'id' => 'ABDKSLEKFJ'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->expectException(ApplicationException::class);
        $this->restngAlumniPhoneRepository->delete('ABDKSLEKFJ');
    }

    public function testCreateANewPhoneRecord()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            [
                'status' => App::API_CREATED,
                'message' => 'ok',
                'phone' => [
                    [
                        "areaPart" => "513",
                        "numberPart" => "1234567",
                        "pidm" => "111111",
                        "phoneType" => "A1",
                        "phoneDesc" => "Alumni Primary Phone",
                        "activityDate" => "14-JUL-14",
                        "sequenceNumber" => "4",
                        "isPrimary" => true,
                        "status" => "active",
                        "ext" => "8800",
                        "id" => "AAAWV8AAUAAHPE6ABG",
                        "phoneNumber" => "+15131234567",
                        "phoneNumberNational" => "(513) 123-4567",
                        "phoneNumberInternational" => "+1 513-123-4567"
                    ]
                ]
            ]
        ]);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.phone.post'),
                $this->equalTo([
                    'params' => [
                        'muid' => 'pidm=111111'
                    ],
                    'data' => [
                        [
                            'phoneType' => 'A1',
                            'phoneNumber' => '+15131234567;ext=8800'
                        ]
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $phone = $this->restngAlumniPhoneRepository->create(new CreateAlumniPhoneRequest(
            111111,
            'A1',
            '513',
            '1234567',
            '8800'
        ));
        $this->assertSame([
            'id' => "AAAWV8AAUAAHPE6ABG",
            'pidm' => 111111,
            'phoneType' => 'A1',
            'phoneDesc' => 'Alumni Primary Phone',
            'isPrimary' => true,
            'areaPart' => '513',
            'numberPart' => '1234567',
            'phoneNumber' => '+15131234567',
            'ext' => '8800',
            'phoneNumberNational' => '(513) 123-4567',
            'phoneNumberInternational' => '+1 513-123-4567',
            'sequenceNumber' => 4,
            'status' => 'active',
            'activityDate' => '2014-07-14 00:00:00',
        ], $phone->toJsonArray());
    }

    public function testFailedToCreateANewPhoneRecord()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            [
                'status' => App::API_BADREQUEST,
                'message' => 'askldjl',
                'phone' => []
            ]
        ]);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.phone.post'),
                $this->equalTo([
                    'params' => [
                        'muid' => 'pidm=111111'
                    ],
                    'data' => [
                        [
                            'phoneType' => 'A1',
                            'phoneNumber' => '+15131234567;ext=8800'
                        ]
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->expectException(ApplicationException::class);
        $this->restngAlumniPhoneRepository->create(new CreateAlumniPhoneRequest(
            111111,
            'A1',
            '513',
            '1234567',
            '8800'
        ));
    }
}
