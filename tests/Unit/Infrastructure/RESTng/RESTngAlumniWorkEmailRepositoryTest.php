<?php

namespace MiamiOH\AlumniWebService\Tests\Unit\Infrastructure\RESTng;

use Carbon\Carbon;
use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Requests\UpdateEmailRequest;
use MiamiOH\AlumniWebService\Exceptions\ApplicationException;
use MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniWorkEmailRepository;
use MiamiOH\AlumniWebService\Tests\Unit\TestCase;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * @covers \MiamiOH\AlumniWebService\Infrastructure\RESTng\RESTngAlumniWorkEmailRepository
 * @covers \MiamiOH\AlumniWebService\Infrastructure\RESTng\BaseRESTngRepository
 */
class RESTngAlumniWorkEmailRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $app;
    /**
     * @var RESTngAlumniWorkEmailRepository
     */
    private $restngAlumniWorkEmailRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app = $this->createMock(App::class);
        $this->restngAlumniWorkEmailRepository = new RESTngAlumniWorkEmailRepository($this->app);
    }

    public function testGetByPidmAndType()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            [
                "id" => "AAAV3IAATAAH3RWAAc",
                "pidm" => 111111,
                "typeCode" => "APS",
                "type" => 'Work Email',
                "email" => "aaa@gmail.com",
                "isActive" => true,
                "isPreferred" => false,
                "updatedAt" => "2012-11-16 00:00:00",
                "updatedBy" => "SSD",
                "comment" => "Uploaded by SSD Axiom",
                "isDisplayedOnWeb" => false
            ]
        ]);

        $this->app->method('callResource')
            ->with(
                $this->equalTo('person.email.v2.search'),
                $this->equalTo([
                    'options' => [
                        'pidms' => 111111,
                        'typeCodes' => 'APS',
                        'isActive' => 'true'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $phones = $this->restngAlumniWorkEmailRepository->getByPidmAndType(111111, 'APS');
        $this->assertCount(1, $phones);
        $this->assertSame([
            [
                "id" => "AAAV3IAATAAH3RWAAc",
                "pidm" => 111111,
                "typeCode" => "APS",
                'type' => 'Work Email',
                "email" => "aaa@gmail.com",
                "isPreferred" => false,
                "isActive" => true,
                "isDisplayedOnWeb" => false,
                "comment" => "Uploaded by SSD Axiom",
                "updatedBy" => "SSD",
                "updatedAt" => "2012-11-16 00:00:00",
            ]
        ], $phones->toJsonArray());
    }

    public function testCreateANewEmail()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            "id" => "AAAV3IAATAAH3RWAAc",
            "pidm" => 111111,
            "typeCode" => "APS",
            "type" => 'Work Email',
            "email" => "aaa@gmail.com",
            "isActive" => true,
            "isPreferred" => true,
            "updatedAt" => "2012-11-16 00:00:00",
            "updatedBy" => "adsf",
            "comment" => "asdfjlasdkf",
            "isDisplayedOnWeb" => true
        ]);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.email.v2.create'),
                $this->equalTo([
                    'data' => [
                        "pidm" => 111111,
                        "typeCode" => "ADM",
                        "email" => "aaa@gmail.com",
                        "isActive" => true,
                        'isForced' => true,
                        "isPreferred" => true,
                        "comment" => "asdfjlasdkf",
                        "isDisplayedOnWeb" => true,
                        'createdBy' => 'adsf'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $email = $this->restngAlumniWorkEmailRepository->create(new CreateAlumniWorkEmailRequest(
            111111,
            'ADM',
            'aaa@gmail.com',
            true,
            'asdfjlasdkf',
            true,
            true,
            'adsf'
        ));

        $this->assertSame([
            "id" => "AAAV3IAATAAH3RWAAc",
            "pidm" => 111111,
            "typeCode" => "APS",
            "type" => 'Work Email',
            "email" => "aaa@gmail.com",
            "isPreferred" => true,
            "isActive" => true,
            "isDisplayedOnWeb" => true,
            "comment" => "asdfjlasdkf",
            "updatedBy" => "adsf",
            "updatedAt" => "2012-11-16 00:00:00",
        ], $email->toJsonArray());
    }

    public function testFailedToCreateANewEmail()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([
            'message' => 'email already exists',
            'errors' => []
        ]);
        $response->method('getStatus')->willReturn(500);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.email.v2.create'),
                $this->equalTo([
                    'data' => [
                        "pidm" => 111111,
                        "typeCode" => "APS",
                        "email" => "aaa@gmail.com",
                        "isPreferred" => true,
                        'isForced' => true,
                        "isActive" => true,
                        "isDisplayedOnWeb" => true,
                        "comment" => "asdfjlasdkf",
                        'createdBy' => 'adsf'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->expectException(ApplicationException::class);
        $this->restngAlumniWorkEmailRepository->create(new CreateAlumniWorkEmailRequest(
            111111,
            'APS',
            'aaa@gmail.com',
            true,
            'asdfjlasdkf',
            true,
            true,
            'adsf'
        ));
    }

    public function testDeleteAnEmailByID()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn([]);
        $response->method('getStatus')->willReturn(App::API_OK);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.email.v2.delete'),
                $this->equalTo([
                    'params' => [
                        'id' => 'ABDKSLEKFJ'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->restngAlumniWorkEmailRepository->delete('ABDKSLEKFJ');
        $this->assertTrue(true);
    }

    public function testFailedToDeleteAPhoneDueToIDNotFound()
    {
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn(['message' => 'asdf']);
        $response->method('getStatus')->willReturn(App::API_FAILED);

        $this->app->expects($this->once())
            ->method('callResource')
            ->with(
                $this->equalTo('person.email.v2.delete'),
                $this->equalTo([
                    'params' => [
                        'id' => 'ABDKSLEKFJ'
                    ]
                ]),
                $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);

        $this->expectException(ApplicationException::class);
        $this->restngAlumniWorkEmailRepository->delete('ABDKSLEKFJ');
    }

    public function testCanUpdateEmail(): void
    {
        $mockUpdateEmailRequest = new UpdateEmailRequest(
            '1692001',
            true,
            'Test',
            'WS_USER'
        );
      
        $response = $this->createMock(Response::class);
        $response->method('getPayload')->willReturn(
            [
                  "id" => 'AAAV3IAATAAH3RWAA',
                  "pidm" => 1692001,
                  "typeCode" => "APS",
                  "type" => 'Work Email',
                  "email" => "aaa@gmail.com",
                  "isActive" => true,
                  "isPreferred" => true,
                  "updatedAt" => "2012-11-16 00:00:00",
                  "updatedBy" => "WS_USER",
                  "comment" => "Test",
                  "isDisplayedOnWeb" => false
            ]);
    
        $response->method('getStatus')->willReturn(200);
    
        $this->app->expects($this->once())
            ->method('callResource')
            ->with( 
                $this->equalTo('person.email.v2.update'),
                $this->equalTo([
                    'params' => [
                        'id' => '1692001'
                    ],
                    'data' => [
                        
                          'isPreferred' => $mockUpdateEmailRequest->isPreferred(),
                          'updatedBy' => $mockUpdateEmailRequest->getId(),
                          'comment' => 'Test'
                    ]
                ]),
              $this->equalTo(['deferred' => false])
            )
            ->willReturn($response);
        
        $updatedEmail = $this->restngAlumniWorkEmailRepository->update($mockUpdateEmailRequest);
    
        $this->assertSame([
            "id" => "AAAV3IAATAAH3RWAA", 
            "pidm" => 1692001,
            "typeCode" => "APS",
            "type" => "Work Email",
            "email" => "aaa@gmail.com",
            "isPreferred" => true,
            "isActive" => true,
            "isDisplayedOnWeb" => false,
            "comment" => "Test",
            "updatedBy" => "WS_USER",
            "updatedAt" => "2012-11-16 00:00:00"
          ], $updatedEmail->toJsonArray());
    }
}
