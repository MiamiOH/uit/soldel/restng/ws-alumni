<?php

namespace MiamiOH\AlumniWebService\Tests\Unit;

use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Database\Eloquent\Collection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniAddressCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniEmploymentCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniEmploymentDTOCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniLinkedInUrlCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniPhoneCollection;
use MiamiOH\AlumniWebService\Domain\Collections\AlumniWorkEmailCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniEmploymentResultCollection;
use MiamiOH\AlumniWebService\Domain\Collections\CreateAlumniLinkedInUrlRequestDTOCollection;
use MiamiOH\AlumniWebService\Domain\Models\AlumniAddress;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmployment;
use MiamiOH\AlumniWebService\Domain\Models\AlumniEmploymentDTO;
use MiamiOH\AlumniWebService\Domain\Models\AlumniLinkedInUrl;
use MiamiOH\AlumniWebService\Domain\Models\AlumniPhone;
use MiamiOH\AlumniWebService\Domain\Models\AlumniWorkEmail;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniAddressRequestDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniEmploymentRequestDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniLinkedInUrlRequestDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniPhoneRequestDTO;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequest;
use MiamiOH\AlumniWebService\Domain\Requests\CreateAlumniWorkEmailRequestDTO;
use MiamiOH\AlumniWebService\Domain\Results\CreateAlumniEmploymentResult;
use MiamiOH\AlumniWebService\Domain\Results\CreateAlumniLinkedInUrlResult;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumni;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentAlumniEmployment;
use MiamiOH\AlumniWebService\Infrastructure\Eloquent\Models\EloquentEmployer;

trait MockModels
{
    /**
     * @var Generator
     */
    private $faker;

    /***********************************************************************************
     * Mock Models
     ***********************************************************************************/

    protected function mockAlumniEmployment(array $data = []): AlumniEmployment
    {
        $from = $this->randomDate();
        $to = $from->copy()->addDays($this->faker->numberBetween(10, 100));
        return $this->mockObject(AlumniEmployment::class, [
            'id' => $this->randomLetterOnlyString(10),
            'prospectId' => $this->randomProspectId(),
            'seq' => $this->faker->randomNumber(),
            'employer' => $this->faker->word,
            'employerId' => $this->randomEmployerId(),
            'position' => $this->faker->word,
            'isPrimary' => $this->faker->boolean,
            'from' => $from,
            'to' => $to,
            'jobCategoryCodes' => [
                $this->randomJobCategoryCode()
            ],
            'standardIndustrialCode' => $this->randomStandardIndustrialCode(),
            'crossReferenceCode' => $this->randomCrossReferenceCode(),
            'statusCode' => $this->randomAlumniEmploymentStatusCode(),
            'weeklyHours' => $this->faker->randomFloat(null, 10, 80),
            'comment' => $this->faker->text,
            'address' => null,
            'phones' => $this->mockAlumniPhones(2),
            'workEmails' => $this->mockAlumniWorkEmails(2),
            'user' => $this->randomUniqueId(),
            'updatedAt' => $this->randomDate(),
            'isOkForNotes' => $this->faker->boolean,
            'isDisplayedNotes' => $this->faker->boolean,
            'isMatchingGift' => $this->faker->boolean,
            'isReviewed' => $this->faker->boolean,
            'reviewedBy' => $this->randomUniqueId(),
            'isCoop' => $this->faker->boolean,
            'noteDate' => $this->randomDate(),
        ], $data);
    }

    protected function mockAlumniEmployments(int $number = 1, array $data = []): AlumniEmploymentCollection
    {
        return $this->mockCollection(
            AlumniEmploymentCollection::class,
            function (array $d) {
                return $this->mockAlumniEmployment($d);
            },
            $number,
            $data
        );
    }

    protected function mockAlumniEmploymentDTO(array $data = []): AlumniEmploymentDTO
    {
        $from = $this->randomDate();
        $to = $from->copy()->addDays($this->faker->numberBetween(10, 100));
        return $this->mockObject(AlumniEmploymentDTO::class, [
            'id' => $this->randomLetterOnlyString(10),
            'prospectId' => $this->randomProspectId(),
            'seq' => $this->faker->randomNumber(),
            'employer' => $this->faker->word,
            'employerId' => $this->randomEmployerId(),
            'position' => $this->faker->word,
            'isPrimary' => $this->faker->boolean,
            'from' => $from,
            'to' => $to,
            'jobCategoryCodes' => [
                $this->randomJobCategoryCode()
            ],
            'standardIndustrialCode' => $this->randomStandardIndustrialCode(),
            'crossReferenceCode' => $this->randomCrossReferenceCode(),
            'statusCode' => $this->randomAlumniEmploymentStatusCode(),
            'weeklyHours' => $this->faker->randomFloat(null, 10, 80),
            'comment' => $this->faker->text,
            'isOkForNotes' => $this->faker->boolean,
            'isDisplayedNotes' => $this->faker->boolean,
            'isMatchingGift' => $this->faker->boolean,
            'isReviewed' => $this->faker->boolean,
            'reviewedBy' => $this->randomUniqueId(),
            'isCoop' => $this->faker->boolean,
            'noteDate' => $this->randomDate(),
            'user' => $this->randomUniqueId(),
            'addressType' => $this->faker->randomKey([
                'A1',
                'A2'
            ]),
            'addressSeq' => $this->faker->randomNumber(),
            'updatedAt' => $this->randomDate(),
        ], $data);
    }

    protected function mockAlumniEmploymentDTOs(int $number = 1, array $data = []): AlumniEmploymentDTOCollection
    {
        return $this->mockCollection(
            AlumniEmploymentDTOCollection::class,
            function (array $d) {
                return $this->mockAlumniEmploymentDTO($d);
            },
            $number,
            $data
        );
    }

    protected function mockAlumniLinkedInUrl(array $data = []): AlumniLinkedInUrl
    {
        return $this->mockObject(AlumniLinkedInUrl::class, [
            'id' => $this->faker->text,
            'prospectId' => $this->randomProspectId(),
            'url' => $this->faker->url,
            'comment' => $this->faker->text,
            'updatedAt' => $this->randomDate(),
        ], $data);
    }

    protected function mockAlumniLinkedInUrls(int $number = 1, array $data = []): AlumniLinkedInUrlCollection
    {
        return $this->mockCollection(
            AlumniLinkedInUrlCollection::class,
            function (array $d) {
                return $this->mockAlumniLinkedInUrl($d);
            },
            $number,
            $data
        );
    }

    protected function mockCreateAlumniLinkedUrlResult(array $data = []): CreateAlumniLinkedInUrlResult
    {
        return $this->mockObject(CreateAlumniLinkedInUrlResult::class, [
            'isSuccess' => $this->faker->boolean,
            'data' => $this->mockAlumniLinkedInUrls(),
            'message' => $this->faker->text,
            'errors' => [],
        ], $data);
    }
    
    protected function mockCreateAlumniEmploymentResult(array $data = []): CreateAlumniEmploymentResult
    {
        return $this->mockObject(CreateAlumniEmploymentResult::class, [
            'isSuccess' => $this->faker->boolean,
            'data' => $this->mockAlumniEmployment(),
            'message' => $this->faker->text,
            'errors' => [],
        ], $data);
    }

    protected function mockCreateAlumniEmploymentResults(int $number = 1, array $data = []): CreateAlumniEmploymentResultCollection
    {
        return $this->mockCollection(
            CreateAlumniEmploymentResultCollection::class,
            function (array $d) {
                return $this->mockCreateAlumniEmploymentResult($d);
            },
            $number,
            $data
        );
    }
    
    protected function mockAlumniAddress(array $data = []): AlumniAddress
    {
        return $this->mockObject(AlumniAddress::class, [
            'id' => $this->randomLetterOnlyString(10),
            'pidm' => $this->randomPidm(),
            'addressType' => $this->faker->randomKey(['A1', 'A2']),
            'streetLine1' => $this->faker->address,
            'streetLine2' => $this->faker->address,
            'streetLine3' => $this->faker->address,
            'city' => $this->faker->city,
            'state' => $this->faker->state,
            'postalCode' => $this->faker->numberBetween(10000,99999),
            'nationBannerCode' => $this->faker->text,
            'nationDescription' => $this->faker->text,
            'sequenceNumber' => $this->faker->randomNumber(),
            'status' => $this->faker->word,
            'from' => $this->randomDate(),
            'to' => $this->randomDate(),
            'addressSourceCode' => $this->faker->text,
        ], $data);
    }

    protected function mockAlumniPhone(array $data = []): AlumniPhone
    {
        return $this->mockObject(AlumniPhone::class, [
            'id' => $this->randomLetterOnlyString(10),
            'pidm' => $this->randomPidm(),
            'phoneType' => $this->faker->randomKey(['A1', 'A2']),
            'phoneDesc' => $this->faker->text,
            'isPrimary' => $this->faker->boolean,
            'areaPart' => $this->faker->numberBetween(100, 999),
            'numberPart' => $this->faker->numberBetween(1000000, 9999999),
            'phoneNumber' => $this->faker->phoneNumber,
            'ext' => $this->faker->numberBetween(1000, 9999),
            'phoneNumberNational' => $this->faker->phoneNumber,
            'phoneNumberInternational' => $this->faker->phoneNumber,
            'sequenceNumber' => $this->faker->randomNumber(),
            'status' => $this->faker->word,
            'activityDate' => $this->randomDate(),
        ], $data);
    }

    protected function mockCreateAlumniWorkEmailRequest(array $data = []): CreateAlumniWorkEmailRequest
    {
        return $this->mockObject(CreateAlumniWorkEmailRequest::class, [
            'pidm' => $this->randomPidm(),
            'type' => 'WRKE',
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'isActive' => $this->faker->boolean,
            'isDispWeb' => $this->faker->boolean,
            'createdBy' => $this->faker->word
        ], $data);
    }

    protected function mockAlumniAddresses(int $number = 1, array $data = []): AlumniAddressCollection
    {
        return $this->mockCollection(
            AlumniAddressCollection::class,
            function (array $d) {
                return $this->mockAlumniAddress($d);
            },
            $number,
            $data
        );
    }
    
    protected function mockAlumniPhones(int $number = 1, array $data = []): AlumniPhoneCollection
    {
        return $this->mockCollection(
            AlumniPhoneCollection::class,
            function (array $d) {
                return $this->mockAlumniPhone($d);
            },
            $number,
            $data
        );
    }

    protected function mockAlumniWorkEmail(array $data = []): AlumniWorkEmail
    {
        return $this->mockObject(AlumniWorkEmail::class, [
            'id' => $this->randomLetterOnlyString(10),
            'pidm' => $this->randomPidm(),
            'typeCode' => 'WRKE',
            'type' => 'Alumni Work Email',
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'isActive' => $this->faker->boolean,
            'isDisplayedOnWeb' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'updatedBy' => $this->faker->word,
            'updatedAt' => $this->randomDate(),
        ], $data);
    }

    protected function mockAlumniWorkEmails(int $number = 1, array $data = []): AlumniWorkEmailCollection
    {
        return $this->mockCollection(
            AlumniWorkEmailCollection::class,
            function (array $d) {
                return $this->mockAlumniWorkEmail($d);
            },
            $number,
            $data
        );
    }

    protected function mockCreateAlumniEmploymentRequest(array $data = []): CreateAlumniEmploymentRequest
    {
        return $this->mockObject(CreateAlumniEmploymentRequest::class, [
            'pidm' => $this->randomPidm(),
            'employer' => $this->faker->word,
            'employerId' => $this->randomEmployerId(),
            'position' => $this->faker->word,
            'isPrimary' => $this->faker->boolean,
            'from' => $this->randomDate(),
            'to' => $this->randomDate(),
            'weeklyHours' => $this->faker->randomFloat(null, 10, 80),
            'statusCode' => $this->randomAlumniEmploymentStatusCode(),
            'jobCategoryCodes' => [$this->randomJobCategoryCode()],
            'standardIndustrialCode' => $this->randomStandardIndustrialCode(),
            'crossReferenceCode' => $this->randomCrossReferenceCode(),
            'comment' => $this->faker->text,
            'addressType' => $this->faker->randomKey(['A1', 'A2']),
            'addressSeq' => $this->faker->randomNumber(),
            'isOkForNotes' => $this->faker->boolean,
            'isDisplayedNotes' => $this->faker->boolean,
            'isMatchingGift' => $this->faker->boolean,
            'isReviewed' => $this->faker->boolean,
            'reviewedBy' => $this->faker->boolean,
            'isCoop' => $this->faker->boolean,
            'noteDate' => $this->randomDate(),
            'createdBy' => $this->randomUniqueId(),
        ], $data);
    }

    protected function mockCreateAlumniEmploymentDTORequest(array $data = []): CreateAlumniEmploymentRequestDTO
    {
        return $this->mockObject(CreateAlumniEmploymentRequestDTO::class, [
            'prospectId' => $this->randomLetterOnlyString(10),
            'employer' => $this->faker->word,
            'employerId' => $this->randomEmployerId(),
            'position' => $this->faker->word,
            'isPrimary' => $this->faker->boolean,
            'from' => $this->randomDate(),
            'to' => $this->randomDate(),
            'weeklyHours' => $this->faker->randomFloat(null, 10, 80),
            'statusCode' => $this->randomAlumniEmploymentStatusCode(),
            'jobCategoryCodes' => [$this->randomJobCategoryCode()],
            'standardIndustrialCode' => $this->randomStandardIndustrialCode(),
            'crossReferenceCode' => $this->randomCrossReferenceCode(),
            'comment' => $this->faker->text,
            'isOkForNotes' => $this->faker->boolean,
            'isDisplayedNotes' => $this->faker->boolean,
            'isMatchingGift' => $this->faker->boolean,
            'isReviewed' => $this->faker->boolean,
            'reviewedBy' => $this->faker->boolean,
            'isCoop' => $this->faker->boolean,
            'noteDate' => $this->randomDate(),
            'createWorkEmailRequest' => null,
            'createPhoneRequest' => null,
            'createAddressRequest' => null,
            'createdBy' => $this->randomUniqueId(),
        ], $data);
    }

    protected function mockCreateAlumniEmploymentDTORequests(int $number = 1, array $data = []): CreateAlumniEmploymentRequestDTOCollection
    {
        return $this->mockCollection(
            CreateAlumniEmploymentRequestDTOCollection::class,
            function (array $d) {
                return $this->mockCreateAlumniEmploymentDTORequest($d);
            },
            $number,
            $data
        );
    }

    protected function mockCreateAlumniWorkEmailRequestDTO(array $data = []): CreateAlumniWorkEmailRequestDTO
    {
        return $this->mockObject(CreateAlumniWorkEmailRequestDTO::class, [
            'type' => 'WRKE',
            'email' => $this->faker->email,
            'isPreferred' => $this->faker->boolean,
            'comment' => $this->faker->text,
            'isActive' => $this->faker->boolean,
            'isDispWeb' => $this->faker->boolean,
            'createdBy' => $this->faker->word
        ], $data);
    }

    protected function mockCreateAlumniAddressRequestDTO(array $data = []): CreateAlumniAddressRequestDTO
    {
        return $this->mockObject(CreateAlumniAddressRequestDTO::class, [
            'type' => $this->faker->randomKey(['A1', 'A2']),
            'from' => $this->randomDate(),
            'to' => $this->randomDate(),
            'isPreferred' => $this->faker->boolean,
            'streetLine1' => $this->faker->address,
            'streetLine2' => $this->faker->address,
            'streetLine3' => $this->faker->address,
            'city' => $this->faker->city,
            'state' => $this->faker->state,
            'zip' => $this->faker->numberBetween(10000,99999),
            'addressSourceCode' => $this->faker->text,
        ], $data);
    }
    
    protected function mockCreateAlumniLinkedInUrlRequestDTO(array $data = []): CreateAlumniLinkedInUrlRequestDTO
    {
        return $this->mockObject(CreateAlumniLinkedInUrlRequestDTO::class, [
            'prospectId' => $this->randomProspectId(),
            'url' => $this->faker->url,
            'comment' => $this->faker->text,
            'createdBy' => $this->randomDate(),
        ], $data);
    }

    protected function mockCreateAlumniLinkedInUrlRequestDTOs(int $number = 1, array $data = []): CreateAlumniLinkedInUrlRequestDTOCollection
    {
        return $this->mockCollection(
            CreateAlumniLinkedInUrlRequestDTOCollection::class,
            function (array $d) {
                return $this->mockCreateAlumniLinkedInUrlRequestDTO($d);
            },
            $number,
            $data
        );
    }
    
    protected function mockCreateAlumniAddressRequest(array $data = []): CreateAlumniAddressRequest
    {
        return $this->mockObject(CreateAlumniAddressRequest::class, [
            'pidm' => $this->randomPidm(),
            'type' => $this->faker->randomKey(['A1', 'A2']),
            'from' => $this->randomDate(),
            'to' => $this->randomDate(),
            'isPreferred' => $this->faker->boolean,
            'streetLine1' => $this->faker->address,
            'streetLine2' => $this->faker->address,
            'streetLine3' => $this->faker->address,
            'city' => $this->faker->city,
            'state' => $this->faker->state,
            'zip' => $this->faker->numberBetween(10000,99999),
            'addressSourceCode' => $this->faker->text,
        ], $data);
    }
    
    protected function mockCreateAlumniPhoneRequestDTO(array $data = []): CreateAlumniPhoneRequestDTO
    {
        return $this->mockObject(CreateAlumniPhoneRequestDTO::class, [
            'type' => $this->faker->randomKey(['A1', 'A2']),
            'areaCode' => $this->faker->numberBetween(100, 999),
            'number' => $this->faker->numberBetween(1000000, 9999999),
            'extension' => $this->faker->numberBetween(1000, 9999),
        ], $data);
    }

    protected function mockCreateAlumniPhoneRequest(array $data = []): CreateAlumniPhoneRequest
    {
        return $this->mockObject(CreateAlumniPhoneRequest::class, [
            'pidm' => $this->randomPidm(),
            'type' => $this->faker->randomKey(['A1', 'A2']),
            'areaCode' => $this->faker->numberBetween(100, 999),
            'number' => $this->faker->numberBetween(1000000, 9999999),
            'extension' => $this->faker->numberBetween(1000, 9999),
        ], $data);
    }

    protected function mockEloquentAlumni(array $data = []): EloquentAlumni
    {
        $rtn = $this->createMock(EloquentAlumni::class);
        $rtn->method('getPidm')->willReturn($this->getValue($data, 'getPidm', $this->randomPidm()));
        $rtn->method('getId')->willReturn($this->getValue($data, 'getId', $this->randomProspectId()));
        return $rtn;
    }

    protected function mockEloquentEmployer(array $data = []): EloquentEmployer
    {
        $rtn = $this->createMock(EloquentEmployer::class);
        $rtn->method('getPidm')->willReturn($this->getValue($data, 'getPidm', $this->randomPidm()));
        $rtn->method('getId')->willReturn($this->getValue($data, 'getId', $this->randomEmployerId()));
        $rtn->method('getName')->willReturn($this->getValue($data, 'getName', $this->faker->word));
        return $rtn;
    }

    protected function mockEloquentAlumniEmployment(array $data = []): EloquentAlumniEmployment
    {
        $rtn = $this->createMock(EloquentAlumniEmployment::class);
        $rtn->method('getId')->willReturn($this->getValue($data, 'getId', $this->randomLetterOnlyString(10)));
        $rtn->method('getPidm')->willReturn($this->getValue($data, 'getPidm', $this->randomPidm()));
        $rtn->method('getEmployer')->willReturn($this->getValue($data, 'getEmployer', $this->faker->word));
        $rtn->method('getProspectId')->willReturn($this->getValue($data, 'getProspectId', $this->randomProspectId()));
        $rtn->method('getJobCategoryCode')->willReturn($this->getValue($data, 'getJobCategoryCode', $this->randomJobCategoryCode()));
        $rtn->method('getPosition')->willReturn($this->getValue($data, 'getPosition', $this->faker->word));
        $rtn->method('getFrom')->willReturn($this->getValue($data, 'getFrom', $this->randomDate()));
        $rtn->method('getTo')->willReturn($this->getValue($data, 'getTo', $this->randomDate()));
        $rtn->method('getEmployerPidm')->willReturn($this->getValue($data, 'getEmployerPidm', $this->randomPidm()));
        $rtn->method('getEmployerId')->willReturn($this->getValue($data, 'getEmployerId', $this->randomEmployerId()));
        $rtn->method('getEmployerName')->willReturn($this->getValue($data, 'getEmployerName', $this->faker->word));
        $rtn->method('getActivityDate')->willReturn($this->getValue($data, 'getActivityDate', $this->randomDate()));
        $rtn->method('getStandardIndustrialCode')->willReturn($this->getValue($data, 'getStandardIndustrialCode', $this->randomStandardIndustrialCode()));
        $rtn->method('getCrossReferenceCode')->willReturn($this->getValue($data, 'getCrossReferenceCode', $this->randomCrossReferenceCode()));
        $rtn->method('getEmploymentStatusCode')->willReturn($this->getValue($data, 'getEmploymentStatusCode', $this->randomAlumniEmploymentStatusCode()));
        $rtn->method('getWeeklyHours')->willReturn($this->getValue($data, 'getWeeklyHours', $this->faker->randomFloat(null, 10, 80)));
        $rtn->method('isPrimary')->willReturn($this->getValue($data, 'isPrimary', $this->faker->boolean));
        $rtn->method('getAddressType')->willReturn($this->getValue($data, 'getAddressType', $this->faker->randomKey(['A1', 'A2'])));
        $rtn->method('getAddressSeq')->willReturn($this->getValue($data, 'getAddressSeq', $this->faker->randomNumber()));
        $rtn->method('getJobCategoryCode2')->willReturn($this->getValue($data, 'getJobCategoryCode2', $this->randomJobCategoryCode()));
        $rtn->method('getJobCategoryCode3')->willReturn($this->getValue($data, 'getJobCategoryCode3', $this->randomJobCategoryCode()));
        $rtn->method('getJobCategoryCode4')->willReturn($this->getValue($data, 'getJobCategoryCode4', $this->randomJobCategoryCode()));
        $rtn->method('getComments')->willReturn($this->getValue($data, 'getComments', $this->faker->text));
        $rtn->method('getSeq')->willReturn($this->getValue($data, 'getSeq', $this->faker->randomNumber()));
        $rtn->method('getUser')->willReturn($this->getValue($data, 'getUser', strtoupper($this->randomUniqueId())));
        $rtn->method('isOkForNotes')->willReturn($this->getValue($data, 'isOkForNotes', $this->faker->boolean));
        $rtn->method('isDisplayedNotes')->willReturn($this->getValue($data, 'isDisplayedNotes', $this->faker->boolean));
        $rtn->method('isMatchingGift')->willReturn($this->getValue($data, 'isMatchingGift', $this->faker->boolean));
        $rtn->method('isReviewed')->willReturn($this->getValue($data, 'isReviewed', $this->faker->boolean));
        $rtn->method('getReviewedBy')->willReturn($this->getValue($data, 'getReviewedBy', strtoupper($this->randomUniqueId())));
        $rtn->method('isCoop')->willReturn($this->getValue($data, 'isCoop', $this->faker->boolean));
        $rtn->method('getNoteDate')->willReturn($this->getValue($data, 'getNoteDate', $this->randomDate()));
        return $rtn;
    }

    protected function mockEloquentAlumniEmployments(int $number = 1, array $data = []): Collection
    {
        return $this->mockCollection(
            Collection::class,
            function (array $d) {
                return $this->mockEloquentAlumniEmployment($d);
            },
            $number,
            $data
        );
    }

    /***********************************************************************************
     * Helper functions
     ***********************************************************************************/
    private function mockObject(string $class, array $config = [], array $data = [])
    {
        $c = [];
        foreach ($config as $key => $value) {
            $c[] = $this->getValue($data, $key, $value);
        }

        return new $class(...$c);
    }

    private function mockCollection(string $class, \Closure $mockObject, int $number = 1, array $data = [])
    {
        $collection = new $class();

        for ($i = 0; $i < $number; $i++) {
            $collection->push($mockObject($data[$i] ?? []));
        }

        return $collection;
    }

    private function randomProspectId(): string
    {
        return 'A' . $this->faker->numberBetween(1849374, 95849674);
    }

    private function randomLetterOnlyString(int $length = 10): string
    {
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $this->faker->randomLetter;
        }

        return $str;
    }

    private function randomPidm(): string
    {
        return $this->faker->randomNumber(7);
    }

    private function randomUniqueId(): string
    {
        return $this->randomLetterOnlyString(5) . $this->faker->numberBetween(1, 100);
    }

    private function randomDate(): Carbon
    {
        return Carbon::createFromFormat('Y-m-d', $this->faker->date('Y-m-d'));
    }

    private function getValue(array $data, string $key, $default)
    {
        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return $default;
    }

    private function randomTermCode(): string
    {
        return $this->faker->numberBetween(2013, 2019) . $this->faker->randomKey([
                '10',
                '15',
                '20',
                '30',
            ]);
    }

    private function randomJobCategoryCode(): string
    {
        return $this->faker->randomKey([
            'COR',
            'CEO',
            'CFO',
            'COB',
            'CON',
            'COO',
            'DIR',
            'EVP',
        ]);
    }

    private function randomStandardIndustrialCode(): string
    {
        return $this->faker->randomKey([
            '91XX',
            '92XX',
            '02XX',
            '07XX',
            '08XX',
            '09XX',
            '10XX',
        ]);
    }

    private function randomCrossReferenceCode(): string
    {
        return $this->faker->randomKey([
            'CFD',
            'CFM',
            'FMG',
            'CMG',
            'SPS',
            'EMR',
        ]);
    }

    private function randomAlumniEmploymentStatusCode(): string
    {
        return $this->faker->randomKey([
            'R',
            'F',
            'C',
            'B',
            'U',
        ]);
    }

    private function randomEmployerId(): string
    {
        return strtoupper($this->randomLetterOnlyString(6));
    }
}
